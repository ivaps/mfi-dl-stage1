package com.lti.mfi.data.load;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.lti.ivaps.mfi.beans.Address;
import com.lti.ivaps.mfi.beans.MfiDomain;
import com.lti.ivaps.mfi.constans.MfiConstants;
import com.lti.ivaps.mfi.validation.AddressValidation;

public class AddressBeanBuilder {
	public static List<Address> buidAddressBean(String record, MfiDomain mfiDomain){/*
		String[] split =  record.split("$");
		Address address = new Address();
		for(String s : split){
			if(s.contains("ADRCRD")){
				String[] splitPreserveAllTokens = StringUtils.splitPreserveAllTokens(s, "~");
				for(String token : splitPreserveAllTokens){
					System.out.println(token);
				}

				address.setSegmentIdentifier(splitPreserveAllTokens[0]);
				address.setPermanentAddress(splitPreserveAllTokens[1]);
				address.setPermanentAdrstateCode(splitPreserveAllTokens[2]);
				address.setPermanentAdrpinCode(splitPreserveAllTokens[3]);
				address.setCurrentAddress(splitPreserveAllTokens[4]);
				address.setCurrentAdrstateCode(splitPreserveAllTokens[5]);
				address.setCurrentAdrpinCode(splitPreserveAllTokens[6]);
			}
		}

		return address;
	 */
		//System.out.println("Address Bean builder");
		String[] splitPreserveAllTokens2 = StringUtils.splitPreserveAllTokens(record, MfiConstants.splitDelimiterDollar);
		//System.out.println("Address Bean builder splitPreserveAllTokens2 records:" +record);
		List<Address> listAddress = new ArrayList<Address>();
		HashMap<Address, String> hma = new HashMap<Address, String>();
		for(String s : splitPreserveAllTokens2){
			System.out.println(s);
			
			if(s.startsWith("ADRCRD")){
				Address address = new Address();
				String[] splitPreserveAllTokens = StringUtils.splitPreserveAllTokens(s, MfiConstants.splitDelimiterTilda);
				/*for(String token : splitPreserveAllTokens){
					System.out.println(token);
				}*/

				address.setSegmentIdentifier(splitPreserveAllTokens[0]);
				address.setPermanentAddress(splitPreserveAllTokens[1]);
				address.setPermanentAdrstateCode(splitPreserveAllTokens[2]);
				address.setPermanentAdrpinCode(splitPreserveAllTokens[3]);
				address.setCurrentAddress(splitPreserveAllTokens[4]);
				address.setCurrentAdrstateCode(splitPreserveAllTokens[5]);
				address.setCurrentAdrpinCode(splitPreserveAllTokens[6]);
				
				//Address Validation++++++
				if(mfiDomain.getReasonForFailure().isEmpty()){
				mfiDomain.setReasonForFailure(AddressValidation.validateAddress(address));
				}else{
				mfiDomain.setReasonForFailure(mfiDomain.getReasonForFailure().concat(", " +AddressValidation.validateAddress(address)));
				}
				
				if(null == AddressValidation.validateAddress(address)){
					listAddress.add(address);
				}

			}
		}
		
		return listAddress;
	}
	
	/*public static void main(String[] args) {
		MfiDomain md = new MfiDomain();
		String r = "ADRCRD~. MAIN ROAD KARABALUA CHIKITIPENTHO~AP~760002~~AP~~~";
		buidAddressBean(r, md);
	}*/
	
}
