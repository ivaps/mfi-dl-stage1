package com.lti.mfi.data.load;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.HiveContext;

import com.augmentiq.maxiq.extenation.InputParameterValues;
import com.augmentiq.maxiq.extenation.MaxiqType1Job;
import com.augmentiq.maxiq.extenation.MaxiqType2Job;
import com.lti.dao.MfiJobTrackerDao;
import com.lti.ivaps.mfi.DataFramesFactory;
import com.lti.ivaps.mfi.PopulateMfiDomain;
import com.lti.ivaps.mfi.beans.Account;
import com.lti.ivaps.mfi.beans.FailedRecordsBean;
import com.lti.ivaps.mfi.beans.MfiDomain;
import com.lti.ivaps.mfi.beans.PassedRecordsBean;
import com.lti.ivaps.mfi.constans.MfiConstants;
import com.lti.ivaps.mfi.dataframes.Profiling;
import com.lti.ivaps.mfi.dataframes.SummaryStatistics;
import com.lti.ivaps.mfi.error.ErrorCodeList;
import com.lti.ivaps.mfi.error.ErrorRecord;
import com.lti.ivaps.mfi.exceptions.CdfParsingException;
import com.lti.ivaps.mfi.parser.CdfParser;
import com.lti.ivaps.mfi.validation.CdfValidation;

public class MfiDataLoad<T, M> implements MaxiqType2Job<T, M> {
	
	
	static  Logger log = Logger.getLogger(MfiDataLoad.class);
	
	SQLContext sqlContext;
	static Map<String, String> argumentVar = new HashMap<String, String>();
	String mfiId;
	String rptDt;
	String mfiJobId;
	String maxIqJobId;

	String appName /*= "stage_1_new"*/;
	String nodeName/* = "MFI"*/;

	static String baapId; // = "MAXIQ01";
	boolean isSuceed = false;

	//static String test="terst" ;

	public boolean init(Map<String, String> arg0, Map<String, String> arg1) {
		//test = "VALUE";
		// TODO Auto-generated method stub
	//	System.out.println("Argument 0 :" +arg0); 
	//	System.out.println("Argument 1 :" +arg1);
		
		PropertyConfigurator.configure("/Application/softwares/mfi/log4j.properties");
		Logger.getLogger("org").setLevel(Level.ERROR);
	    Logger.getLogger("akka").setLevel(Level.ERROR);
	    
	    log.info("Initialisation of application starts...");

		if(null != arg0 && null != arg1){
			argumentVar = arg0;

			baapId = arg0.get("rootId");

			mfiJobId = arg1.get("gp_MFI_JOB_ID");
			//System.out.println("MFI JOB ID : " + mfiJobId);
			mfiId = arg1.get("gp_MFI_ID");
			//System.out.println("MFIID : " + mfiId);
			/*rptDt = StringUtils.removePattern(arg1.get("gp_REPORTED_DATE"), " ");
			rptDt = arg1.get("gp_REPORTED_DATE").split(" ")[0];*/
			rptDt = arg1.get("gp_REPORTED_DATE");
			//System.out.println("RPT DT : " + rptDt);
			appName = arg1.get("gp_STAGE1_FLOW_NAME");
			nodeName = arg1.get("gp_STAGE1_NODE_NAME").toLowerCase();

		}

		/*mfiId = "MFI0000006";
			 rptDt = "24102013";
			mfiJobId = "40";
			baapId = "MAXIQ01";	*/		

		return false;
	}
	
	
	public static Map<String, String> getConnectionDetail() {

		//System.out.println("Get connection Details:" + argumentVar);
		log.info("Get connection details...");

		return argumentVar;
	}

	public Map<String, DataFrame> run(Map<String, DataFrame> dataFrames) {
		//init(null, null);
		//System.out.println("Inside run method");
		Map<String, DataFrame> dfMap = new HashMap<String, DataFrame>();
		try {
			for (Entry<String, DataFrame> entry : dataFrames.entrySet()) {
				//System.out.println("For loop Myvalue : " + mfiId +"Reported Date" +rptDt);
				//
				//System.out.println("test : " + test);

				DataFrame df = entry.getValue();
				
			//	System.out.println("INcoming DF ***********************************************************");
			//	df.show();
			//	System.out.println("INcoming DF ***********************************************************");
				
				sqlContext = df.sqlContext();
				SparkContext sparkContext = df.sqlContext().sparkContext();
				JavaSparkContext javaSparkContext = new JavaSparkContext(sparkContext);

				HiveContext hiveContext = new HiveContext(sqlContext.sparkContext());
				JavaRDD<Row> javaRDD = df.toJavaRDD();

				JavaRDD<MfiDomain> mfiDomainRdd = javaRDD.map(new Function<Row, MfiDomain>() {
					private static final long serialVersionUID = 1L;

					public MfiDomain call(Row record) throws Exception {

						//System.out.println("Myvalue : " + mfiId +"Reported Date" +rptDt);
						String stringRecord = (String) record.get(0);
						MfiDomain mfiDomain = new MfiDomain(mfiId, rptDt, stringRecord);

						HashMap<String, ArrayList<String[]>> parsedRecord = null;
						boolean validateRecord = true;

						try {
							if (StringUtils.startsWith(stringRecord, "HDR")
									|| StringUtils.startsWith(stringRecord, "TRL")) {
								mfiDomain.setHeaderTrailerIndicator(true);
							} else {
								parsedRecord = CdfParser.parseMe(stringRecord);
								log.info("CDF parsing is successful");

								if (parsedRecord.get(MfiConstants.CNSCRD).size() > 1) {
									mfiDomain.setValid(false);
									mfiDomain.addErrorCode(ErrorCodeList.E004);
									log.error("Record contains more than one consumer segment.");
								}

								if ( null == parsedRecord.get(MfiConstants.ACTCRD)/* && 0 == parsedRecord.get(MfiConstants.ACTCRD).size()*/) {
									mfiDomain.setValid(false);
									mfiDomain.addErrorCode(ErrorCodeList.E002);
									log.error("Record do not contain any account segment");
								}

								if (null == parsedRecord.get(MfiConstants.ADRCRD) /*&& 0 == parsedRecord.get(MfiConstants.ADRCRD).size()*/) {
									mfiDomain.setValid(false);
									mfiDomain.addErrorCode(ErrorCodeList.E003);
									log.error("Record do not contain any address segment.");
								}

								if (mfiDomain.getValid() && !mfiDomain.getHeaderTrailerIndicator()) {
									PopulateMfiDomain.populateMfiDomainFromRDD(parsedRecord, mfiDomain);
									
									validateRecord = CdfValidation.validateRecord(mfiDomain);
									
									if(validateRecord){
										log.info("CDF validation is successful");
									}else{
									log.info("CDF validation is unsuccessful");
									}
								}
							}
						} catch (CdfParsingException e) {
							mfiDomain.setValid(false);
							mfiDomain.setReasonForFailure(e.getMessage());
							log.error("Record parsing unsuccessful");
						}
						return mfiDomain;
					}
				});

				JavaRDD<MfiDomain> mfiDomainRdd1 = mfiDomainRdd.filter(new Function<MfiDomain, Boolean>(){

					//@Override
					public Boolean call(MfiDomain arg0) throws Exception {
						// TODO Auto-generated method stub

						return !arg0.getHeaderTrailerIndicator();

					}

				});

				/*JavaRDD<MfiDomain> failedRecords = mfiDomainRdd1.filter(new Function<MfiDomain, Boolean>() {
					// @Override
					public Boolean call(MfiDomain arg0) throws Exception {
						return !arg0.getValid();
					}
				});

				JavaRDD<MfiDomain> passedRecords = mfiDomainRdd1.filter(new Function<MfiDomain, Boolean>() {
					// @Override
					public Boolean call(MfiDomain arg0) throws Exception {
						return arg0.getValid();
					}
				});*/

				JavaRDD<PassedRecordsBean> passedRecordsRdd = mfiDomainRdd1
						.map(new Function<MfiDomain, PassedRecordsBean>() {
							// @Override
							public PassedRecordsBean call(MfiDomain mfiDomain) throws Exception {
								return PassedRecordsBean.createPassedRecord(mfiDomain);
							}
						}).filter(new Function<PassedRecordsBean, Boolean>() {

							public Boolean call(PassedRecordsBean arg0) throws Exception {
								// TODO Auto-generated method stub
								return null != arg0.getMemberId();
							}
						});

				
			//	System.out.println("Passed records rdd count: "+ passedRecordsRdd.count());
				
				JavaRDD<FailedRecordsBean> failedRecordsRdd = mfiDomainRdd1
						.map(new Function<MfiDomain, FailedRecordsBean>() {
							// @Override
							public FailedRecordsBean call(MfiDomain mfiDomain) throws Exception {
								return FailedRecordsBean.createFailedRecord(mfiDomain);
							}
						}).filter(new Function<FailedRecordsBean, Boolean>() {

							public Boolean call(FailedRecordsBean arg0) throws Exception {
								// TODO Auto-generated method stub
								return null != arg0.getSeverity();
							}
						});
				
				//System.out.println("Failed records rdd count: "+ failedRecordsRdd.count());

				DataFrame passedRecordsDf = DataFramesFactory.createDataFrame(passedRecordsRdd, PassedRecordsBean.class,
						hiveContext);
				
				//System.out.println("Passed records DF count: "+ passedRecordsDf.count());
				
				DataFrame failedRecordsDf = DataFramesFactory.createDataFrame(failedRecordsRdd, FailedRecordsBean.class,
						hiveContext);
				
				//System.out.println("Passed records DF count: "+ failedRecordsDf.count());

				//passedRecordsDf.show();
				//failedRecordsDf.show();

				final Accumulator<Integer> failedCount = javaSparkContext.accumulator(0);
				final Accumulator<Integer> passedCount = javaSparkContext.accumulator(0);
				final Accumulator<Integer> totalCount = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfE001 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfE002 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfE003 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfE004 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfE005 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfE006 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfE007 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT002 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT003 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT004 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT005 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT006 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT007 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT008 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT011 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT012 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT015 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT019 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT020 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT023 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT024 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEACT025 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEADR000 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEADR001 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfEADR002 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfECNS002 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfECNS003 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfECNS004 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfECNS006 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfECNS010 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfECNS011 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfECNS012 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfECNS013 = javaSparkContext.accumulator(0);
				final Accumulator<Integer> countOfECNS015_024 = javaSparkContext.accumulator(0);

				// Profiling counts
				final Accumulator<Integer> loanCategoryJLGGroup = javaSparkContext.accumulator(0);
				final Accumulator<Integer> loanCategoryJLGIndividual = javaSparkContext.accumulator(0);
				final Accumulator<Integer> loanCategoryIndividual = javaSparkContext.accumulator(0);
				final Accumulator<Integer> loanCategoryOther = javaSparkContext.accumulator(0);

				final Accumulator<Integer> loanPurposeIndividualLoanEducationLoan = javaSparkContext.accumulator(0);
				final Accumulator<Integer> loanPurposeGroupLoanEmergencyFestivalLoan = javaSparkContext.accumulator(0);
				final Accumulator<Integer> loanPurposeIndividualLoanEmergencyFestivalLoan = javaSparkContext.accumulator(0);
				final Accumulator<Integer> loanPurposeFamilyLoan = javaSparkContext.accumulator(0);
				final Accumulator<Integer> loanPurposeGeneralLoan = javaSparkContext.accumulator(0);
				final Accumulator<Integer> loanPurposeGoldLoan = javaSparkContext.accumulator(0);
				final Accumulator<Integer> loanPurposeOthers = javaSparkContext.accumulator(0);

				final Accumulator<Integer> accStatusLoanSubmitted = javaSparkContext.accumulator(0);
				final Accumulator<Integer> accStatusLoanApprovedNotyetdisbursed = javaSparkContext.accumulator(0);
				final Accumulator<Integer> accStatusLoanDeclined = javaSparkContext.accumulator(0);
				final Accumulator<Integer> accStatusCurrent = javaSparkContext.accumulator(0);
				final Accumulator<Integer> accStatusDelinquent = javaSparkContext.accumulator(0);
				final Accumulator<Integer> accStatusWrittenOff = javaSparkContext.accumulator(0);
				final Accumulator<Integer> accStatusAccountClosed = javaSparkContext.accumulator(0);

				mfiDomainRdd.foreach(new VoidFunction<MfiDomain>(){

					public void call(MfiDomain mfiDomain) throws Exception {
						// TODO Auto-generated method stub
						//System.out.println("Reason of failure:" +mfiDomain.getReasonOfFailureForCounter());
						
						if(!mfiDomain.getHeaderTrailerIndicator()){
							
							if(mfiDomain.getValid()){
								
							passedCount.add(1);
							
								List<Account> accounts = mfiDomain.getAccount();
								for(Account account : accounts) {

									if (StringUtils.isNotBlank(account.getLoanCategory()) 
											&& StringUtils.isNotBlank(account.getLoanPurpose()) 
											&& StringUtils.isNotBlank(account.getAccStatus())) {

										// Loan Category
										if(account.getLoanCategory().equalsIgnoreCase(MfiConstants.T01_ACCOUNT_LOAN_CATEGORY)){
											loanCategoryJLGGroup.add(1);
										}else if(account.getLoanCategory().equalsIgnoreCase(MfiConstants.T02_ACCOUNT_LOAN_CATEGORY)){
											loanCategoryJLGIndividual.add(1);
										}else if (account.getLoanCategory().equalsIgnoreCase(MfiConstants.T03_ACCOUNT_LOAN_CATEGORY)){
											loanCategoryIndividual.add(1);
										}else{
											loanCategoryOther.add(1);
										}
										//Loan Purpose
										if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A01_ACCOUNT_LOAN_PURPOSE)){
											loanPurposeIndividualLoanEducationLoan.add(1);
										}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A02_ACCOUNT_LOAN_PURPOSE)){
											loanPurposeGroupLoanEmergencyFestivalLoan.add(1);
										}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A03_ACCOUNT_LOAN_PURPOSE)){
											loanPurposeIndividualLoanEmergencyFestivalLoan.add(1);
										}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A04_ACCOUNT_LOAN_PURPOSE)){
											loanPurposeFamilyLoan.add(1);
										}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A05_ACCOUNT_LOAN_PURPOSE)){
											loanPurposeGeneralLoan.add(1);
										}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A06_ACCOUNT_LOAN_PURPOSE)){
											loanPurposeGoldLoan.add(1);
										}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A35_ACCOUNT_LOAN_PURPOSE)){
											loanPurposeOthers.add(1);
										}
										//Account Status
										if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S01_ACCOUNT_STATUS)){
											accStatusLoanSubmitted.add(1);
										}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S02_ACCOUNT_STATUS)){
											accStatusLoanApprovedNotyetdisbursed.add(1);
										}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S03_ACCOUNT_STATUS)){
											accStatusLoanDeclined.add(1);
										}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S04_ACCOUNT_STATUS)){
											accStatusCurrent.add(1);
										}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S05_ACCOUNT_STATUS)){
											accStatusDelinquent.add(1);
										}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S06_ACCOUNT_STATUS)){
											accStatusWrittenOff.add(1);
										}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S07_ACCOUNT_STATUS)){
											accStatusAccountClosed.add(1);
										}
									}
								}
								//}
						}else{
							failedCount.add(1);
							String[] arrayOfReasonOfFailure = null;
							if(null != mfiDomain.getReasonOfFailureForCounter()){
								arrayOfReasonOfFailure = StringUtils.splitPreserveAllTokens(mfiDomain.getReasonOfFailureForCounter(), MfiConstants.splitDelimiterComma);
							for (int i = 0; i < arrayOfReasonOfFailure.length; i++) {
								
								if(null != arrayOfReasonOfFailure[i]){
							

									if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT002)) {
										countOfEACT002.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT003)) {
										countOfEACT003.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT004)) {
										countOfEACT004.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT005)) {
										countOfEACT005.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT006)) {
										countOfEACT006.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT007)) {
										countOfEACT007.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT008)) {
										countOfEACT008.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT011)) {
										countOfEACT011.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT012)) {
										countOfEACT012.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT015)) {
										countOfEACT015.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT019)) {
										countOfEACT019.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT020)) {
										countOfEACT020.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT023)) {
										countOfEACT023.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT024)) {
										countOfEACT024.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT025)) {
										countOfEACT025.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EADR000)
											|| arrayOfReasonOfFailure[i].contains(ErrorCodeList.EADR001)
											|| arrayOfReasonOfFailure[i].contains(ErrorCodeList.EADR002)) {
										countOfEADR000.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS002)) {
										countOfECNS002.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS003)) {
										countOfECNS003.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS004)) {
										countOfECNS004.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS006)) {
										countOfECNS006.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS010)) {
										countOfECNS010.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS011)) {
										countOfECNS011.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS012)) {
										countOfECNS012.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS013)) {
										countOfECNS013.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS015_024)) {
										countOfECNS015_024.add(1);
									}else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E001)) {
										countOfE001.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E002)) {
										countOfE002.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E003)) {
										countOfE003.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E004)) {
										countOfE004.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E005)) {
										countOfE005.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E006)) {
										countOfE006.add(1);
									} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E007)) {
										countOfE007.add(1);
									}
							    }
							
							 }
						   }
						}
							totalCount.add(1);
					}
						
					}
					
				});

				
				JavaRDD<SummaryStatistics> summaryStatisticsRdd = javaSparkContext.parallelize(SummaryStatistics.generateErrorList(totalCount.value(), passedCount.value(), failedCount.value(),
						countOfE001.value(), countOfE002.value(),countOfE003.value(),countOfE004.value(),countOfE005.value(),countOfE006.value(),countOfE007.value(),
						countOfEACT002.value(), countOfEACT003.value(), countOfEACT004.value(), countOfEACT005.value(),
						countOfEACT006.value(), countOfEACT007.value(), countOfEACT008.value(), countOfEACT011.value(),
						countOfEACT012.value(), countOfEACT015.value(), countOfEACT019.value(), countOfEACT020.value(),
						countOfEACT023.value(), countOfEACT024.value(), countOfEACT025.value(), countOfEADR000.value(),
						countOfECNS002.value(), countOfECNS003.value(), countOfECNS004.value(), countOfECNS006.value(), countOfECNS010.value(),
						countOfECNS011.value(), countOfECNS012.value(), countOfECNS013.value(), countOfECNS015_024.value())).filter(new Function<SummaryStatistics, Boolean>() {

							//@Override
							public Boolean call(SummaryStatistics arg0) throws Exception {
								// TODO Auto-generated method stub
								return 0 != arg0.getCount();
							}
						});

				DataFrame summaryStatisticsDf = DataFramesFactory.createDataFrame(summaryStatisticsRdd,
						SummaryStatistics.class, hiveContext);

				JavaRDD<Profiling> profilingRdd = javaSparkContext
						.parallelize(Profiling.generateProfilingList(loanCategoryJLGGroup.value(),
								loanCategoryJLGIndividual.value(), loanCategoryIndividual.value(),
								loanCategoryOther.value(), loanPurposeIndividualLoanEducationLoan.value(),
								loanPurposeGroupLoanEmergencyFestivalLoan.value(),
								loanPurposeIndividualLoanEmergencyFestivalLoan.value(), loanPurposeFamilyLoan.value(),
								loanPurposeGeneralLoan.value(), loanPurposeGoldLoan.value(), loanPurposeOthers.value(),
								accStatusLoanSubmitted.value(), accStatusLoanApprovedNotyetdisbursed.value(),
								accStatusLoanDeclined.value(), accStatusCurrent.value(), accStatusDelinquent.value()));

				DataFrame profilingDf = DataFramesFactory.createDataFrame(profilingRdd, Profiling.class, hiveContext);

				dfMap.put("passedrecords", passedRecordsDf);
				dfMap.put("failedRecords", failedRecordsDf);
				dfMap.put("summarystatistics", summaryStatisticsDf);
				dfMap.put("profiling", profilingDf);

				isSuceed = MfiJobTrackerDao.updateMfiJobTrackerAndProcessedFilesStatus(mfiJobId, baapId, appName,/*
						MfiConstants.MFI_JOB_STATUS_FINISHED,*/ nodeName, totalCount.value(), failedCount.value(),
						passedCount.value());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!isSuceed)
					MfiJobTrackerDao.updateMfiJobTrackerAndProcessedFilesStatus(mfiJobId, baapId, appName,
							/*MfiConstants.MFI_JOB_STATUS_FAILED,*/ nodeName, 0, 0, 0);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return dfMap;
	}

}
