package com.lti.connection.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import com.lti.mfi.data.load.MfiDataLoad;

public class MysqlConnection {

	private static String confKey = "conf";

	private static String jdbcDriver = null;
	private static String dbAddress = null;
	private static String dbName = null;
	private static String userName = null;
	private static String password = null;

	//Testing
//	 private static String jdbcDriver = "com.mysql.jdbc.Driver";
//	 private static String dbAddress = "jdbc:mysql://172.20.99.22:3306/";
//	 private static String dbName = "ivaps_mfi";
//	 private static String userName = "root";
//	 private static String password = "";

	private static Connection cn = null;
	private static Long lastGivenAt = 0L;
	
	static{
		Map<String,String> conf = MfiDataLoad.getConnectionDetail();
		/*jdbcDriver = conf.get("MYSQL_JDBC_DRIVER").toString();
		dbAddress = conf.get("MYSQL_DB_ADDRESS").toString();
		dbName = conf.get("MYSQL_DB_NAME").toString();
		userName = conf.get("MYSQL_USER_NAME").toString();
		password = conf.get("MYSQL_PASSWORD").toString();*/
		
		//Original
		jdbcDriver = conf.get("MYSQL_JDBC_DRIVER");
		dbAddress = conf.get("MYSQL_DB_ADDRESS");
		dbName = conf.get("MYSQL_DB_NAME");
		userName = conf.get("MYSQL_USER_NAME");
		password = conf.get("MYSQL_PASSWORD");
	}

	/*public static void setConnectionVariables(Map connectionDetails) {
		Map conf = ((Map) connectionDetails.get(confKey));
		jdbcDriver = conf.get("MYSQL_JDBC_DRIVER").toString();
		dbAddress = conf.get("MYSQL_DB_ADDRESS").toString();
		dbName = conf.get("MYSQL_DB_NAME").toString();
		userName = conf.get("MYSQL_USER_NAME").toString();
		password = conf.get("MYSQL_PASSWORD").toString();
	}*/

	public static Connection getConnection() throws Exception {
		Long currentTime = System.currentTimeMillis();
		if (cn == null || (currentTime - lastGivenAt) / 1000 > 30) {

			try {
				if (cn != null)
					cn.close();

				cn = null;
				lastGivenAt = currentTime;

				Class.forName(jdbcDriver);
				Connection connection = DriverManager
						.getConnection(dbAddress + dbName + "?zeroDateTimeBehavior=convertToNull", userName, password);

				return connection;
			} catch (Exception e) {

				e.printStackTrace();
				throw new Exception(e);
			}
		}
		return cn;
	}

	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection(
					"jdbc:mysql://172.20.99.22:3306/" + "ivaps_mfi" + "?zeroDateTimeBehavior=convertToNull", "root", "");
			Statement createStatement = connection.createStatement();
			ResultSet executeQuery = createStatement.executeQuery("select * from mfi_login");
			if (executeQuery.next()) {
				ResultSetMetaData metaData = executeQuery.getMetaData();
				//System.out.println("Connetion:" +executeQuery.getString(2));
				//System.out.println(metaData.getTableName(0));

			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
