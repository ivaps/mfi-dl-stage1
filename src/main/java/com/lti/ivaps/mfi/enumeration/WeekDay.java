package com.lti.ivaps.mfi.enumeration;

public enum WeekDay {
	MON,
	TUE,
	WED,
	THU,
	FRI,
	SAT,
	SUN
}
