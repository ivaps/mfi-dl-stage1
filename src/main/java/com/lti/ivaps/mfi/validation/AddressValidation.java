package com.lti.ivaps.mfi.validation;

import org.apache.commons.lang3.StringUtils;

import com.lti.ivaps.mfi.beans.Address;
import com.lti.ivaps.mfi.beans.MfiDomain;
import com.lti.ivaps.mfi.constans.AddressConstants;
import com.lti.ivaps.mfi.constans.MfiConstants;

public class AddressValidation {
	
	public static String validateAddress(Address addressCrd) {
		
		String addressValidationData = null;
		//String addressValidationData2 = null;
        
		if(!checkAddress(addressCrd.getCurrentAddress()) && !checkAddress(addressCrd.getCurrentAdrstateCode()) && !checkPinCode(addressCrd.getCurrentAdrpinCode())){
			addressValidationData = "InValid Current address";
			System.out.println("InValid Current address");
			
		}
				
		if(!checkAddress(addressCrd.getPermanentAddress())
				&& !checkAddress(addressCrd.getPermanentAdrstateCode()) &&  !checkPinCode(addressCrd.getCurrentAdrpinCode())){
			addressValidationData.concat("and InValid Permanent address");
			System.out.println("InValid Permanent address");
			
		}
		return addressValidationData;

	}
	
	private static boolean checkAddress(String address) {
		
			if(!StringUtils.isBlank(address) && address != null && address.length() > 7){
				return true;
			}
			return false;
	}
	
	public static boolean checkPinCode( String pin )
	   {
		if(!StringUtils.isBlank(pin) && pin != null && pin.length() <= 6 && pin.matches("d{6}")){
			return true;
		}
		return false;

	   } 
	
}
