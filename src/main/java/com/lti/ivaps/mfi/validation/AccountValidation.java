package com.lti.ivaps.mfi.validation;

import org.apache.calcite.linq4j.tree.ThrowStatement;
import org.apache.commons.lang3.StringUtils;

import com.lti.ivaps.mfi.beans.Account;
import com.lti.ivaps.mfi.beans.Address;
import com.lti.ivaps.mfi.beans.MfiDomain;
import com.lti.ivaps.mfi.constans.MfiConstants;
import com.lti.ivaps.mfi.enumeration.AccountStatus;
import com.lti.ivaps.mfi.enumeration.InsuranceIndicator;
import com.lti.ivaps.mfi.enumeration.LoanCategory;
import com.lti.ivaps.mfi.enumeration.RepaymentFrequency;
import com.lti.ivaps.mfi.enumeration.TypeOfInsurance;
import com.lti.ivaps.mfi.enumeration.WeekDay;
import com.lti.ivaps.mfi.enumeration.WriteOffReason;

public class AccountValidation {
	
	String errorString = null;
	//String accountValidationData = null;
	
	public static String validateAccount(Account actCrd, MfiDomain mfiDomain){
		
		if(!checkAccRefAndAccNumber(actCrd.getUniqueAccRefNumber())
				&& !checkAccRefAndAccNumber(actCrd.getAccNumber())
				&& !checkBranchAndKendraIdentifierAndLoanOfficer(actCrd.getBranchIdentifier())
				&& !checkBranchAndKendraIdentifierAndLoanOfficer(actCrd.getKendraCentreIdentifier())
				&& !checkBranchAndKendraIdentifierAndLoanOfficer(actCrd.getLoanOfficerForOriginatingTheLoan())
				&& !checkDateOfAccInfo(actCrd.getDateOfAccInfo()) 
				&& !checkLoanCategory(actCrd.getLoanCategory(), actCrd.getGroupIdentifier())
				&& !checkLoanPurpose(actCrd.getLoanPurpose())
				&& !checkAccountStatus(actCrd.getAccStatus(), actCrd.getDateClosed())
				&& !checkDate(actCrd.getDateOpened())
				&& !checkTotalAmountDisbursed(actCrd.getTotalAmountDisbursed())
				&& !checkNumberOfInstallments(actCrd.getNumberOfInstallments())
				&& !checkDueAmountAndCurrentBalanceAndAmountOverdue(actCrd.getCurrentBalance())
				&& !checkDueAmountAndCurrentBalanceAndAmountOverdue(actCrd.getAmountOverdue())
				&& !checkDaysPasseddue(actCrd.getDpd())){
				
				actCrd.setDateOfAccInfo(mfiDomain.getRptDate());
				//actCrd.getGroupIdentifier().isEmpty()
				
				//accountValidationData = "Invalid Account Data";
				
				//mfiDomain.setReasonForFailure("Invalid Account Data");
				if(null == mfiDomain.getReasonForFailure()){
					mfiDomain.setReasonForFailure("Invalid Account Data");
					
					}else{
					mfiDomain.setReasonForFailure(mfiDomain.getReasonForFailure().concat(", Invalid Account Data"));
					}
					
		}
		
		
		System.out.println("Invalid account");
		return mfiDomain.getReasonForFailure();
	}
	
	private static Boolean checkAccRefAndAccNumber(String number){
		
		if(!StringUtils.isBlank(number) && number != null && number.length() <= 35){
			return true;
		}
		return false;
		
	}
	
private static boolean checkBranchAndKendraIdentifierAndLoanOfficer(String number){
		
		if(!StringUtils.isBlank(number) && number != null && number.length() <= 30){
			return true;
		}
		return false;
		
	}

private static boolean checkDateOfAccInfo(String number) {
	if(!StringUtils.isBlank(number) && number != null && number.matches("\\d{8}")){
		return true;
	}
	return false;
}

private static boolean checkLoanCategory(String loanCat, String grpId) {
	Boolean indicator = false;
	if(!StringUtils.isBlank(loanCat) && loanCat != null && loanCat.matches("\\d{3}") ){
		
		for (LoanCategory l : LoanCategory.values()) {
	        if (l.name().equals(loanCat)) {
	        	if(loanCat.contains(MfiConstants.T01_ACCOUNT_LOAN_CATEGORY)
	        			|| loanCat.contains(MfiConstants.T02_ACCOUNT_LOAN_CATEGORY)){
	        		if(checkGroupIdentifierAndWriteOffReason(grpId)){
	        			indicator= true;
	        			}
	        	}
	        	
	           // return true;
	        }
	    }

	    //return false;		
	}
		/*if(loanCat.contains(MfiConstants.T01_ACCOUNT_LOAN_CATEGORY)
			|| loanCat.contains(MfiConstants.T02_ACCOUNT_LOAN_CATEGORY)
			){
			if(checkGroupIdentifier(grpId)){
		return true;
			}else{
				return false;
			}
		}
		if(loanCat.contains(MfiConstants.T03_ACCOUNT_LOAN_CATEGORY)){
			return true;
		}
	}
	return false;
	*/
	return indicator;
}

private static boolean checkGroupIdentifierAndWriteOffReason(String grpId) {
	if(!StringUtils.isBlank(grpId) && grpId != null ){
		return true;
	}
	return false;
	
}

private static boolean checkLoanCycleId(String loadId) {
	if(!StringUtils.isBlank(loadId) && loadId != null ){
		return true;
	}
	return false;
	
}

private static boolean checkLoanPurpose(String loanPurpose){
	if(!StringUtils.isBlank(loanPurpose) && loanPurpose != null ){
		return true;
	}
	return false;
	
}

private static boolean checkAccountStatus(String accountStatus, String date){
	Boolean indicator = false;
	if(!StringUtils.isBlank(accountStatus) && accountStatus != null ){
		
		for (AccountStatus a : AccountStatus.values()) {
	        if (a.name().equals(accountStatus)) {
	        	if(accountStatus.contains(MfiConstants.S06_ACCOUNT_STATUS) && checkDate(date)){
	        		
	        			indicator= true;
	      
	        	}
	        	
	           // return true;
	        }
	    }
		//return true;
	}
	return indicator;
	
}

private static boolean checkDate(String date){
	if(!StringUtils.isBlank(date) && date != null ){
		return true;
	}
	return false;
	
}

private static boolean checkAppliedForAmountAndLoanSanctioned(String amount){
	if(!StringUtils.isBlank(amount) && amount != null ){
		return true;
	}
	return false;
	
}

private static boolean checkTotalAmountDisbursed(String amount){
	if(!StringUtils.isBlank(amount) && amount != null ){
		return true;
	}
	return false;
	
}

private static boolean checkNumberOfInstallments(String installment){
	if(!StringUtils.isBlank(installment) && installment != null ){
		return true;
	}
	return false;
	
}

private static boolean checkRepaymentFreq(String repFreq){
	Boolean indicator = false;
	if(!StringUtils.isBlank(repFreq) && repFreq != null ){
		for (RepaymentFrequency r : RepaymentFrequency.values()) {
	        if (r.name().equals(repFreq)) {
	        	indicator= true;
	        			
	        	}
	        }
	    }
		
	return indicator;
	
}

private static boolean checkDueAmountAndCurrentBalanceAndAmountOverdue(String amt){
	if(!StringUtils.isBlank(amt) && amt != null ){
		return true;
	}
	return false;
	
}

private static boolean checkDaysPasseddue(String days){
	if(!StringUtils.isBlank(days) && days != null ){
		return true;
	}
	return false;
	
}

private static boolean checkWriteOffReason(String reason){
	Boolean indicator = false;
	if(!StringUtils.isBlank(reason) && reason != null ){
		for (WriteOffReason r : WriteOffReason.values()) {
	        if (r.name().equals(reason)) {
	        	indicator= true;
	        			
	        	}
	        }
	    }
		
	return indicator;
	
}

private static boolean checkMeetings(String days){
	if(!StringUtils.isBlank(days) && days != null ){
		return true;
	}
	return false;
	
}

private static boolean checkInsuranceInd(String ind){
	Boolean indicator = false;
	if(!StringUtils.isBlank(ind) && ind != null ){
		for (InsuranceIndicator i : InsuranceIndicator.values()) {
	        if (i.name().equals(ind)) {
	        	indicator = true;
	        			
	        	}
	        }
	}
	return indicator;
}

private static boolean checkTypeOfInsurance(String insurance){
	Boolean indicator = false;
	if(!StringUtils.isBlank(insurance) && insurance != null ){
		for (TypeOfInsurance i : TypeOfInsurance.values()) {
	        if (i.name().equals(insurance)) {
	        	indicator = true;
	        			
	        	}
	        }
	}
	return indicator;
	
}

private static boolean checkSumOfCoverage(String insurance){
	if(!StringUtils.isBlank(insurance) && insurance != null ){
		return true;
	}
	return false;
	
}

private static boolean checkAgreedMeetingday(String day){
	Boolean indicator = false;
	if(!StringUtils.isBlank(day) && day != null ){
		for (WeekDay w : WeekDay.values()) {
	        if (w.name().equals(day)) {
	        	indicator = true;
	        			
	        	}
	        }
	}
	return indicator;
	
}

private static boolean checkMeetingTime(String meetingTime){
	if(!StringUtils.isBlank(meetingTime) && meetingTime != null ){
		return true;
	}
	return false;
	
}

}
