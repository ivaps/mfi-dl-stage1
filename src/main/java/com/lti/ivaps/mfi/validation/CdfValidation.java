package com.lti.ivaps.mfi.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.lti.ivaps.mfi.ExperianCRC32;
import com.lti.ivaps.mfi.beans.Account;
import com.lti.ivaps.mfi.beans.Address;
import com.lti.ivaps.mfi.beans.Consumer;
import com.lti.ivaps.mfi.beans.MfiDomain;
import com.lti.ivaps.mfi.constans.MfiConstants;
import com.lti.ivaps.mfi.error.ErrorCodeList;
import com.lti.ivaps.mfi.exceptions.CdfValidationException;
import com.lti.ivaps.mfi.parser.CdfParser;
import com.lti.mfi.data.load.AccountBeanBuilder;
import com.lti.mfi.data.load.AddressBeanBuilder;
import com.lti.mfi.data.load.ConsumerBeanBuilder;

//import scala.collection.Iterable;
/*import scala.collection.Set;
import scala.collection.mutable.HashMap;*/

public class CdfValidation {
	
	static  Logger log = Logger.getLogger(CdfValidation.class);
	
	private static void validateField (MfiDomain mfiDomain, String errorCode, String fieldValue, Boolean blankCheck, Boolean integerCheck, Boolean numericCheck, Boolean dateCheck, Integer minLength, Integer maxLength) {
		
		if (!GenericValidator.validateMe(fieldValue, blankCheck, integerCheck, numericCheck, dateCheck, minLength, maxLength)) {
			mfiDomain.setValid(false);
			mfiDomain.addErrorCode(errorCode);
		}
	}
	
	private static void validateOneOfFieldOfAddress(MfiDomain mfiDomain, HashMap<String, String> fieldValues, Boolean blankCheck, Boolean integerCheck, Boolean numericCheck, Boolean dateCheck, Integer minLength, Integer maxLength, Address address) {
		Set<String> keySet = fieldValues.keySet();
		
		for (String key : keySet) {
			validateField(mfiDomain, key, fieldValues.get(key), blankCheck, integerCheck, numericCheck, dateCheck, minLength, maxLength);
		}
			
		}
		
		//Boolean addressIndicator = true;
		//String errorCode1;
	/*	for (String fieldValue : fieldValues) {
			if (!GenericValidator.validateMe(fieldValue, blankCheck, integerCheck, numericCheck, dateCheck, minLength, maxLength) && StringUtils.isNotBlank(fieldValue)) {
				mfiDomain.setValid(false);
				//address.setAddressValid(false);
				mfiDomain.addErrorCode(errorCode);
				//addressIndicator = true;
			}
			//	addressIndicator = addressIndicator && address.getAddressValid();
		}*/
		
	//	return addressIndicator || address.getAddressValid();
		
//		for (String key : fieldValues.keySet()) {
//		    System.out.println(key + ":" + fieldValues.get(key));
//		}
		
				
	/*	for (int i = 0; i < fieldValues.size(); i++) {
			Set<String> keySet = fieldValues.keySet();
			
			for (String key : keySet) {
				
			}
			validateField(mfiDomain, fieldValues.key, fieldValue, blankCheck, integerCheck, numericCheck, dateCheck, minLength, maxLength);
		}
		
	}*/
		
	
	//Modifies method
	/*private static void validateOneOfFieldOfAddress1(MfiDomain mfiDomain, String errorCode, Boolean blankCheck, Boolean integerCheck, Boolean numericCheck, Boolean dateCheck, Integer minLength, Integer maxLength, Address address) {
		//Boolean addressIndicator = true;
		//String errorCode1;
		if(StringUtils.isBlank(address.getPermanentAddress().concat(address.getPermanentAdrstateCode()).concat(address.getPermanentAdrpinCode())) 
					&& StringUtils.isBlank(address.getCurrentAddress().concat(address.getCurrentAdrstateCode()).concat(address.getCurrentAdrpinCode()))){
			address.setAddressValid(false);
			//mfiDomain.setValid(false);
			mfiDomain.addErrorCode("WADR");
		}
		
		if(StringUtils.isBlank(address.getPermanentAddress().concat(address.getPermanentAdrstateCode()).concat(address.getPermanentAdrpinCode())) 
				|| StringUtils.isBlank(address.getCurrentAddress().concat(address.getCurrentAdrstateCode()).concat(address.getCurrentAdrpinCode()))){
			validateField(mfiDomain, errorCode, address.getPermanentAddress().concat(address.getPermanentAdrstateCode()).concat(address.getPermanentAdrpinCode()), blankCheck, integerCheck, numericCheck, dateCheck, minLength, maxLength);
			validateField(mfiDomain, errorCode, address.getCurrentAddress().concat(address.getCurrentAdrstateCode()).concat(address.getCurrentAdrpinCode()), blankCheck, integerCheck, numericCheck, dateCheck, minLength, maxLength);
		}
	}*/
	
	private static void validateOneOfAccountSegment(MfiDomain mfiDomain, String errorCode, String fieldValue, Boolean blankCheck, Boolean integerCheck, Boolean numericCheck, Boolean dateCheck, Integer minLength, Integer maxLength, Account account) {
		if (!GenericValidator.validateMe(fieldValue, blankCheck, integerCheck, numericCheck, dateCheck, minLength, maxLength)) {
			account.setAccountValid(false);
			mfiDomain.addErrorCode(errorCode);
		}
	}
	
	private static void validateOneOfFieldOfConsumer(MfiDomain mfiDomain, String errorCode, List<String> listOfFieldValues, Boolean blankCheck, Boolean integerCheck, Boolean numericCheck, Boolean dateCheck, Integer minLength, Consumer consumer) {
	//	Boolean consumerIndicator = true;
		//String[] errorCode = {ErrorCodeList.EADR001, ErrorCodeList.EADR002};
		for (String fieldValueWithPipe : listOfFieldValues) {
			String[] fieldValue = StringUtils.split(fieldValueWithPipe, MfiConstants.splitDelimiterPipe);
			for (int i = 0; i < fieldValue.length; i++) {
				if (!GenericValidator.validateMe(fieldValue[i], blankCheck, integerCheck, numericCheck, dateCheck, minLength, 100)){
					if(!GenericValidator.validateMe(fieldValue[i+1], blankCheck, integerCheck, numericCheck, dateCheck, minLength, 3)){
						mfiDomain.setValid(false);
						mfiDomain.addErrorCode(errorCode);
					}
				}
				break;
			}
			
				
			//if(consumerIndicator){
			/*if (!GenericValidator.validateMe(fieldValue[1], blankCheck, integerCheck, numericCheck, dateCheck, minLength, 3)) {
				mfiDomain.setValid(false);
				//consumer.setConsumerValid(false);
				mfiDomain.addErrorCode(errorCode);
			}*/
			//}
		}
			
		//	addressIndicator = addressIndicator && address.getAddressValid();
		
		
		//return addressIndicator || consumer.getConsumerValid();
		
	}
	
	public static Boolean validateRecord(MfiDomain mfiDomain) {
		log.info("Starting CDF validation");
		validateAccountRecord(mfiDomain);
		validateAddressRecord(mfiDomain);
		validateConsumerRecord(mfiDomain);
		
		return mfiDomain.getValid();
	}
	
	public static void validateAccountRecord(MfiDomain mfiDomain){
		log.info("Starting account validation");
		List<Account> accountList = mfiDomain.getAccount();
		
		int noOfFailedAccountSegments = 0;
		
//			validateField(mfiDomain, ErrorCodeList.E008, account.getUniqueAccRefNumber(), true, false, false, false, null, 35);
//			validateField(mfiDomain, ErrorCodeList.E009, account.getAccNumber(), true, false, false, false, null, 35);
			if(mfiDomain.getValid()){
				if(accountList.size() == 1 ){
					Account account = accountList.get(0);
				validateField(mfiDomain,ErrorCodeList.EACT002,account.getUniqueAccRefNumber(),true,false,false,false,null,35);
				validateField(mfiDomain,ErrorCodeList.EACT003,account.getAccNumber(),true,false,false,false,null,35);
				validateField(mfiDomain,ErrorCodeList.EACT004,account.getBranchIdentifier(),true,false,false,false,null,30);
				validateField(mfiDomain,ErrorCodeList.EACT005,account.getKendraCentreIdentifier(),true,false,false,false,null,30);
				validateField(mfiDomain,ErrorCodeList.EACT006,account.getLoanOfficerForOriginatingTheLoan(),true,false,false,false,null,30);
				validateField(mfiDomain,ErrorCodeList.EACT007,account.getDateOfAccInfo(),true,false,false,true,null,8);
				validateField(mfiDomain,ErrorCodeList.EACT008,account.getLoanCategory(),true,false,false,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.EACT009,account.getGroupIdentifier(),false,false,false,false,null,50);
//				validateField(mfiDomain,ErrorCodeList.EACT010,account.getLoanCycleId(),false,false,false,false,null,30);
				validateField(mfiDomain,ErrorCodeList.EACT011,account.getLoanPurpose(),true,false,false,false,null,20);
				validateField(mfiDomain,ErrorCodeList.EACT012,account.getAccStatus(),true,false,false,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.EACT013,account.getApplicationDate(),false,false,false,true,null,8);
//				validateField(mfiDomain,ErrorCodeList.EACT014,account.getSanctionedDate(),false,false,false,true,null,8);
				validateField(mfiDomain,ErrorCodeList.EACT015,account.getDateOpened(),true,false,false,true,null,8);
//				validateField(mfiDomain,ErrorCodeList.EACT016,account.getDateClosed(),false,false,false,true,null,8);
//				validateField(mfiDomain,ErrorCodeList.EACT017,account.getDateofLastPayment(),false,false,false,true,null,8);
//				validateField(mfiDomain,ErrorCodeList.EACT018,account.getAppliedForAmount(),false,false,true,false,null,9);
				validateField(mfiDomain,ErrorCodeList.EACT019,account.getLoanAmountSanctioned(),true,false,true,false,null,9);
				validateField(mfiDomain,ErrorCodeList.EACT020,account.getTotalAmountDisbursed(),true,false,true,false,null,9);
//				validateField(mfiDomain,ErrorCodeList.EACT021,account.getNumberOfInstallments(),false,false,true,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.EACT022,account.getRepaymentFrequency(),true,false,true,false,null,3);
				validateField(mfiDomain,ErrorCodeList.EACT023,account.getInstalmentAmount(),true,false,true,false,null,9);
				validateField(mfiDomain,ErrorCodeList.EACT024,account.getCurrentBalance(),true,false,true,false,null,9);
				validateField(mfiDomain,ErrorCodeList.EACT025,account.getAmountOverdue(),true,false,true,false,null,9);
//				validateField(mfiDomain,ErrorCodeList.EACT026,account.getDpd(),true,false,true,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.EACT027,account.getWriteOffAmount(),false,false,true,false,null,9);
//				validateField(mfiDomain,ErrorCodeList.EACT028,account.getDateWriteOff(),false,false,false,true,null,8);
//				validateField(mfiDomain,ErrorCodeList.EACT029,account.getWriteOffReason(),false,false,false,false,null,20);
//				validateField(mfiDomain,ErrorCodeList.EACT030,account.getNoOfMeetingsHeld(),false,true,false,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.EACT031,account.getNoOfMeetingsSkipped(),false,true,false,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.EACT032,account.getInsuranceIndicator(),false,false,false,false,null,1);
//				validateField(mfiDomain,ErrorCodeList.EACT033,account.getTypeOfInsurance(),false,false,false,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.EACT034,account.getSumAssured(),false,true,false,false,null,10);
//				validateField(mfiDomain,ErrorCodeList.EACT035,account.getAgreedMeetingDayOfTheWeek(),false,true,false,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.EACT036,account.getAgreedMeetingTimeOfTheDay(),false,true,false,false,null,5);
				
				}else{
					for (Account account : accountList) {
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT002,account.getUniqueAccRefNumber(),true,false,false,false,null,35,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT003,account.getAccNumber(),true,false,false,false,null,35,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT004,account.getBranchIdentifier(),true,false,false,false,null,30,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT005,account.getKendraCentreIdentifier(),true,false,false,false,null,30,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT006,account.getLoanOfficerForOriginatingTheLoan(),true,false,false,false,null,30,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT007,account.getDateOfAccInfo(),true,false,false,true,null,8,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT008,account.getLoanCategory(),true,false,false,false,null,3,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT011,account.getLoanPurpose(),true,false,false,false,null,20,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT012,account.getAccStatus(),true,false,false,false,null,3,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT015,account.getDateOpened(),true,false,false,true,null,8,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT019,account.getLoanAmountSanctioned(),true,false,true,false,null,9,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT020,account.getTotalAmountDisbursed(),true,false,true,false,null,9,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT023,account.getInstalmentAmount(),true,false,true,false,null,9,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT024,account.getCurrentBalance(),true,false,true,false,null,9,account);
					validateOneOfAccountSegment(mfiDomain,ErrorCodeList.EACT025,account.getAmountOverdue(),true,false,true,false,null,9,account);
					
					if(!account.getAccountValid()){
						noOfFailedAccountSegments++;
					}
				}
					
					if(accountList.size() == noOfFailedAccountSegments){
						mfiDomain.setValid(false);
					}
				}
			}
		
	
	}
	
	public static void validateAddressRecord(MfiDomain mfiDomain) {
		
		List<Address> addressList = mfiDomain.getAddress();
		String permanentAddress = null;
		String currentAddress = null;
		HashMap<String, String> listOfAddress = new HashMap();
		
	if(mfiDomain.getValid()){
		if(addressList.size() > 1){
		for (Address address : addressList){
			//validateField(mfiDomain, ErrorCodeList.E010, address.getPermanentAddress(), true, false, false, false, null, 200);
			//validateField(mfiDomain, ErrorCodeList.E012, address.getPermanentAdrpinCode(), true, false, false, false, null, 10);
			//String permanentAddress = null;
			//String currentAddress = null;
			
			permanentAddress = address.getPermanentAddress().concat(address.getPermanentAdrstateCode()).concat(address.getPermanentAdrpinCode());
			currentAddress = address.getCurrentAddress().concat(address.getCurrentAdrstateCode()).concat(address.getCurrentAdrpinCode());
				if(StringUtils.isBlank(permanentAddress) && StringUtils.isBlank(currentAddress)){
				address.setAddressValid(false);
				//mfiDomain.setValid(false);
				mfiDomain.addErrorCode(MfiConstants.WARNING_ADDRESS);
			}
			
			if(StringUtils.isBlank(permanentAddress) || StringUtils.isBlank(currentAddress)){
				
				listOfAddress.put(ErrorCodeList.EADR001, permanentAddress);
				listOfAddress.put(ErrorCodeList.EADR002, currentAddress);
				/*List<String> listOfAddress = new ArrayList();
				listOfAddress.add(0, permanentAddress);
				listOfAddress.add(1, currentAddress);*/
				validateOneOfFieldOfAddress(mfiDomain,  listOfAddress, false, false, false, false, null, 212, address);
			}
			}
		}else{
			permanentAddress = addressList.get(0).getPermanentAddress().concat(addressList.get(0).getPermanentAdrstateCode()).concat(addressList.get(0).getPermanentAdrpinCode());
			currentAddress = addressList.get(0).getCurrentAddress().concat(addressList.get(0).getCurrentAdrstateCode()).concat(addressList.get(0).getCurrentAdrpinCode());
			if(StringUtils.isBlank(permanentAddress) && StringUtils.isBlank(currentAddress)){
				addressList.get(0).setAddressValid(false);
				mfiDomain.setValid(false);
				mfiDomain.addErrorCode(ErrorCodeList.EADR000);
			}else{
				listOfAddress.put(ErrorCodeList.EADR001, permanentAddress);
				listOfAddress.put(ErrorCodeList.EADR002, currentAddress);
				
				validateOneOfFieldOfAddress(mfiDomain,  listOfAddress, false, false, false, false, null, 212, addressList.get(0));
			}
			
		}
	
	/*if(addressList.size() > 1){
		for (Address address : addressList){
			validateOneOfFieldOfAddress1(mfiDomain, ErrorCodeList.EADR000, false, false, false, false, null, 212, address);
		}
	}else{
		validateOneOfFieldOfAddress1(mfiDomain, ErrorCodeList.EADR000, true, false, false, false, null, 212, addressList.get(0));
	}*/
	}
			
			//validateOneOfFieldForAddress(mfiDomain, listOfAddress, true, false, false, false, null, 212, address);
			
			/*validateField(mfiDomain, ErrorCodeList.EADR001, permanentAddress, true, false, false, false, null, 212);
			validateField(mfiDomain, ErrorCodeList.EADR002, currentAddress, true, false, false, false, null, 212);*/
		
			/*validateField(mfiDomain, ErrorCodeList.EADR001, address.getPermanentAddress(), false, false, false, false, null, 200);
			validateField(mfiDomain, ErrorCodeList.EADR001, address.getPermanentAdrstateCode(), false, false, false, false, null, 2);
			validateField(mfiDomain, ErrorCodeList.EADR001, address.getPermanentAdrpinCode(), false, false, false, false, null, 10);
			validateField(mfiDomain, ErrorCodeList.EADR002, address.getCurrentAddress(), true, false, false, false, null, 200);
			validateField(mfiDomain, ErrorCodeList.EADR002, address.getCurrentAdrstateCode(), true, false, false, false, null, 2);
			validateField(mfiDomain, ErrorCodeList.EADR002, address.getCurrentAdrpinCode(), true, true, false, false, null, 10);*/
			/*if(address.getAddressValid()){
				addressList.add(address);
			}*/
		
		//return addressList;
		
	}
	
	public static void validateConsumerRecord(MfiDomain mfiDomain){
		
		Consumer consumer = mfiDomain.getConsumer();
		//validateField(mfiDomain, ErrorCodeList.E016, consumer.getMemberFirstName(), true, false, false, false, 3, 100);
		if(mfiDomain.getValid()){
			if((StringUtils.isNotBlank(consumer.getMemberDOB()) || (StringUtils.isNotBlank(consumer.getMemberAge()) && StringUtils.isNotBlank(consumer.getMemberAgeAsOnDate())))
					&& ((StringUtils.isNotBlank(consumer.getKeyPersonName()) && StringUtils.isNotBlank(consumer.getKeyPersonRelnship()))
					|| (StringUtils.isNotBlank(consumer.getRelativeNameOne()) && StringUtils.isNotBlank(consumer.getRelativeRelnshipOne()))
					|| (StringUtils.isNotBlank(consumer.getRelativeNameTwo()) && StringUtils.isNotBlank(consumer.getRelativeRelnshipTwo()))
					|| (StringUtils.isNotBlank(consumer.getRelativeNameThree()) && StringUtils.isNotBlank(consumer.getRelativeRelnshipThree()))
					|| (StringUtils.isNotBlank(consumer.getRelativeNameFour()) && StringUtils.isNotBlank(consumer.getRelativeRelnshipFour())))
							){
				validateField(mfiDomain,ErrorCodeList.ECNS002,consumer.getMemberIdentifier(),true,false,false,false,null,35);
				validateField(mfiDomain,ErrorCodeList.ECNS003,consumer.getBranchIdentifier(),true,false,false,false,null,30);
				validateField(mfiDomain,ErrorCodeList.ECNS004,consumer.getCentreIdentifier(),true,false,false,false,null,30);
//				validateField(mfiDomain,ErrorCodeList.ECNS005,consumer.getGroupIdentifier(),false,false,false,false,null,50);
				validateField(mfiDomain,ErrorCodeList.ECNS006,consumer.getMemberFirstName(),true,false,false,false,null,100);
//				validateField(mfiDomain,ErrorCodeList.ECNS007,consumer.getMemberMiddleName(),false,false,false,false,null,50);
//				validateField(mfiDomain,ErrorCodeList.ECNS008,consumer.getMemberLastName(),false,false,false,false,null,50);
//				validateField(mfiDomain,ErrorCodeList.ECNS009,consumer.getMemberAltName(),false,false,false,false,null,50);
				validateField(mfiDomain,ErrorCodeList.ECNS010,consumer.getMemberDOB(),false,false,false,true,null,8);
				validateField(mfiDomain,ErrorCodeList.ECNS011,consumer.getMemberAge(),false,true,false,false,null,3);
				validateField(mfiDomain,ErrorCodeList.ECNS012,consumer.getMemberAgeAsOnDate(),false,true,false,false,null,8);
				validateField(mfiDomain,ErrorCodeList.ECNS013,consumer.getMemberGender(),true,false,false,false,null,1);
//				validateField(mfiDomain,ErrorCodeList.ECNS014,consumer.getMaritalStatus(),false,false,false,false,null,3);
				String keyPerson = consumer.getKeyPersonName() +"|"+ consumer.getKeyPersonRelnship();
				String relativeOne = consumer.getRelativeNameOne() +"|" + consumer.getRelativeRelnshipOne();
				String relativeTwo = consumer.getRelativeNameTwo() +"|" + consumer.getRelativeRelnshipTwo();
				String relativeThree = consumer.getRelativeNameThree() +"|" + consumer.getRelativeRelnshipThree();
				String relativeFour = consumer.getRelativeNameFour() +"|" + consumer.getRelativeRelnshipFour();
				
				List<String> listOfRelatives = new ArrayList();
				listOfRelatives.add(0, keyPerson);
				listOfRelatives.add(1, relativeOne);
				listOfRelatives.add(2, relativeTwo);
				listOfRelatives.add(3, relativeThree);
				listOfRelatives.add(4, relativeFour);
				
				validateOneOfFieldOfConsumer(mfiDomain, ErrorCodeList.ECNS015_024, listOfRelatives, false,false,false,false,null,consumer);
				/*validateField(mfiDomain,ErrorCodeList.ECNS015,consumer.getKeyPersonName(),true,false,false,false,null,100);
				validateField(mfiDomain,ErrorCodeList.ECNS016,consumer.getKeyPersonRelnship(),true,false,false,false,null,3);
				validateField(mfiDomain,ErrorCodeList.ECNS017,consumer.getRelativeNameOne(),true,false,false,false,null,100);
				validateField(mfiDomain,ErrorCodeList.ECNS018,consumer.getRelativeRelnshipOne(),true,false,false,false,null,3);
				validateField(mfiDomain,ErrorCodeList.ECNS019,consumer.getRelativeNameTwo(),true,false,false,false,null,100);
				validateField(mfiDomain,ErrorCodeList.ECNS020,consumer.getRelativeRelnshipTwo(),true,false,false,false,null,3);
				validateField(mfiDomain,ErrorCodeList.ECNS021,consumer.getRelativeNameThree(),true,false,false,false,null,100);
				validateField(mfiDomain,ErrorCodeList.ECNS022,consumer.getRelativeRelnshipThree(),true,false,false,false,null,3);
				validateField(mfiDomain,ErrorCodeList.ECNS023,consumer.getRelativeNameFour(),true,false,false,false,null,100);
				validateField(mfiDomain,ErrorCodeList.ECNS024,consumer.getRelativeRelnshipFour(),true,false,false,false,null,3);*/
//				validateField(mfiDomain,ErrorCodeList.ECNS025,consumer.getNomineeName(),false,false,false,false,null,100);
//				validateField(mfiDomain,ErrorCodeList.ECNS026,consumer.getNomineeRelnship(),false,false,false,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.ECNS027,consumer.getNomineeAge(),false,true,false,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.ECNS028,consumer.getVoterId(),false,false,false,false,null,20);
//				validateField(mfiDomain,ErrorCodeList.ECNS029,consumer.getUid(),false,false,false,false,null,40);
//				validateField(mfiDomain,ErrorCodeList.ECNS030,consumer.getPan(),false,false,false,false,null,15);
//				validateField(mfiDomain,ErrorCodeList.ECNS031,consumer.getRationCard(),false,false,false,false,null,20);
//				validateField(mfiDomain,ErrorCodeList.ECNS032,consumer.getOtrIdTypeOneDesc(),false,false,false,false,null,20);
//				validateField(mfiDomain,ErrorCodeList.ECNS033,consumer.getOtrIdTypeOneValue(),false,false,false,false,null,30);
//				validateField(mfiDomain,ErrorCodeList.ECNS034,consumer.getOtrIdTypeTwoDesc(),false,false,false,false,null,20);
//				validateField(mfiDomain,ErrorCodeList.ECNS035,consumer.getOtherIdTypeTwoValue(),false,false,false,false,null,30);
//				validateField(mfiDomain,ErrorCodeList.ECNS036,consumer.getOtrIdTypeThreeDesc(),false,false,false,false,null,20);
//				validateField(mfiDomain,ErrorCodeList.ECNS037,consumer.getOtherIdTypeThreeValue(),false,false,false,false,null,30);
//				validateField(mfiDomain,ErrorCodeList.ECNS038,consumer.getTelephNumberTypeOneInd(),false,false,false,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.ECNS039,consumer.getTelephNumberTypeOneValue(),false,false,false,false,null,15);
//				validateField(mfiDomain,ErrorCodeList.ECNS040,consumer.getTelephNumberTypeTwoInd(),false,false,false,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.ECNS041,consumer.getTelephNumberTypeTwoValue(),false,false,false,false,null,15);
//				validateField(mfiDomain,ErrorCodeList.ECNS042,consumer.getPovertyIndex(),true,false,false,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.ECNS043,consumer.getAssetOwnershipInd(),false,false,false,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.ECNS044,consumer.getNoOfDependents(),false,true,false,false,null,2);
//				validateField(mfiDomain,ErrorCodeList.ECNS045,consumer.getBankName(),true,false,false,false,null,50);
//				validateField(mfiDomain,ErrorCodeList.ECNS046,consumer.getBranchName(),true,false,false,false,null,50);
//				validateField(mfiDomain,ErrorCodeList.ECNS047,consumer.getAccountNumber(),true,false,false,false,null,35);
//				validateField(mfiDomain,ErrorCodeList.ECNS048,consumer.getOccupation(),false,false,false,false,null,50);
//				validateField(mfiDomain,ErrorCodeList.ECNS049,consumer.getTotalMonthlyIncome(),true,false,true,false,null,9);
//				validateField(mfiDomain,ErrorCodeList.ECNS050,consumer.getTotalMonthlyExpenses(),true,false,true,false,null,9);
//				validateField(mfiDomain,ErrorCodeList.ECNS051,consumer.getMemberReligion(),false,false,false,false,null,3);
//				validateField(mfiDomain,ErrorCodeList.ECNS052,consumer.getMemberCaste(),true,false,false,false,null,30);
//				validateField(mfiDomain,ErrorCodeList.ECNS053,consumer.getGroupLeaderIndicator(),false,false,false,false,null,1);
//				validateField(mfiDomain,ErrorCodeList.ECNS054,consumer.getCenterLeaderIndicator(),false,false,false,false,null,1);
			}
			
		}
		
	}
}

