package com.lti.ivaps.mfi.constans;

public interface MfiConstants {
	public static final String HDR = "HDR";
	public static final String CNSCRD = "CNSCRD";
	public static final String ADRCRD = "ADRCRD";
	public static final String ACTCRD = "ACTCRD";
	public static final String TRL = "TRL";
	
	public static final String splitDelimiter ="^#";
	public static final String splitDelimiterAmp = "&";
	public static final String splitDelimiterDollar = "$";
	public static final String splitDelimiterTilda = "~";
	public static final String splitDelimiterPipe = "|";
	public static final String splitDelimiterComma = ",";
	public static final String HEADERMFIID = "HeaderMfiId";
	public static final String HEADERRPTDT = "HeaderReportedDate";
	public static final String CONSUMER = "Consumer";
	public static final String ADDRESS = "Address";
	public static final String ACCOUNT = "Account";
	public static final String PHONE = "Phone";
	public static final String IDENTITY = "Identity";
	
	public static final String T01_ACCOUNT_LOAN_CATEGORY = "T01";
	public static final String T02_ACCOUNT_LOAN_CATEGORY = "T02";
	public static final String T03_ACCOUNT_LOAN_CATEGORY = "T03";
	
	
	public static final String A01_ACCOUNT_LOAN_PURPOSE = "A01";
	public static final String A02_ACCOUNT_LOAN_PURPOSE = "A02";
	public static final String A03_ACCOUNT_LOAN_PURPOSE = "A03";
	public static final String A04_ACCOUNT_LOAN_PURPOSE = "A04";
	public static final String A05_ACCOUNT_LOAN_PURPOSE = "A05";
	public static final String A06_ACCOUNT_LOAN_PURPOSE = "A06";
	public static final String A35_ACCOUNT_LOAN_PURPOSE = "A35";
	
	public static final String S01_ACCOUNT_STATUS = "S01";
	public static final String S02_ACCOUNT_STATUS = "S02";
	public static final String S03_ACCOUNT_STATUS = "S03";
	public static final String S04_ACCOUNT_STATUS = "S04";
	public static final String S05_ACCOUNT_STATUS = "S05";
	public static final String S06_ACCOUNT_STATUS = "S06";
	public static final String S07_ACCOUNT_STATUS = "S07";
	
	
	public static final String F01_ACCOUNT_REPAYMENT_FREQUENCY = "F01";
	public static final String F02_ACCOUNT_REPAYMENT_FREQUENCY = "F02";
	public static final String F03_ACCOUNT_REPAYMENT_FREQUENCY = "F03";
	public static final String F04_ACCOUNT_REPAYMENT_FREQUENCY = "F04";
	public static final String F05_ACCOUNT_REPAYMENT_FREQUENCY = "F05";
	public static final String F06_ACCOUNT_REPAYMENT_FREQUENCY = "F06";
	public static final String F07_ACCOUNT_REPAYMENT_FREQUENCY = "F07";
	public static final String F08_ACCOUNT_REPAYMENT_FREQUENCY = "F08";
	public static final String F10_ACCOUNT_REPAYMENT_FREQUENCY = "F09";
	
	public static final String X01_ACCOUNT_WRITE_OFF_REASON = "X01";
	public static final String X02_ACCOUNT_WRITE_OFF_REASON = "X02";
	public static final String X03_ACCOUNT_WRITE_OFF_REASON = "X03";
	public static final String X04_ACCOUNT_WRITE_OFF_REASON = "X04";
	public static final String X09_ACCOUNT_WRITE_OFF_REASON = "X05";
	public static final String X10_ACCOUNT_WRITE_OFF_REASON = "X06";
	
	
	public static final String L01_ACCOUNT_TYPE_OF_INSURANCE = "L01";
	public static final String L02_ACCOUNT_TYPE_OF_INSURANCE = "L02";
	public static final String L03_ACCOUNT_TYPE_OF_INSURANCE = "L03";
	public static final String L04_ACCOUNT_TYPE_OF_INSURANCE = "L04";
	public static final String L05_ACCOUNT_TYPE_OF_INSURANCE = "L05";
	public static final String L10_ACCOUNT_TYPE_OF_INSURANCE = "L06";
	
	public static final String MON = "MON";
	public static final String TUE = "TUE";
	public static final String WED = "WED";
	public static final String THU = "THU";
	public static final String FRI = "FRI";
	public static final String SAT = "SAT";
	public static final String SUN = "SUN";
	
	public static final String MFI_JOB_STATUS_FINISHED = "Finished";
	public static final String MFI_JOB_STATUS_FAILED = "Failed";
	
	public static final String WARNING_ADDRESS = "WADR";
	public static final String WARNING_ACCOUNT = "WACT";
	
		
	
}
