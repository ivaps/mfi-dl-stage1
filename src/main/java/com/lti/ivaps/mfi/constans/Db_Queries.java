package com.lti.ivaps.mfi.constans;

public class Db_Queries {
	public static String updateMfiJobTrackerStatus = "UPDATE mfi_job_tracker SET FINISHED_AT=now() WHERE JOB_ID=? AND MAXIQ_JOB_ID=?";
	//public static String updateMfiJobTrackerStatus = "UPDATE mfi_job_tracker SET FINISHED_AT=now(), STATUS=? WHERE JOB_ID=? AND MAXIQ_JOB_ID=?";
	//public static String updateMfiJobTrackerStatus = "UPDATE mfi_job_tracker SET FINISHED_AT=now() WHERE JOB_ID=?";
	public static String updateMfiProcessedFilesStatus = "UPDATE mfi_processed_files SET TOTAL_RECS=?, FAIL_RECS=?, PASS_RECS=?, PASSED_RES_PATH=?, FAILED_RES_PATH=?, PROFILING_RES_PATH=?, SUMMARY_RES_PATH=? WHERE FILE_ID=? AND STAGE=?";
	//public static String updateMfiProcessedFilesStatus = "UPDATE mfi_processed_files SET TOTAL_RECS=?, FAIL_RECS=?, PASS_RECS=?, PASSED_RES_PATH=?, FAILED_RES_PATH=?, PROFILING_RES_PATH=?, SUMMARY_RES_PATH=? WHERE FILE_ID=? AND STAGE=?";
	public static String getFileIDForProcessedFile = "select FILE_ID from mfi_job_tracker WHERE JOB_ID=? AND MAXIQ_JOB_ID=?";
	//public static String getFileIDForProcessedFile = "select FILE_ID from mfi_job_tracker WHERE JOB_ID=?";
}
