package com.lti.ivaps.mfi.beans;

import java.sql.Timestamp;

public class MfiProcessedFiles {
	
	private int file_id;
	private String file_name;
	private String mfi_id;
	private Timestamp file_arrival_dt;
	private Timestamp reported_dt;
	private String file_size;
	private String status;
	private String stage;
	private int total_recs;
	private int fail_recs;
	private int pass_recs;
	private String passed_res_path;
	private String failed_res_path;
	private String profiling_res_path;
	private String summary_res_path;
	
	public int getFile_id() {
		return file_id;
	}
	public void setFile_id(int file_id) {
		this.file_id = file_id;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getMfi_id() {
		return mfi_id;
	}
	public void setMfi_id(String mfi_id) {
		this.mfi_id = mfi_id;
	}
	public Timestamp getFile_arrival_dt() {
		return file_arrival_dt;
	}
	public void setFile_arrival_dt(Timestamp file_arrival_dt) {
		this.file_arrival_dt = file_arrival_dt;
	}
	public Timestamp getReported_dt() {
		return reported_dt;
	}
	public void setReported_dt(Timestamp reported_dt) {
		this.reported_dt = reported_dt;
	}
	public String getFile_size() {
		return file_size;
	}
	public void setFile_size(String file_size) {
		this.file_size = file_size;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}
	public int getTotal_recs() {
		return total_recs;
	}
	public void setTotal_recs(int total_recs) {
		this.total_recs = total_recs;
	}
	public int getFail_recs() {
		return fail_recs;
	}
	public void setFail_recs(int fail_recs) {
		this.fail_recs = fail_recs;
	}
	public int getPass_recs() {
		return pass_recs;
	}
	public void setPass_recs(int pass_recs) {
		this.pass_recs = pass_recs;
	}
	public String getPassed_res_path() {
		return passed_res_path;
	}
	public void setPassed_res_path(String passed_res_path) {
		this.passed_res_path = passed_res_path;
	}
	public String getFailed_res_path() {
		return failed_res_path;
	}
	public void setFailed_res_path(String failed_res_path) {
		this.failed_res_path = failed_res_path;
	}
	public String getProfiling_res_path() {
		return profiling_res_path;
	}
	public void setProfiling_res_path(String profiling_res_path) {
		this.profiling_res_path = profiling_res_path;
	}
	public String getSummary_res_path() {
		return summary_res_path;
	}
	public void setSummary_res_path(String summary_res_path) {
		this.summary_res_path = summary_res_path;
	}

}
