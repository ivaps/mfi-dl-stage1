package com.lti.ivaps.mfi.beans;

import java.io.Serializable;

public class Consumer implements Serializable{

	private String segmentIdentifier;
	private String memberIdentifier;
	private String branchIdentifier;
	private String centreIdentifier;
	private String groupIdentifier;
	private String memberFirstName;
	private String memberMiddleName;
	private String memberLastName;
	private String memberAltName;
	private String memberDOB;
	private String memberAge;
	private String memberAgeAsOnDate;
	private String memberGender;
	private String maritalStatus;
	private String keyPersonName;
	private String keyPersonRelnship;
	private String relativeNameOne;
	private String relativeRelnshipOne;
	private String relativeNameTwo;
	private String relativeRelnshipTwo;
	private String relativeNameThree;
	private String relativeRelnshipThree;
	private String relativeNameFour;
	private String relativeRelnshipFour;
	private String nomineeName;
	private String nomineeRelnship;
	private String nomineeAge;
	private String voterId;
	private String uid;
	private String pan;
	private String rationCard;
	private String otrIdTypeOneDesc;
	private String otrIdTypeOneValue;
	private String otrIdTypeTwoDesc;
	private String otherIdTypeTwoValue;
	private String otrIdTypeThreeDesc;
	private String otherIdTypeThreeValue;
	private String telephNumberTypeOneInd;
	private String telephNumberTypeOneValue;
	private String telephNumberTypeTwoInd;
	private String telephNumberTypeTwoValue;
	private String povertyIndex;
	private String assetOwnershipInd;
	private String noOfDependents;
	private String bankName;
	private String branchName;
	private String accountNumber;
	private String occupation;
	private String totalMonthlyIncome;
	private String totalMonthlyExpenses;
	private String memberCaste;
	private String memberReligion;
	private String groupLeaderIndicator;
	private String centerLeaderIndicator;
	private Boolean consumerValid = true;
	
	public Consumer() {
		// TODO Auto-generated constructor stub
	}

	public Consumer(String segmentIdentifier, String memberIdentifier, String branchIdentifier, String centreIdentifier,
			String groupIdentifier, String memberFirstName, String memberMiddleName, String memberLastName,
			String memberAltName, String memberDOB, String memberAge, String memberAgeAsOnDate, String memberGender,
			String maritalStatus, String keyPersonName, String keyPersonRelnship, String relativeNameOne,
			String relativeRelnshipOne, String relativeNameTwo, String relativeRelnshipTwo, String relativeNameThree,
			String relativeRelnshipThree, String relativeNameFour, String relativeRelnshipFour, String nomineeName,
			String nomineeRelnship, String nomineeAge, String voterId, String uid, String pan, String rationCard,
			String otrIdTypeOneDesc, String otrIdTypeOneValue, String otrIdTypeTwoDesc, String otherIdTypeTwoValue,
			String otrIdTypeThreeDesc, String otherIdTypeThreeValue, String telephNumberTypeOneInd,
			String telephNumberTypeOneValue, String telephNumberTypeTwoInd, String telephNumberTypeTwoValue,
			String povertyIndex, String assetOwnershipInd, String noOfDependents, String bankName, String branchName,
			String accountNumber, String occupation, String totalMonthlyIncome, String totalMonthlyExpenses,
			String memberCaste, String memberReligion, String groupLeaderIndicator, String centerLeaderIndicator,
			Boolean consumerValid) {
		super();
		this.segmentIdentifier = segmentIdentifier;
		this.memberIdentifier = memberIdentifier;
		this.branchIdentifier = branchIdentifier;
		this.centreIdentifier = centreIdentifier;
		this.groupIdentifier = groupIdentifier;
		this.memberFirstName = memberFirstName;
		this.memberMiddleName = memberMiddleName;
		this.memberLastName = memberLastName;
		this.memberAltName = memberAltName;
		this.memberDOB = memberDOB;
		this.memberAge = memberAge;
		this.memberAgeAsOnDate = memberAgeAsOnDate;
		this.memberGender = memberGender;
		this.maritalStatus = maritalStatus;
		this.keyPersonName = keyPersonName;
		this.keyPersonRelnship = keyPersonRelnship;
		this.relativeNameOne = relativeNameOne;
		this.relativeRelnshipOne = relativeRelnshipOne;
		this.relativeNameTwo = relativeNameTwo;
		this.relativeRelnshipTwo = relativeRelnshipTwo;
		this.relativeNameThree = relativeNameThree;
		this.relativeRelnshipThree = relativeRelnshipThree;
		this.relativeNameFour = relativeNameFour;
		this.relativeRelnshipFour = relativeRelnshipFour;
		this.nomineeName = nomineeName;
		this.nomineeRelnship = nomineeRelnship;
		this.nomineeAge = nomineeAge;
		this.voterId = voterId;
		this.uid = uid;
		this.pan = pan;
		this.rationCard = rationCard;
		this.otrIdTypeOneDesc = otrIdTypeOneDesc;
		this.otrIdTypeOneValue = otrIdTypeOneValue;
		this.otrIdTypeTwoDesc = otrIdTypeTwoDesc;
		this.otherIdTypeTwoValue = otherIdTypeTwoValue;
		this.otrIdTypeThreeDesc = otrIdTypeThreeDesc;
		this.otherIdTypeThreeValue = otherIdTypeThreeValue;
		this.telephNumberTypeOneInd = telephNumberTypeOneInd;
		this.telephNumberTypeOneValue = telephNumberTypeOneValue;
		this.telephNumberTypeTwoInd = telephNumberTypeTwoInd;
		this.telephNumberTypeTwoValue = telephNumberTypeTwoValue;
		this.povertyIndex = povertyIndex;
		this.assetOwnershipInd = assetOwnershipInd;
		this.noOfDependents = noOfDependents;
		this.bankName = bankName;
		this.branchName = branchName;
		this.accountNumber = accountNumber;
		this.occupation = occupation;
		this.totalMonthlyIncome = totalMonthlyIncome;
		this.totalMonthlyExpenses = totalMonthlyExpenses;
		this.memberCaste = memberCaste;
		this.memberReligion = memberReligion;
		this.groupLeaderIndicator = groupLeaderIndicator;
		this.centerLeaderIndicator = centerLeaderIndicator;
		this.consumerValid = consumerValid;
	}

	public String getSegmentIdentifier() {
		return segmentIdentifier;
	}

	public void setSegmentIdentifier(String segmentIdentifier) {
		this.segmentIdentifier = segmentIdentifier;
	}

	public String getMemberIdentifier() {
		return memberIdentifier;
	}

	public void setMemberIdentifier(String memberIdentifier) {
		this.memberIdentifier = memberIdentifier;
	}

	public String getBranchIdentifier() {
		return branchIdentifier;
	}

	public void setBranchIdentifier(String branchIdentifier) {
		this.branchIdentifier = branchIdentifier;
	}

	public String getCentreIdentifier() {
		return centreIdentifier;
	}

	public void setCentreIdentifier(String centreIdentifier) {
		this.centreIdentifier = centreIdentifier;
	}

	public String getGroupIdentifier() {
		return groupIdentifier;
	}

	public void setGroupIdentifier(String groupIdentifier) {
		this.groupIdentifier = groupIdentifier;
	}

	public String getMemberFirstName() {
		return memberFirstName;
	}

	public void setMemberFirstName(String memberFirstName) {
		this.memberFirstName = memberFirstName;
	}

	public String getMemberMiddleName() {
		return memberMiddleName;
	}

	public void setMemberMiddleName(String memberMiddleName) {
		this.memberMiddleName = memberMiddleName;
	}

	public String getMemberLastName() {
		return memberLastName;
	}

	public void setMemberLastName(String memberLastName) {
		this.memberLastName = memberLastName;
	}

	public String getMemberAltName() {
		return memberAltName;
	}

	public void setMemberAltName(String memberAltName) {
		this.memberAltName = memberAltName;
	}

	public String getMemberDOB() {
		return memberDOB;
	}

	public void setMemberDOB(String memberDOB) {
		this.memberDOB = memberDOB;
	}

	public String getMemberAge() {
		return memberAge;
	}

	public void setMemberAge(String memberAge) {
		this.memberAge = memberAge;
	}

	public String getMemberAgeAsOnDate() {
		return memberAgeAsOnDate;
	}

	public void setMemberAgeAsOnDate(String memberAgeAsOnDate) {
		this.memberAgeAsOnDate = memberAgeAsOnDate;
	}

	public String getMemberGender() {
		return memberGender;
	}

	public void setMemberGender(String memberGender) {
		this.memberGender = memberGender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getKeyPersonName() {
		return keyPersonName;
	}

	public void setKeyPersonName(String keyPersonName) {
		this.keyPersonName = keyPersonName;
	}

	public String getKeyPersonRelnship() {
		return keyPersonRelnship;
	}

	public void setKeyPersonRelnship(String keyPersonRelnship) {
		this.keyPersonRelnship = keyPersonRelnship;
	}

	public String getRelativeNameOne() {
		return relativeNameOne;
	}

	public void setRelativeNameOne(String relativeNameOne) {
		this.relativeNameOne = relativeNameOne;
	}

	public String getRelativeRelnshipOne() {
		return relativeRelnshipOne;
	}

	public void setRelativeRelnshipOne(String relativeRelnshipOne) {
		this.relativeRelnshipOne = relativeRelnshipOne;
	}

	public String getRelativeNameTwo() {
		return relativeNameTwo;
	}

	public void setRelativeNameTwo(String relativeNameTwo) {
		this.relativeNameTwo = relativeNameTwo;
	}

	public String getRelativeRelnshipTwo() {
		return relativeRelnshipTwo;
	}

	public void setRelativeRelnshipTwo(String relativeRelnshipTwo) {
		this.relativeRelnshipTwo = relativeRelnshipTwo;
	}

	public String getRelativeNameThree() {
		return relativeNameThree;
	}

	public void setRelativeNameThree(String relativeNameThree) {
		this.relativeNameThree = relativeNameThree;
	}

	public String getRelativeRelnshipThree() {
		return relativeRelnshipThree;
	}

	public void setRelativeRelnshipThree(String relativeRelnshipThree) {
		this.relativeRelnshipThree = relativeRelnshipThree;
	}

	public String getRelativeNameFour() {
		return relativeNameFour;
	}

	public void setRelativeNameFour(String relativeNameFour) {
		this.relativeNameFour = relativeNameFour;
	}

	public String getRelativeRelnshipFour() {
		return relativeRelnshipFour;
	}

	public void setRelativeRelnshipFour(String relativeRelnshipFour) {
		this.relativeRelnshipFour = relativeRelnshipFour;
	}

	public String getNomineeName() {
		return nomineeName;
	}

	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	public String getNomineeRelnship() {
		return nomineeRelnship;
	}

	public void setNomineeRelnship(String nomineeRelnship) {
		this.nomineeRelnship = nomineeRelnship;
	}

	public String getNomineeAge() {
		return nomineeAge;
	}

	public void setNomineeAge(String nomineeAge) {
		this.nomineeAge = nomineeAge;
	}

	public String getVoterId() {
		return voterId;
	}

	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getRationCard() {
		return rationCard;
	}

	public void setRationCard(String rationCard) {
		this.rationCard = rationCard;
	}

	public String getOtrIdTypeOneDesc() {
		return otrIdTypeOneDesc;
	}

	public void setOtrIdTypeOneDesc(String otrIdTypeOneDesc) {
		this.otrIdTypeOneDesc = otrIdTypeOneDesc;
	}

	public String getOtrIdTypeOneValue() {
		return otrIdTypeOneValue;
	}

	public void setOtrIdTypeOneValue(String otrIdTypeOneValue) {
		this.otrIdTypeOneValue = otrIdTypeOneValue;
	}

	public String getOtrIdTypeTwoDesc() {
		return otrIdTypeTwoDesc;
	}

	public void setOtrIdTypeTwoDesc(String otrIdTypeTwoDesc) {
		this.otrIdTypeTwoDesc = otrIdTypeTwoDesc;
	}

	public String getOtherIdTypeTwoValue() {
		return otherIdTypeTwoValue;
	}

	public void setOtherIdTypeTwoValue(String otherIdTypeTwoValue) {
		this.otherIdTypeTwoValue = otherIdTypeTwoValue;
	}

	public String getOtrIdTypeThreeDesc() {
		return otrIdTypeThreeDesc;
	}

	public void setOtrIdTypeThreeDesc(String otrIdTypeThreeDesc) {
		this.otrIdTypeThreeDesc = otrIdTypeThreeDesc;
	}

	public String getOtherIdTypeThreeValue() {
		return otherIdTypeThreeValue;
	}

	public void setOtherIdTypeThreeValue(String otherIdTypeThreeValue) {
		this.otherIdTypeThreeValue = otherIdTypeThreeValue;
	}

	public String getTelephNumberTypeOneInd() {
		return telephNumberTypeOneInd;
	}

	public void setTelephNumberTypeOneInd(String telephNumberTypeOneInd) {
		this.telephNumberTypeOneInd = telephNumberTypeOneInd;
	}

	public String getTelephNumberTypeOneValue() {
		return telephNumberTypeOneValue;
	}

	public void setTelephNumberTypeOneValue(String telephNumberTypeOneValue) {
		this.telephNumberTypeOneValue = telephNumberTypeOneValue;
	}

	public String getTelephNumberTypeTwoInd() {
		return telephNumberTypeTwoInd;
	}

	public void setTelephNumberTypeTwoInd(String telephNumberTypeTwoInd) {
		this.telephNumberTypeTwoInd = telephNumberTypeTwoInd;
	}

	public String getTelephNumberTypeTwoValue() {
		return telephNumberTypeTwoValue;
	}

	public void setTelephNumberTypeTwoValue(String telephNumberTypeTwoValue) {
		this.telephNumberTypeTwoValue = telephNumberTypeTwoValue;
	}

	public String getPovertyIndex() {
		return povertyIndex;
	}

	public void setPovertyIndex(String povertyIndex) {
		this.povertyIndex = povertyIndex;
	}

	public String getAssetOwnershipInd() {
		return assetOwnershipInd;
	}

	public void setAssetOwnershipInd(String assetOwnershipInd) {
		this.assetOwnershipInd = assetOwnershipInd;
	}

	public String getNoOfDependents() {
		return noOfDependents;
	}

	public void setNoOfDependents(String noOfDependents) {
		this.noOfDependents = noOfDependents;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getTotalMonthlyIncome() {
		return totalMonthlyIncome;
	}

	public void setTotalMonthlyIncome(String totalMonthlyIncome) {
		this.totalMonthlyIncome = totalMonthlyIncome;
	}

	public String getTotalMonthlyExpenses() {
		return totalMonthlyExpenses;
	}

	public void setTotalMonthlyExpenses(String totalMonthlyExpenses) {
		this.totalMonthlyExpenses = totalMonthlyExpenses;
	}

	public String getMemberCaste() {
		return memberCaste;
	}

	public void setMemberCaste(String memberCaste) {
		this.memberCaste = memberCaste;
	}

	public String getMemberReligion() {
		return memberReligion;
	}

	public void setMemberReligion(String memberReligion) {
		this.memberReligion = memberReligion;
	}

	public String getGroupLeaderIndicator() {
		return groupLeaderIndicator;
	}

	public void setGroupLeaderIndicator(String groupLeaderIndicator) {
		this.groupLeaderIndicator = groupLeaderIndicator;
	}

	public String getCenterLeaderIndicator() {
		return centerLeaderIndicator;
	}

	public void setCenterLeaderIndicator(String centerLeaderIndicator) {
		this.centerLeaderIndicator = centerLeaderIndicator;
	}

	public Boolean getConsumerValid() {
		return consumerValid;
	}

	public void setConsumerValid(Boolean consumerValid) {
		this.consumerValid = consumerValid;
	}

	@Override
	public String toString() {
		return "Consumer [segmentIdentifier=" + segmentIdentifier + ", memberIdentifier=" + memberIdentifier
				+ ", branchIdentifier=" + branchIdentifier + ", centreIdentifier=" + centreIdentifier
				+ ", groupIdentifier=" + groupIdentifier + ", memberFirstName=" + memberFirstName
				+ ", memberMiddleName=" + memberMiddleName + ", memberLastName=" + memberLastName + ", memberAltName="
				+ memberAltName + ", memberDOB=" + memberDOB + ", memberAge=" + memberAge + ", memberAgeAsOnDate="
				+ memberAgeAsOnDate + ", memberGender=" + memberGender + ", maritalStatus=" + maritalStatus
				+ ", keyPersonName=" + keyPersonName + ", keyPersonRelnship=" + keyPersonRelnship + ", relativeNameOne="
				+ relativeNameOne + ", relativeRelnshipOne=" + relativeRelnshipOne + ", relativeNameTwo="
				+ relativeNameTwo + ", relativeRelnshipTwo=" + relativeRelnshipTwo + ", relativeNameThree="
				+ relativeNameThree + ", relativeRelnshipThree=" + relativeRelnshipThree + ", relativeNameFour="
				+ relativeNameFour + ", relativeRelnshipFour=" + relativeRelnshipFour + ", nomineeName=" + nomineeName
				+ ", nomineeRelnship=" + nomineeRelnship + ", nomineeAge=" + nomineeAge + ", voterId=" + voterId
				+ ", uid=" + uid + ", pan=" + pan + ", rationCard=" + rationCard + ", otrIdTypeOneDesc="
				+ otrIdTypeOneDesc + ", otrIdTypeOneValue=" + otrIdTypeOneValue + ", otrIdTypeTwoDesc="
				+ otrIdTypeTwoDesc + ", otherIdTypeTwoValue=" + otherIdTypeTwoValue + ", otrIdTypeThreeDesc="
				+ otrIdTypeThreeDesc + ", otherIdTypeThreeValue=" + otherIdTypeThreeValue + ", telephNumberTypeOneInd="
				+ telephNumberTypeOneInd + ", telephNumberTypeOneValue=" + telephNumberTypeOneValue
				+ ", telephNumberTypeTwoInd=" + telephNumberTypeTwoInd + ", telephNumberTypeTwoValue="
				+ telephNumberTypeTwoValue + ", povertyIndex=" + povertyIndex + ", assetOwnershipInd="
				+ assetOwnershipInd + ", noOfDependents=" + noOfDependents + ", bankName=" + bankName + ", branchName="
				+ branchName + ", accountNumber=" + accountNumber + ", occupation=" + occupation
				+ ", totalMonthlyIncome=" + totalMonthlyIncome + ", totalMonthlyExpenses=" + totalMonthlyExpenses
				+ ", memberCaste=" + memberCaste + ", memberReligion=" + memberReligion + ", groupLeaderIndicator="
				+ groupLeaderIndicator + ", centerLeaderIndicator=" + centerLeaderIndicator + ", consumerValid="
				+ consumerValid + "]";
	}

}
