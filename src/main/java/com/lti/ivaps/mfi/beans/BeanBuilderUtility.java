package com.lti.ivaps.mfi.beans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.augmentiq.maxiq.extenation.Validation;
import com.lti.ivaps.mfi.error.ErrorCodeList;
import com.lti.ivaps.mfi.exceptions.CdfValidationException;
import com.lti.ivaps.mfi.validation.AccountValidation;
import com.lti.ivaps.mfi.validation.AddressValidation;
import com.lti.ivaps.mfi.validation.CdfValidation;
import com.lti.ivaps.mfi.validation.GenericValidator;

public class BeanBuilderUtility {

	GenericValidator genericValidator = new GenericValidator();
	CdfValidation cdfValidation = new CdfValidation();

	public static Consumer buildConsumerBean(String[] consumerSegmentData) {
		Consumer consumer = new Consumer();
		// List<Consumer> listConsumer = new ArrayList<Consumer>();

		consumer.setSegmentIdentifier(consumerSegmentData[0]);
		consumer.setMemberIdentifier(consumerSegmentData[1]);
		consumer.setBranchIdentifier(consumerSegmentData[2]);
		consumer.setCentreIdentifier(consumerSegmentData[3]);
		consumer.setGroupIdentifier(consumerSegmentData[4]);
		consumer.setMemberFirstName(consumerSegmentData[5]);
		consumer.setMemberMiddleName(consumerSegmentData[6]);
		consumer.setMemberLastName(consumerSegmentData[7]);
		consumer.setMemberAltName(consumerSegmentData[8]);
		consumer.setMemberDOB(consumerSegmentData[9]);
		consumer.setMemberAge(consumerSegmentData[10]);
		consumer.setMemberAgeAsOnDate(consumerSegmentData[11]);
		consumer.setMemberGender(consumerSegmentData[12]);
		consumer.setMaritalStatus(consumerSegmentData[13]);
		consumer.setKeyPersonName(consumerSegmentData[14]);
		consumer.setKeyPersonRelnship(consumerSegmentData[15]);
		consumer.setRelativeNameOne(consumerSegmentData[16]);
		consumer.setRelativeRelnshipOne(consumerSegmentData[17]);
		consumer.setRelativeNameTwo(consumerSegmentData[18]);
		consumer.setRelativeRelnshipTwo(consumerSegmentData[19]);
		consumer.setRelativeNameThree(consumerSegmentData[20]);
		consumer.setRelativeRelnshipThree(consumerSegmentData[21]);
		consumer.setRelativeNameFour(consumerSegmentData[22]);
		consumer.setRelativeRelnshipFour(consumerSegmentData[23]);
		consumer.setNomineeName(consumerSegmentData[24]);
		consumer.setNomineeRelnship(consumerSegmentData[25]);
		consumer.setNomineeAge(consumerSegmentData[26]);
		consumer.setVoterId(consumerSegmentData[27]);
		consumer.setUid(consumerSegmentData[28]);
		consumer.setPan(consumerSegmentData[29]);
		consumer.setRationCard(consumerSegmentData[30]);
		consumer.setOtrIdTypeOneDesc(consumerSegmentData[31]);
		consumer.setOtrIdTypeOneValue(consumerSegmentData[32]);
		consumer.setOtrIdTypeTwoDesc(consumerSegmentData[33]);
		consumer.setOtherIdTypeTwoValue(consumerSegmentData[34]);
		consumer.setOtrIdTypeThreeDesc(consumerSegmentData[35]);
		consumer.setOtherIdTypeThreeValue(consumerSegmentData[36]);
		consumer.setTelephNumberTypeOneInd(consumerSegmentData[37]);
		consumer.setTelephNumberTypeOneValue(consumerSegmentData[38]);
		consumer.setTelephNumberTypeTwoInd(consumerSegmentData[39]);
		consumer.setTelephNumberTypeTwoValue(consumerSegmentData[40]);
		consumer.setPovertyIndex(consumerSegmentData[41]);
		consumer.setAssetOwnershipInd(consumerSegmentData[42]);
		consumer.setNoOfDependents(consumerSegmentData[43]);
		consumer.setBankName(consumerSegmentData[44]);
		consumer.setBranchName(consumerSegmentData[45]);
		consumer.setAccountNumber(consumerSegmentData[46]);
		consumer.setOccupation(consumerSegmentData[47]);
		consumer.setTotalMonthlyIncome(consumerSegmentData[48]);
		consumer.setTotalMonthlyExpenses(consumerSegmentData[49]);
		consumer.setMemberCaste(consumerSegmentData[50]);
		consumer.setMemberReligion(consumerSegmentData[51]);
		consumer.setGroupLeaderIndicator(consumerSegmentData[52]);
		consumer.setCenterLeaderIndicator(consumerSegmentData[53]);

		// listConsumer.add(consumer);

		return consumer;
	}

	public static List<Address> buildAddressBean(List<String[]> addressSegmentData) {

		List<Address> listAddress = new ArrayList<Address>();

		if (null == addressSegmentData || 0 == addressSegmentData.size()) {
			return listAddress;
		}

		for (String[] addressFields : addressSegmentData) {

			if (null != addressFields && 0 != addressFields.length) {
				Address address = new Address();

				address.setSegmentIdentifier(addressFields[0]);
				address.setPermanentAddress(addressFields[1]);
				address.setPermanentAdrstateCode(addressFields[2]);
				address.setPermanentAdrpinCode(addressFields[3]);
				address.setCurrentAddress(addressFields[4]);
				address.setCurrentAdrstateCode(addressFields[5]);
				address.setCurrentAdrpinCode(addressFields[6]);

				listAddress.add(address);
			}
		}

		return listAddress;
	}

	public static List<Account> buildAccountBean(List<String[]> accountSegmentData) {
		
		List<Account> listAccount = new ArrayList<Account>();

		if (null == accountSegmentData || 0 == accountSegmentData.size()) {
			return listAccount;
		}

		for (String[] accountFields : accountSegmentData) {

			if (null != accountFields && 0 != accountFields.length) {
				Account account = new Account();

				account.setSegmentIdentifier(accountFields[0]);
				account.setUniqueAccRefNumber(accountFields[1]);
				account.setAccNumber(accountFields[2]);
				account.setBranchIdentifier(accountFields[3]);
				account.setKendraCentreIdentifier(accountFields[4]);
				account.setLoanOfficerForOriginatingTheLoan(accountFields[5]);
				account.setDateOfAccInfo(accountFields[6]);
				account.setLoanCategory(accountFields[7]);
				account.setGroupIdentifier(accountFields[8]);
				account.setLoanCycleId(accountFields[9]);
				account.setLoanPurpose(accountFields[10]);
				account.setAccStatus(accountFields[11]);
				account.setApplicationDate(accountFields[12]);
				account.setSanctionedDate(accountFields[13]);
				account.setDateOpened(accountFields[14]);
				account.setDateClosed(accountFields[15]);
				account.setDateofLastPayment(accountFields[16]);
				account.setAppliedForAmount(accountFields[17]);
				account.setLoanAmountSanctioned(accountFields[18]);
				account.setTotalAmountDisbursed(accountFields[19]);
				account.setNumberOfInstallments(accountFields[20]);
				account.setRepaymentFrequency(accountFields[21]);
				account.setInstalmentAmount(accountFields[22]);
				account.setCurrentBalance(accountFields[23]);
				account.setAmountOverdue(accountFields[24]);
				account.setDpd(accountFields[25]);
				account.setWriteOffAmount(accountFields[26]);
				account.setDateWriteOff(accountFields[27]);
				account.setWriteOffReason(accountFields[28]);
				account.setNoOfMeetingsHeld(accountFields[29]);
				account.setNoOfMeetingsSkipped(accountFields[30]);
				account.setInsuranceIndicator(accountFields[31]);
				account.setTypeOfInsurance(accountFields[32]);
				account.setSumAssured(accountFields[33]);
				account.setAgreedMeetingDayOfTheWeek(accountFields[34]);
				account.setAgreedMeetingTimeOfTheDay(accountFields[35]);

				listAccount.add(account);
			}
		}

		return listAccount;
	}
}
