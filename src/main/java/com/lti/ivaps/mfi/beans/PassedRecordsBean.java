package com.lti.ivaps.mfi.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.lti.ivaps.mfi.constans.MfiConstants;
import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class PassedRecordsBean {
	private String mfiId_;
	private String memberId_;
	private String rptDate;
	//private static Boolean valid_;
	private String consumerSegmentStr_;
	private String addressSegmentStr_;
	private String accountSegmentStr_;
	private Long globalCrc_;
	
	public String getMfiId() {
		return mfiId_;
	}
	public void setMfiId(String mfiId) {
		mfiId_ = mfiId;
	}
	public String getMemberId() {
		return memberId_;
	}
	public void setMemberId(String memberId) {
		memberId_ = memberId;
	}
	public String getRptDate() {
		return rptDate;
	}
	public void setRptDate(String rptDate) {
		this.rptDate = rptDate;
	}
	public String getConsumerSegmentStr() {
		return consumerSegmentStr_;
	}
	public void setConsumerSegmentStr(String consumerSegmentStr) {
		consumerSegmentStr_ = consumerSegmentStr;
	}
	public String getAddressSegmentStr() {
		return addressSegmentStr_;
	}
	public void setAddressSegmentStr(String addressSegmentStr) {
		addressSegmentStr_ = addressSegmentStr;
	}
	public String getAccountSegmentStr() {
		return accountSegmentStr_;
	}
	public void setAccountSegmentStr(String accountSegmentStr) {
		accountSegmentStr_ = accountSegmentStr;
	}
	public Long getGlobalCrc() {
		return globalCrc_;
	}
	public void setGlobalCrc(Long globalCrc) {
		globalCrc_ = globalCrc;
	}
	
	
	@Override
	public String toString() {
		return "PassedRecordsBean [mfiId_=" + mfiId_ + ", memberId_=" + memberId_ + ", rptDate=" + rptDate
				+ ", consumerSegmentStr_=" + consumerSegmentStr_ + ", addressSegmentStr_=" + addressSegmentStr_
				+ ", accountSegmentStr_=" + accountSegmentStr_ + ", globalCrc_=" + globalCrc_ + "]";
	}
	
	public static PassedRecordsBean createPassedRecord(MfiDomain mfiDomain) {
		PassedRecordsBean passedRecordsBean = new PassedRecordsBean();
		if(mfiDomain.getValid() && !mfiDomain.getHeaderTrailerIndicator()){
			checkPassSegments(mfiDomain);
			passedRecordsBean.setMfiId(mfiDomain.getMfiId());
			passedRecordsBean.setMemberId(mfiDomain.getMemberId());
			passedRecordsBean.setRptDate(mfiDomain.getRptDate());
			passedRecordsBean.setConsumerSegmentStr(recordSplitterForConsumer(mfiDomain.getConsumerSegment()));
			passedRecordsBean.setAddressSegmentStr(recordSplitter(mfiDomain.getAddressSegment()));
			passedRecordsBean.setAccountSegmentStr(recordSplitter(mfiDomain.getAccountSegment()));
			passedRecordsBean.setGlobalCrc(mfiDomain.getGlobalCrc());
			
			
		}
		return passedRecordsBean;
		
	}
	
	public static MfiDomain checkPassSegments(MfiDomain mfiDomain){
		List<Account> accountList = mfiDomain.getAccount();
		List<Address> addressList = mfiDomain.getAddress();
		
		/*for (Address address : addressList) {
			if(!address.getAddressValid()){
				addressList.remove(address);
			}			
		}
		for (Account account : accountList) {
			if(!account.getAccountValid()){
				accountList.remove(account);
			}			
		}*/
		
		for (int i = 0; i < accountList.size(); i++) {
			if(!accountList.get(i).getAccountValid()){
				accountList.remove(i);
			}
		}
		
		for (int j = 0; j < addressList.size(); j++) {
			if(!addressList.get(j).getAddressValid()){
				addressList.remove(j);
			}
		}
		return mfiDomain;
		
	}
		//List<String[]> list
		public static String recordSplitter(List<String[]> segment) {
			
			StringBuffer outputSegment = new StringBuffer();
			for (String[] s : segment) {
				StringBuffer singletSegment = new StringBuffer();
				for (String string : s) {
					singletSegment.append(string).append(MfiConstants.splitDelimiterTilda);
				}
				outputSegment.append(singletSegment).append(MfiConstants.splitDelimiter);
			}
		
			//System.out.println("output segment" +StringUtils.removeEnd(outputSegment.toString(),MfiConstants.splitDelimiter));
			return StringUtils.removeEnd(outputSegment.toString(),MfiConstants.splitDelimiter);
		}
		
		public static String recordSplitterForConsumer(String[] segment) {
			
			StringBuffer outputSegment = new StringBuffer();
			for (String s : segment) {
				
				outputSegment.append(s).append(MfiConstants.splitDelimiterTilda);
			}
		
			//System.out.println("output segment" +StringUtils.removeEnd(outputSegment.toString(),MfiConstants.splitDelimiterTilda));
			return StringUtils.removeEnd(outputSegment.toString(),MfiConstants.splitDelimiterTilda);
		}
		
		public static void main(String[] args) {
			
			String[] a = {"CNSCRD", "772", "12th Cross", "7th Main","Shasr","Ko","ang","560047"};
			String[] b = {"ADRCRD", "772", "12th Cross", "","Shasr","Ko","ang","560047"};
			/*List<String[]> segment = new ArrayList<String[]>();
			segment.add(a);
			segment.add(b);
			recordSplitter(segment);*/
			//System.out.println("Result: " +recordSplitterForConsumer(a));
			
			//System.out.println("consumer output: " +Arrays.toString(a).replaceAll(",", "~"));
		}
}
