package com.lti.ivaps.mfi.beans;

import java.sql.Timestamp;

public class MfiJobTracker {
	private int fileId; 
	private int jobId; 
	private String jobStage; 
	private String status;
	private String maxiqJobId;
	private Timestamp startedAt;
	private String startedBy;
	private Timestamp finishedAt;
	private String abortedBy;
	private Timestamp abortedAt;
	
	public int getFileId() {
		return fileId;
	}
	public void setFileId(int fileId) {
		this.fileId = fileId;
	}
	public int getJobId() {
		return jobId;
	}
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}
	public String getJobStage() {
		return jobStage;
	}
	public void setJobStage(String jobStage) {
		this.jobStage = jobStage;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMaxiqJobId() {
		return maxiqJobId;
	}
	public void setMaxiqJobId(String maxiqJobId) {
		this.maxiqJobId = maxiqJobId;
	}
	public Timestamp getStartedAt() {
		return startedAt;
	}
	public void setStartedAt(Timestamp startedAt) {
		this.startedAt = startedAt;
	}
	public String getStartedBy() {
		return startedBy;
	}
	public void setStartedBy(String startedBy) {
		this.startedBy = startedBy;
	}
	public Timestamp getFinishedAt() {
		return finishedAt;
	}
	public void setFinishedAt(Timestamp finishedAt) {
		this.finishedAt = finishedAt;
	}
	public String getAbortedBy() {
		return abortedBy;
	}
	public void setAbortedBy(String abortedBy) {
		this.abortedBy = abortedBy;
	}
	public Timestamp getAbortedAt() {
		return abortedAt;
	}
	public void setAbortedAt(Timestamp abortedAt) {
		this.abortedAt = abortedAt;
	}
	
	public MfiJobTracker(int fileId, int jobId, String jobStage, String status, String maxiqJobId, Timestamp startedAt,
			String startedBy, Timestamp finishedAt, String abortedBy, Timestamp abortedAt) {
		super();
		this.fileId = fileId;
		this.jobId = jobId;
		this.jobStage = jobStage;
		this.status = status;
		this.maxiqJobId = maxiqJobId;
		this.startedAt = startedAt;
		this.startedBy = startedBy;
		this.finishedAt = finishedAt;
		this.abortedBy = abortedBy;
		this.abortedAt = abortedAt;
	}
	
	@Override
	public String toString() {
		return "MfiJobTracker [fileId=" + fileId + ", jobId=" + jobId + ", jobStage=" + jobStage + ", status=" + status
				+ ", maxiqJobId=" + maxiqJobId + ", startedAt=" + startedAt + ", startedBy=" + startedBy
				+ ", finishedAt=" + finishedAt + ", abortedBy=" + abortedBy + ", abortedAt=" + abortedAt + "]";
	}
	
	

	
}
