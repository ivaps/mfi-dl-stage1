package com.lti.ivaps.mfi.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.lti.ivaps.mfi.constans.MfiConstants;

public class Address implements Serializable {

	private String segmentIdentifier;
	private String permanentAddress;
	private String permanentAdrstateCode;
	private String permanentAdrpinCode;
	private String currentAddress;
	private String currentAdrstateCode;
	private String currentAdrpinCode;
	private Boolean addressValid = true;
	
	
	public Address() {
		// TODO Auto-generated constructor stub
	}
	
	public Address(String segmentIdentifier, String permanentAddress, String permanentAdrstateCode,
			String permanentAdrpinCode, String currentAddress, String currentAdrstateCode, String currentAdrpinCode,
			Boolean addressValid) {
		super();
		this.segmentIdentifier = segmentIdentifier;
		this.permanentAddress = permanentAddress;
		this.permanentAdrstateCode = permanentAdrstateCode;
		this.permanentAdrpinCode = permanentAdrpinCode;
		this.currentAddress = currentAddress;
		this.currentAdrstateCode = currentAdrstateCode;
		this.currentAdrpinCode = currentAdrpinCode;
		this.addressValid = addressValid;
	}
	
	


	public String getSegmentIdentifier() {
		return segmentIdentifier;
	}

	public void setSegmentIdentifier(String segmentIdentifier) {
		this.segmentIdentifier = segmentIdentifier;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentAdrstateCode() {
		return permanentAdrstateCode;
	}

	public void setPermanentAdrstateCode(String permanentAdrstateCode) {
		this.permanentAdrstateCode = permanentAdrstateCode;
	}

	public String getPermanentAdrpinCode() {
		return permanentAdrpinCode;
	}

	public void setPermanentAdrpinCode(String permanentAdrpinCode) {
		this.permanentAdrpinCode = permanentAdrpinCode;
	}

	public String getCurrentAddress() {
		return currentAddress;
	}

	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}

	public String getCurrentAdrstateCode() {
		return currentAdrstateCode;
	}

	public void setCurrentAdrstateCode(String currentAdrstateCode) {
		this.currentAdrstateCode = currentAdrstateCode;
	}

	public String getCurrentAdrpinCode() {
		return currentAdrpinCode;
	}

	public void setCurrentAdrpinCode(String currentAdrpinCode) {
		this.currentAdrpinCode = currentAdrpinCode;
	}

	public Boolean getAddressValid() {
		return addressValid;
	}

	public void setAddressValid(Boolean addressValid) {
		this.addressValid = addressValid;
	}
	

	@Override
	public String toString() {
		return "Address [segmentIdentifier=" + segmentIdentifier + ", permanentAddress=" + permanentAddress
				+ ", permanentAdrstateCode=" + permanentAdrstateCode + ", permanentAdrpinCode=" + permanentAdrpinCode
				+ ", currentAddress=" + currentAddress + ", currentAdrstateCode=" + currentAdrstateCode
				+ ", currentAdrpinCode=" + currentAdrpinCode + ", addressValid=" + addressValid + "]";
	}

	public static void main(String[] args) {
		String record = "ADRCRD~BALARAMPRASAD SIDHAMULA NAYGARH NAYGARH~AP~752069~~AP~~~$ADRCRD~aditya SIDHAMULA NAYGARH NAYGARH~AP~752069~~AP~~~$ADRCRD~JAM NAGAR~GJ~444606~~GJ~~~";
		String[] split =  record.split(MfiConstants.splitDelimiterDollar);
		String[] splitPreserveAllTokens2 = StringUtils.splitPreserveAllTokens(record, MfiConstants.splitDelimiterDollar);
		List<Address> listAddress = new ArrayList<Address>();
		for(String s : splitPreserveAllTokens2){
			//System.out.println(s);
			if(s.contains("ADRCRD")){
				Address address = new Address();
				String[] splitPreserveAllTokens = StringUtils.splitPreserveAllTokens(s, MfiConstants.splitDelimiterTilda);
				/*for(String token : splitPreserveAllTokens){
					System.out.println(token);
				}*/
				
				address.setSegmentIdentifier(splitPreserveAllTokens[0]);
				address.setPermanentAddress(splitPreserveAllTokens[1]);
				address.setPermanentAdrstateCode(splitPreserveAllTokens[2]);
				address.setPermanentAdrpinCode(splitPreserveAllTokens[3]);
				address.setCurrentAddress(splitPreserveAllTokens[4]);
				address.setCurrentAdrstateCode(splitPreserveAllTokens[5]);
				address.setCurrentAdrpinCode(splitPreserveAllTokens[6]);
				
				listAddress.add(address);
			}
		}
		
		for(Address a : listAddress){
			//System.out.println(a);
			
		}
		//System.out.println(address.toString());
	}
	
}


