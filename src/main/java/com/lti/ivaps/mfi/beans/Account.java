package com.lti.ivaps.mfi.beans;

import java.io.Serializable;

public class Account implements Serializable {
	
	private String segmentIdentifier;
	private String uniqueAccRefNumber;
	private String accNumber;
	private String branchIdentifier;
	private String kendraCentreIdentifier;
	private String loanOfficerForOriginatingTheLoan;
	private String dateOfAccInfo;
	private String loanCategory;
	private String groupIdentifier;
	private String loanCycleId;
	private String loanPurpose;
	private String accStatus;
	private String applicationDate;
	private String sanctionedDate;
	private String dateOpened;
	private String dateClosed;
	private String dateofLastPayment;
	private String appliedForAmount;
	private String loanAmountSanctioned;
	private String totalAmountDisbursed;
	private String numberOfInstallments;
	private String repaymentFrequency;
	private String instalmentAmount;
	private String currentBalance;
	private String amountOverdue;
	private String dpd;
	private String writeOffAmount;
	private String dateWriteOff;
	private String writeOffReason;
	private String noOfMeetingsHeld;
	private String noOfMeetingsSkipped;
	private String insuranceIndicator;
	private String typeOfInsurance;
	private String sumAssured;
	private String agreedMeetingDayOfTheWeek;
	private String agreedMeetingTimeOfTheDay;
	private Boolean accountValid = true;
	
	public Account() {
		// TODO Auto-generated constructor stub
	}
	
	public Account(String segmentIdentifier, String uniqueAccRefNumber, String accNumber, String branchIdentifier,
			String kendraCentreIdentifier, String loanOfficerForOriginatingTheLoan, String dateOfAccInfo,
			String loanCategory, String groupIdentifier, String loanCycleId, String loanPurpose, String accStatus,
			String applicationDate, String sanctionedDate, String dateOpened, String dateClosed,
			String dateofLastPayment, String appliedForAmount, String loanAmountSanctioned, String totalAmountDisbursed,
			String numberOfInstallments, String repaymentFrequency, String instalmentAmount, String currentBalance,
			String amountOverdue, String dpd, String writeOffAmount, String dateWriteOff, String writeOffReason,
			String noOfMeetingsHeld, String noOfMeetingsSkipped, String insuranceIndicator, String typeOfInsurance,
			String sumAssured, String agreedMeetingDayOfTheWeek, String agreedMeetingTimeOfTheDay,
			Boolean accountValid) {
		super();
		this.segmentIdentifier = segmentIdentifier;
		this.uniqueAccRefNumber = uniqueAccRefNumber;
		this.accNumber = accNumber;
		this.branchIdentifier = branchIdentifier;
		this.kendraCentreIdentifier = kendraCentreIdentifier;
		this.loanOfficerForOriginatingTheLoan = loanOfficerForOriginatingTheLoan;
		this.dateOfAccInfo = dateOfAccInfo;
		this.loanCategory = loanCategory;
		this.groupIdentifier = groupIdentifier;
		this.loanCycleId = loanCycleId;
		this.loanPurpose = loanPurpose;
		this.accStatus = accStatus;
		this.applicationDate = applicationDate;
		this.sanctionedDate = sanctionedDate;
		this.dateOpened = dateOpened;
		this.dateClosed = dateClosed;
		this.dateofLastPayment = dateofLastPayment;
		this.appliedForAmount = appliedForAmount;
		this.loanAmountSanctioned = loanAmountSanctioned;
		this.totalAmountDisbursed = totalAmountDisbursed;
		this.numberOfInstallments = numberOfInstallments;
		this.repaymentFrequency = repaymentFrequency;
		this.instalmentAmount = instalmentAmount;
		this.currentBalance = currentBalance;
		this.amountOverdue = amountOverdue;
		this.dpd = dpd;
		this.writeOffAmount = writeOffAmount;
		this.dateWriteOff = dateWriteOff;
		this.writeOffReason = writeOffReason;
		this.noOfMeetingsHeld = noOfMeetingsHeld;
		this.noOfMeetingsSkipped = noOfMeetingsSkipped;
		this.insuranceIndicator = insuranceIndicator;
		this.typeOfInsurance = typeOfInsurance;
		this.sumAssured = sumAssured;
		this.agreedMeetingDayOfTheWeek = agreedMeetingDayOfTheWeek;
		this.agreedMeetingTimeOfTheDay = agreedMeetingTimeOfTheDay;
		this.accountValid = accountValid;
	}
	public String getSegmentIdentifier() {
		return segmentIdentifier;
	}
	public void setSegmentIdentifier(String segmentIdentifier) {
		this.segmentIdentifier = segmentIdentifier;
	}
	public String getUniqueAccRefNumber() {
		return uniqueAccRefNumber;
	}
	public void setUniqueAccRefNumber(String uniqueAccRefNumber) {
		this.uniqueAccRefNumber = uniqueAccRefNumber;
	}
	public String getAccNumber() {
		return accNumber;
	}
	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}
	public String getBranchIdentifier() {
		return branchIdentifier;
	}
	public void setBranchIdentifier(String branchIdentifier) {
		this.branchIdentifier = branchIdentifier;
	}
	public String getKendraCentreIdentifier() {
		return kendraCentreIdentifier;
	}
	public void setKendraCentreIdentifier(String kendraCentreIdentifier) {
		this.kendraCentreIdentifier = kendraCentreIdentifier;
	}
	public String getLoanOfficerForOriginatingTheLoan() {
		return loanOfficerForOriginatingTheLoan;
	}
	public void setLoanOfficerForOriginatingTheLoan(String loanOfficerForOriginatingTheLoan) {
		this.loanOfficerForOriginatingTheLoan = loanOfficerForOriginatingTheLoan;
	}
	public String getDateOfAccInfo() {
		return dateOfAccInfo;
	}
	public void setDateOfAccInfo(String dateOfAccInfo) {
		this.dateOfAccInfo = dateOfAccInfo;
	}
	public String getLoanCategory() {
		return loanCategory;
	}
	public void setLoanCategory(String loanCategory) {
		this.loanCategory = loanCategory;
	}
	public String getGroupIdentifier() {
		return groupIdentifier;
	}
	public void setGroupIdentifier(String groupIdentifier) {
		this.groupIdentifier = groupIdentifier;
	}
	public String getLoanCycleId() {
		return loanCycleId;
	}
	public void setLoanCycleId(String loanCycleId) {
		this.loanCycleId = loanCycleId;
	}
	public String getLoanPurpose() {
		return loanPurpose;
	}
	public void setLoanPurpose(String loanPurpose) {
		this.loanPurpose = loanPurpose;
	}
	public String getAccStatus() {
		return accStatus;
	}
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}
	public String getApplicationDate() {
		return applicationDate;
	}
	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}
	public String getSanctionedDate() {
		return sanctionedDate;
	}
	public void setSanctionedDate(String sanctionedDate) {
		this.sanctionedDate = sanctionedDate;
	}
	public String getDateOpened() {
		return dateOpened;
	}
	public void setDateOpened(String dateOpened) {
		this.dateOpened = dateOpened;
	}
	public String getDateClosed() {
		return dateClosed;
	}
	public void setDateClosed(String dateClosed) {
		this.dateClosed = dateClosed;
	}
	public String getDateofLastPayment() {
		return dateofLastPayment;
	}
	public void setDateofLastPayment(String dateofLastPayment) {
		this.dateofLastPayment = dateofLastPayment;
	}
	public String getAppliedForAmount() {
		return appliedForAmount;
	}
	public void setAppliedForAmount(String appliedForAmount) {
		this.appliedForAmount = appliedForAmount;
	}
	public String getLoanAmountSanctioned() {
		return loanAmountSanctioned;
	}
	public void setLoanAmountSanctioned(String loanAmountSanctioned) {
		this.loanAmountSanctioned = loanAmountSanctioned;
	}
	public String getTotalAmountDisbursed() {
		return totalAmountDisbursed;
	}
	public void setTotalAmountDisbursed(String totalAmountDisbursed) {
		this.totalAmountDisbursed = totalAmountDisbursed;
	}
	public String getNumberOfInstallments() {
		return numberOfInstallments;
	}
	public void setNumberOfInstallments(String numberOfInstallments) {
		this.numberOfInstallments = numberOfInstallments;
	}
	public String getRepaymentFrequency() {
		return repaymentFrequency;
	}
	public void setRepaymentFrequency(String repaymentFrequency) {
		this.repaymentFrequency = repaymentFrequency;
	}
	public String getInstalmentAmount() {
		return instalmentAmount;
	}
	public void setInstalmentAmount(String instalmentAmount) {
		this.instalmentAmount = instalmentAmount;
	}
	public String getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(String currentBalance) {
		this.currentBalance = currentBalance;
	}
	public String getAmountOverdue() {
		return amountOverdue;
	}
	public void setAmountOverdue(String amountOverdue) {
		this.amountOverdue = amountOverdue;
	}
	public String getDpd() {
		return dpd;
	}
	public void setDpd(String dpd) {
		this.dpd = dpd;
	}
	public String getWriteOffAmount() {
		return writeOffAmount;
	}
	public void setWriteOffAmount(String writeOffAmount) {
		this.writeOffAmount = writeOffAmount;
	}
	public String getDateWriteOff() {
		return dateWriteOff;
	}
	public void setDateWriteOff(String dateWriteOff) {
		this.dateWriteOff = dateWriteOff;
	}
	public String getWriteOffReason() {
		return writeOffReason;
	}
	public void setWriteOffReason(String writeOffReason) {
		this.writeOffReason = writeOffReason;
	}
	public String getNoOfMeetingsHeld() {
		return noOfMeetingsHeld;
	}
	public void setNoOfMeetingsHeld(String noOfMeetingsHeld) {
		this.noOfMeetingsHeld = noOfMeetingsHeld;
	}
	public String getNoOfMeetingsSkipped() {
		return noOfMeetingsSkipped;
	}
	public void setNoOfMeetingsSkipped(String noOfMeetingsSkipped) {
		this.noOfMeetingsSkipped = noOfMeetingsSkipped;
	}
	public String getInsuranceIndicator() {
		return insuranceIndicator;
	}
	public void setInsuranceIndicator(String insuranceIndicator) {
		this.insuranceIndicator = insuranceIndicator;
	}
	public String getTypeOfInsurance() {
		return typeOfInsurance;
	}
	public void setTypeOfInsurance(String typeOfInsurance) {
		this.typeOfInsurance = typeOfInsurance;
	}
	public String getSumAssured() {
		return sumAssured;
	}
	public void setSumAssured(String sumAssured) {
		this.sumAssured = sumAssured;
	}
	public String getAgreedMeetingDayOfTheWeek() {
		return agreedMeetingDayOfTheWeek;
	}
	public void setAgreedMeetingDayOfTheWeek(String agreedMeetingDayOfTheWeek) {
		this.agreedMeetingDayOfTheWeek = agreedMeetingDayOfTheWeek;
	}
	public String getAgreedMeetingTimeOfTheDay() {
		return agreedMeetingTimeOfTheDay;
	}
	public void setAgreedMeetingTimeOfTheDay(String agreedMeetingTimeOfTheDay) {
		this.agreedMeetingTimeOfTheDay = agreedMeetingTimeOfTheDay;
	}
	public Boolean getAccountValid() {
		return accountValid;
	}
	public void setAccountValid(Boolean accountValid) {
		this.accountValid = accountValid;
	}
	@Override
	public String toString() {
		return "Account [segmentIdentifier=" + segmentIdentifier + ", uniqueAccRefNumber=" + uniqueAccRefNumber
				+ ", accNumber=" + accNumber + ", branchIdentifier=" + branchIdentifier + ", kendraCentreIdentifier="
				+ kendraCentreIdentifier + ", loanOfficerForOriginatingTheLoan=" + loanOfficerForOriginatingTheLoan
				+ ", dateOfAccInfo=" + dateOfAccInfo + ", loanCategory=" + loanCategory + ", groupIdentifier="
				+ groupIdentifier + ", loanCycleId=" + loanCycleId + ", loanPurpose=" + loanPurpose + ", accStatus="
				+ accStatus + ", applicationDate=" + applicationDate + ", sanctionedDate=" + sanctionedDate
				+ ", dateOpened=" + dateOpened + ", dateClosed=" + dateClosed + ", dateofLastPayment="
				+ dateofLastPayment + ", appliedForAmount=" + appliedForAmount + ", loanAmountSanctioned="
				+ loanAmountSanctioned + ", totalAmountDisbursed=" + totalAmountDisbursed + ", numberOfInstallments="
				+ numberOfInstallments + ", repaymentFrequency=" + repaymentFrequency + ", instalmentAmount="
				+ instalmentAmount + ", currentBalance=" + currentBalance + ", amountOverdue=" + amountOverdue
				+ ", dpd=" + dpd + ", writeOffAmount=" + writeOffAmount + ", dateWriteOff=" + dateWriteOff
				+ ", writeOffReason=" + writeOffReason + ", noOfMeetingsHeld=" + noOfMeetingsHeld
				+ ", noOfMeetingsSkipped=" + noOfMeetingsSkipped + ", insuranceIndicator=" + insuranceIndicator
				+ ", typeOfInsurance=" + typeOfInsurance + ", sumAssured=" + sumAssured + ", agreedMeetingDayOfTheWeek="
				+ agreedMeetingDayOfTheWeek + ", agreedMeetingTimeOfTheDay=" + agreedMeetingTimeOfTheDay
				+ ", accountValid=" + accountValid + "]";
	}
	
	
	}
