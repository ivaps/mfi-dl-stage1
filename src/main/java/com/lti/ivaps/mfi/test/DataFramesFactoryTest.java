package com.lti.ivaps.mfi.test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.SparkMasterRegex;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.hive.HiveContext;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import groovy.sql.Sql;

public class DataFramesFactoryTest implements Serializable {

public static DataFrame registerDataFrameForSource (String filePath, final String delimiter) throws Exception {

// Create SparkConf. Setup application name, set master as "local". 
SparkConf conf = new SparkConf().setAppName("DfTest1");
conf.setMaster("local");

// Create JavaSparkContext using above configuration 
JavaSparkContext context = new JavaSparkContext(conf);

// Setup hive context

//HiveContext hc = new HiveContext(context.sc());
SQLContext sc = new SQLContext(context.sc());
//sc.setConf("spark.sql.version", sc.sparkContext());
//hc.setConf("spark.sql.hive.version", HiveContext.hiveExecutionVersion());

// Initialize StructField list
List<StructField> structFields = new ArrayList<StructField>();

// Create StructFields for String
StructField structField1 = DataTypes.createStructField("String", DataTypes.StringType, true);
//StructField structField2 = DataTypes.createStructField("age", DataTypes.IntegerType, true);

// Add StructFields name & age into list
structFields.add(structField1);
//structFields.add(structField2);

// Create StructType from StructFields. This will be used to create DataFrame
StructType schema = DataTypes.createStructType(structFields);

// Create a plain string java rdd on the file path
JavaRDD<String> textFile = context.textFile(filePath, 200);

// Create RowRDD from plain string rdd. Use MapRddToFields class to convert String to Row object
MapRddToFields mapFn = new MapRddToFields();
mapFn.setDelimiter(delimiter);
JavaRDD<Row> rowRdd =textFile.map(mapFn);

// Create DataFrame and register table "t1"
DataFrame df = sc.createDataFrame(rowRdd, schema);

df.registerTempTable("t1");


// Display result
df.show();

return df;

}

}