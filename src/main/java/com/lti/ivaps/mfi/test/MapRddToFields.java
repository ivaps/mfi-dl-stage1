package com.lti.ivaps.mfi.test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

public class MapRddToFields implements
Function<String, Row>, Serializable {

String delimiter;

public String getDelimiter() {
return delimiter;
}

public void setDelimiter(String delimiter) {
this.delimiter = delimiter;
}

public Row call(String str) throws Exception {

// Split the input string using delimiter. To get name  age. 

String[] splt = StringUtils.splitPreserveAllTokens(str, delimiter);

// Convert age to Integer. Add both into List<Object>

List<Object> values = new ArrayList<Object>();

values.add(splt[0]);
values.add(Integer.valueOf(splt[1]));

// Create Row object by calling RowFactory.create method. Return same

return RowFactory.create(values);

}

}