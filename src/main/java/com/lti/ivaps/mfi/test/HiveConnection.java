package com.lti.ivaps.mfi.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Singleton class to get the connection to Hive server
 * 
 * @author shiva
 * 
 */
public class HiveConnection {
	private static final Logger logger = LoggerFactory.getLogger(HiveConnection.class);

	private volatile Connection connection;
	private static Object mutex = new Object();
	String groupId;

	public HiveConnection() {
		
	}
	
	public HiveConnection(String groupId) {
		this.groupId = groupId;
	}

	public Connection getConnection() {
		if (null == connection) {
			synchronized (mutex) {
				if (null == connection) {
					connection = init();
				}
			}
		}
		return connection;
	}

	public void refreshConnection() {
		connection = init();
	}

	private Connection init() {
		
		try {
			
			String driverName = "org.apache.hive.jdbc.HiveDriver";
			Class.forName(driverName);
			String queueName = "";
			if (StringUtils.isBlank(queueName) 
					|| queueName.equalsIgnoreCase("null")) {
				if(!StringUtils.isBlank(this.groupId) && !this.groupId.equalsIgnoreCase("null"))
					queueName = "";
			}
			
			String url = "jdbc:hive2://172.20.99.23:10000/default?;";
			url += "mapred.job.queue.name=MaxiqQueue";
			connection = DriverManager.getConnection(url,"hdfs","");

		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return connection;

	}

	public void close() {
		try {
			logger.info("Closing the connection");
			this.connection.close();
		} catch (SQLException e) {
			logger.error("Error Closing Connection : " + e);
			throw new RuntimeException(e);
		}
	}
}
