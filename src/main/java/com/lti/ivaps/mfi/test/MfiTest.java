package com.lti.ivaps.mfi.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.HiveContext;

import com.lti.ivaps.mfi.DataFramesFactory;
import com.lti.ivaps.mfi.PopulateMfiDomain;
import com.lti.ivaps.mfi.beans.Account;
import com.lti.ivaps.mfi.beans.FailedRecordsBean;
import com.lti.ivaps.mfi.beans.MfiDomain;
import com.lti.ivaps.mfi.beans.PassedRecordsBean;
import com.lti.ivaps.mfi.constans.MfiConstants;
import com.lti.ivaps.mfi.dataframes.Profiling;
import com.lti.ivaps.mfi.dataframes.SummaryStatistics;
import com.lti.ivaps.mfi.error.ErrorCodeList;
import com.lti.ivaps.mfi.error.ErrorRecord;
import com.lti.ivaps.mfi.exceptions.CdfParsingException;
import com.lti.ivaps.mfi.parser.CdfParser;
import com.lti.ivaps.mfi.validation.CdfValidation;
import com.lti.mfi.data.load.MfiDataLoad;

import io.netty.util.internal.StringUtil;
import scala.annotation.varargs;

public class MfiTest {
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final String mfiId = "MFI0000006";
		final String rptDt = "24102013";
		
		SparkConf sparkConf = null;
		HiveContext hiveContext = null;
		
		
		
		System.out.println("Spark Configuration");
		sparkConf = new SparkConf().setAppName("SparkApp");
		sparkConf.setMaster("local");
		sparkConf.set("spark.sql.parquet.compression.codec", "snappy");
		final JavaSparkContext javaSparkcontext = new JavaSparkContext(sparkConf);
		
		final SQLContext sqlContext = new SQLContext(javaSparkcontext);
		
		JavaRDD<String> textFile = javaSparkcontext.textFile("C:\\Users\\10612976\\Desktop\\FileS\\3\\2\\sample1.CDF");
				
		JavaRDD<MfiDomain> mfiDomainRdd = textFile.map(new Function<String, MfiDomain>() {

			public MfiDomain call(String stringRecord) throws Exception {
				
//				System.out.println("Call MFIDomain Constructor");
				MfiDomain mfiDomain = new MfiDomain("MFI0000006", "24102013", stringRecord);
				
				HashMap<String, ArrayList<String[]>> parsedRecord = null;
				boolean validateRecord = true;
				
				try {
									
					if(StringUtils.startsWith(stringRecord, "HDR") || StringUtils.startsWith(stringRecord, "TRL")){
						mfiDomain.setHeaderTrailerIndicator(true);
					} else {
						
						parsedRecord = CdfParser.parseMe(stringRecord);
						
						if(parsedRecord.get(MfiConstants.CNSCRD).size() > 1){
							mfiDomain.setValid(false);
							mfiDomain.addErrorCode(ErrorCodeList.E004);
						}
						
						if(null == parsedRecord.get(MfiConstants.ACTCRD)/*0 == parsedRecord.get(MfiConstants.ACTCRD).size()*/){
							mfiDomain.setValid(false);
							mfiDomain.addErrorCode(ErrorCodeList.E002);
						}
						
						if(null == parsedRecord.get(MfiConstants.ADRCRD)/*0 == parsedRecord.get(MfiConstants.ADRCRD).size()*/){
							mfiDomain.setValid(false);
							mfiDomain.addErrorCode(ErrorCodeList.E003);
						}
						
						
//						System.out.println("MfiDomain get valid indicator: " +mfiDomain.getValid());
						if(mfiDomain.getValid() && !mfiDomain.getHeaderTrailerIndicator()){
							PopulateMfiDomain.populateMfiDomainFromRDD(parsedRecord, mfiDomain);
							
//							System.out.println("MFI Domain populated successfully!!!");
							validateRecord = CdfValidation.validateRecord(mfiDomain);
							
						}
					}
					
				} catch (CdfParsingException e) {
					mfiDomain.setValid(false);
					mfiDomain.setReasonForFailure(e.getMessage());
				}
				
				
				return mfiDomain;
			}
			
		});
		
		
		System.out.println("done parsing "+ mfiDomainRdd.count());
		
		JavaRDD<MfiDomain> mfiDomainRdd1 = mfiDomainRdd.filter(new Function<MfiDomain, Boolean>(){

			//@Override
			public Boolean call(MfiDomain arg0) throws Exception {
				// TODO Auto-generated method stub
				
					return !arg0.getHeaderTrailerIndicator();
				
				
			}
			
		});
		
		/*JavaRDD<MfiDomain> failedRecords = mfiDomainRdd1.filter(new Function<MfiDomain, Boolean>() {
			public Boolean call(MfiDomain arg0) throws Exception {
				return !(arg0.getValid() && !arg0.getHeaderTrailerIndicator());
			}
		});
		
		
		System.out.println("1 "+failedRecords.count());
		List<MfiDomain> failedRecordsToShow = failedRecords.collect();
		System.out.println("***** "+failedRecordsToShow);

		JavaRDD<MfiDomain> passedRecords = mfiDomainRdd1.filter(new Function<MfiDomain, Boolean>() {
			public Boolean call(MfiDomain arg0) throws Exception {
				return (arg0.getValid() && !arg0.getHeaderTrailerIndicator());
			}
		});
		
		System.out.println("2 "+passedRecords.count());*/
		
		JavaRDD<PassedRecordsBean> passedRecordsRdd = mfiDomainRdd1.map(new Function<MfiDomain, PassedRecordsBean> () {

			public PassedRecordsBean call(MfiDomain mfiDomain) throws Exception {
				return PassedRecordsBean.createPassedRecord(mfiDomain);
			}
			
		}).filter(new Function<PassedRecordsBean, Boolean>() {

			public Boolean call(PassedRecordsBean arg0) throws Exception {
				// TODO Auto-generated method stub
				return null != arg0.getMemberId();
			}
		});
		
		System.out.println("3 "+passedRecordsRdd.count());
		
		JavaRDD<FailedRecordsBean> failedRecordsRdd1 = mfiDomainRdd1.map(new Function<MfiDomain, FailedRecordsBean> () {

			public FailedRecordsBean call(MfiDomain mfiDomain) throws Exception {
				return FailedRecordsBean.createFailedRecord(mfiDomain);
			}
			
		});
		JavaRDD<FailedRecordsBean> failedRecordsRdd = failedRecordsRdd1.filter(new Function<FailedRecordsBean, Boolean>() {

			public Boolean call(FailedRecordsBean arg0) throws Exception {
				// TODO Auto-generated method stub
				return null != arg0.getSeverity();
			}
		});
		
		System.out.println("4 "+failedRecordsRdd.count());
		
		DataFrame passedRecordsDf = DataFramesFactory.createDataFrameSql(passedRecordsRdd, PassedRecordsBean.class,sqlContext);
		
		System.out.println("5 "+passedRecordsDf.count());
		
		DataFrame failedRecordsDf = DataFramesFactory.createDataFrameSql(failedRecordsRdd, FailedRecordsBean.class, sqlContext);
		
		System.out.println("6 "+failedRecordsDf.count());
		
		/*MfiDataLoad mfiDataLoad = new MfiDataLoad<>();
		Map<String, DataFrame> dataFrames = new HashMap<String, DataFrame>();
		dataFrames.put("test", passedRecordsDf);
		mfiDataLoad.run(dataFrames);*/
		
		/*JavaRDD<MfiDomain> summaryStatistics = map2.filter(new Function<MfiDomain, Boolean>() {
			@Override
			public Boolean call(MfiDomain arg0) throws Exception {
				return arg0.getValid();
			}
		});*/
		
		//JavaRDD<Account> accountRDD = DataFramesFactory.createAccountDataFrame(map2, Account.class, hc);


		/*DataFrame dataframe = DataFramesFactory.createDataFrame(mfiDomainRdd,MfiDomain.class,hc);
		dataframe.registerTempTable("mfidata");
		
		DataFrame passedRecordsDataFrame = hc.sql("SELECT * from mfidata where valid_ = true");
		
		//DataFrame failedRecords = hc.sql("SELECT * from mfidata where valid = 'no'");
		//DataFrame failedRecordsDataFrame = hc.sql("SELECT mfiId as MFI_ID, CONCAT(CNSCRD,'~', ADDRCRD, '~', ACCTCRD) as Record, reasonForFailure from mfidata where valid_ = false");
		DataFrame failedRecordsDataFrame = hc.sql("SELECT * from mfidata where valid = false");
		
		passedRecordsDf.show(10);
		System.out.println("7");
		failedRecordsDf.show(10);
		System.out.println("8");*/
		passedRecordsDf.show();
		failedRecordsDf.show();
		
		
		/*//Ambica Start
		final Accumulator<Integer> failedCount = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> passedCount = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> totalCount = javaSparkcontext.accumulator(0);
		// Profiling counts
				final Accumulator<Integer> loanCategoryJLGGroup = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> loanCategoryJLGIndividual = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> loanCategoryIndividual = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> loanCategoryOther = javaSparkcontext.accumulator(0);

				final Accumulator<Integer> loanPurposeIndividualLoanEducationLoan = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> loanPurposeGroupLoanEmergencyFestivalLoan = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> loanPurposeIndividualLoanEmergencyFestivalLoan = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> loanPurposeFamilyLoan = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> loanPurposeGeneralLoan = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> loanPurposeGoldLoan = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> loanPurposeOthers = javaSparkcontext.accumulator(0);

				final Accumulator<Integer> accStatusLoanSubmitted = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> accStatusLoanApprovedNotyetdisbursed = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> accStatusLoanDeclined = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> accStatusCurrent = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> accStatusDelinquent = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> accStatusWrittenOff = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> accStatusAccountClosed = javaSparkcontext.accumulator(0);
				
				final Accumulator<Integer> countOfE001 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE002 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE003 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE004 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE005 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE006 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE007 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE008 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE009 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE010 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE011 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE012 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE013 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE014 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE015 = javaSparkcontext.accumulator(0);
				final Accumulator<Integer> countOfE016 = javaSparkcontext.accumulator(0);
		
		JavaRDD<SummaryStatistics> summaryStatisticsRdd = mfiDomainRdd.map(new Function<MfiDomain, FailedRecordsBean> () {

			@Override
			public FailedRecordsBean call(MfiDomain mfiDomain) throws Exception {
				return FailedRecordsBean.createFailedRecord(mfiDomain);
			}
			
		});
				
		mfiDomainRdd.foreach(new VoidFunction<MfiDomain>() {

			//@Override
			public void call(MfiDomain arg0) throws Exception {
				System.out.println("Aditya : " + arg0);
				
			}
		});
		
		
		mfiDomainRdd1.foreach(new VoidFunction<MfiDomain>(){

			public void call(MfiDomain mfiDomain) throws Exception {
				// TODO Auto-generated method stub
				System.out.println("Reason of failure:" +mfiDomain.getReasonOfFailureForCounter());
				String[] arrayOfReasonOfFailure = StringUtils.splitPreserveAllTokens(mfiDomain.getReasonOfFailureForCounter(), MfiConstants.splitDelimiterComma);
				if(!mfiDomain.getHeaderTrailerIndicator()){
					if(mfiDomain.getValid()){
						
					passedCount.add(1);
					if(!mfiDomain.getHeaderTrailerIndicator()){
						// TODO Auto-generated method stub
						List<Account> accounts = mfiDomain.getAccount();
						System.out.println("Accounts" +accounts.get(0));
						for(Account account : accounts) {

							if (StringUtils.isNotBlank(account.getLoanCategory()) 
									&& StringUtils.isNotBlank(account.getLoanPurpose()) 
									&& StringUtils.isNotBlank(account.getAccStatus())) {

								// Loan Category
								if(account.getLoanCategory().equalsIgnoreCase(MfiConstants.T01_ACCOUNT_LOAN_CATEGORY)){
									loanCategoryJLGGroup.add(1);
								}else if(account.getLoanCategory().equalsIgnoreCase(MfiConstants.T02_ACCOUNT_LOAN_CATEGORY)){
									loanCategoryJLGIndividual.add(1);
								}else if (account.getLoanCategory().equalsIgnoreCase(MfiConstants.T03_ACCOUNT_LOAN_CATEGORY)){
									loanCategoryIndividual.add(1);
								}else{
									loanCategoryOther.add(1);
								}
								//Loan Purpose
								if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A01_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeIndividualLoanEducationLoan.add(1);
								}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A02_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeGroupLoanEmergencyFestivalLoan.add(1);
								}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A03_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeIndividualLoanEmergencyFestivalLoan.add(1);
								}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A04_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeFamilyLoan.add(1);
								}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A05_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeGeneralLoan.add(1);
								}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A06_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeGoldLoan.add(1);
								}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A35_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeOthers.add(1);
								}
								//Account Status
								if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S01_ACCOUNT_STATUS)){
									accStatusLoanSubmitted.add(1);
								}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S02_ACCOUNT_STATUS)){
									accStatusLoanApprovedNotyetdisbursed.add(1);
								}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S03_ACCOUNT_STATUS)){
									accStatusLoanDeclined.add(1);
								}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S04_ACCOUNT_STATUS)){
									accStatusCurrent.add(1);
								}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S05_ACCOUNT_STATUS)){
									accStatusDelinquent.add(1);
								}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S06_ACCOUNT_STATUS)){
									accStatusWrittenOff.add(1);
								}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S07_ACCOUNT_STATUS)){
									accStatusAccountClosed.add(1);
								}
							}
						}
						}
				}else{
					failedCount.add(1);
					String[] arrayOfReasonOfFailure1;
					if(null != mfiDomain.getReasonOfFailureForCounter()){
						arrayOfReasonOfFailure1 = StringUtils.splitPreserveAllTokens(mfiDomain.getReasonOfFailureForCounter(), MfiConstants.splitDelimiterComma);
					for (int i = 0; i < arrayOfReasonOfFailure1.length; i++) {
						//String errorCode = arrayOfReasonOfFailure[i];
						if(null != arrayOfReasonOfFailure1[i]){
					//	ErrorRecord er = ErrorRecord.valueOf(arrayOfReasonOfFailure[i]);
						switch (arrayOfReasonOfFailure[i]) {
						case ErrorCodeList.E001:
							countOfE001.add(1);
							break;
						case ErrorCodeList.E002:
							countOfE002.add(1);
							break;
						case ErrorCodeList.E003:
							countOfE003.add(1);
							break;
						case ErrorCodeList.E004:
							countOfE004.add(1);
							break;
						case ErrorCodeList.E005:
							countOfE005.add(1);
							break;
						case ErrorCodeList.E006:
							countOfE006.add(1);
							break;
						case ErrorCodeList.E007:
							countOfE007.add(1);
							break;
						case ErrorCodeList.E008:
							countOfE008.add(1);
							break;
						case ErrorCodeList.E009:
							countOfE009.add(1);
							break;
						case ErrorCodeList.E010:
							countOfE010.add(1);
							break;
						case ErrorCodeList.E011:
							countOfE011.add(1);
							break;
						case ErrorCodeList.E012:
							countOfE012.add(1);
							break;
						case ErrorCodeList.E013:
							countOfE013.add(1);
							break;
						case ErrorCodeList.E014:
							countOfE014.add(1);
							break;
						case ErrorCodeList.E015:
							countOfE015.add(1);
							break;
						case ErrorCodeList.E016:
							countOfE016.add(1);
							break;
						default:
							break;
						}
							

						if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E001)) {
							countOfE001.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E002)) {
							countOfE002.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E003)) {
							countOfE003.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E004)) {
							countOfE004.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E005)) {
							countOfE005.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E006)) {
							countOfE006.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E007)) {
							countOfE007.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E008)) {
							countOfE008.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E009)) {
							countOfE009.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E010)) {
							countOfE010.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E011)) {
							countOfE011.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E012)) {
							countOfE012.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E013)) {
							countOfE013.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E014)) {
							countOfE014.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E015)) {
							countOfE015.add(1);
						} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E016)) {
							countOfE016.add(1);
						}

							if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E001)) {
								countOfE001.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E002)) {
								countOfE002.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E003)) {
								countOfE003.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E004)) {
								countOfE004.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E005)) {
								countOfE005.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E006)) {
								countOfE006.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E007)) {
								countOfE007.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E008)) {
								countOfE008.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E009)) {
								countOfE009.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E010)) {
								countOfE010.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E011)) {
								countOfE011.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E012)) {
								countOfE012.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E013)) {
								countOfE013.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E014)) {
								countOfE014.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E015)) {
								countOfE015.add(1);
							} else if (arrayOfReasonOfFailure1[i].contains(ErrorCodeList.E016)) {
								countOfE016.add(1);
							}
					}
					if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E001)) {
						countOfE001.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E002)) {
						countOfE002.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E003)) {
						countOfE003.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E004)) {
						countOfE004.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E005)) {
						countOfE005.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E006)) {
						countOfE006.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E007)) {
						countOfE007.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E008)) {
						countOfE008.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E009)) {
						countOfE009.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E010)) {
						countOfE010.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E011)) {
						countOfE011.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E012)) {
						countOfE012.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E013)) {
						countOfE013.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E014)) {
						countOfE014.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E015)) {
						countOfE015.add(1);
					} else if (mfiDomain.getReasonOfFailureForCounter().contains(ErrorCodeList.E016)) {
						countOfE016.add(1);
					}
					}
				}
				}
					totalCount.add(1);
			}
				
				//totalCount.add(1);
				
			
				
				//totalCount.add(1);
				
			}
			
		});
		
		JavaRDD<SummaryStatistics> summaryStatisticsRdd = javaSparkcontext.parallelize(SummaryStatistics.generateErrorList(totalCount.value(), passedCount.value(), failedCount.value(),
				countOfE001.value(), countOfE002.value(),countOfE003.value(),countOfE004.value(),countOfE005.value(),countOfE006.value(),countOfE007.value(),
				countOfEACT002.value(), countOfEACT003.value(), countOfEACT004.value(), countOfEACT005.value(),
				countOfEACT006.value(), countOfEACT007.value(), countOfEACT008.value(), countOfEACT011.value(),
				countOfEACT012.value(), countOfEACT015.value(), countOfEACT019.value(), countOfEACT020.value(),
				countOfEACT023.value(), countOfEACT024.value(), countOfEACT025.value(), countOfEADR000.value(),
				countOfECNS002.value(), countOfECNS003.value(), countOfECNS004.value(), countOfECNS006.value(), countOfECNS010.value(),
				countOfECNS011.value(), countOfECNS012.value(), countOfECNS013.value(), countOfECNS015_024.value()));
		//Ambica End
*/		
		
		final Accumulator<Integer> failedCount = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> passedCount = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> totalCount = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfE001 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfE002 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfE003 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfE004 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfE005 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfE006 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfE007 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT002 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT003 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT004 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT005 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT006 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT007 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT008 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT011 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT012 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT015 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT019 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT020 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT023 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT024 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEACT025 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfEADR000 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfECNS002 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfECNS003 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfECNS004 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfECNS006 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfECNS010 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfECNS011 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfECNS012 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfECNS013 = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> countOfECNS015_024 = javaSparkcontext.accumulator(0);

		// Profiling counts
		final Accumulator<Integer> loanCategoryJLGGroup = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> loanCategoryJLGIndividual = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> loanCategoryIndividual = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> loanCategoryOther = javaSparkcontext.accumulator(0);

		final Accumulator<Integer> loanPurposeIndividualLoanEducationLoan = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> loanPurposeGroupLoanEmergencyFestivalLoan = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> loanPurposeIndividualLoanEmergencyFestivalLoan = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> loanPurposeFamilyLoan = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> loanPurposeGeneralLoan = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> loanPurposeGoldLoan = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> loanPurposeOthers = javaSparkcontext.accumulator(0);

		final Accumulator<Integer> accStatusLoanSubmitted = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> accStatusLoanApprovedNotyetdisbursed = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> accStatusLoanDeclined = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> accStatusCurrent = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> accStatusDelinquent = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> accStatusWrittenOff = javaSparkcontext.accumulator(0);
		final Accumulator<Integer> accStatusAccountClosed = javaSparkcontext.accumulator(0);

		mfiDomainRdd.foreach(new VoidFunction<MfiDomain>(){

			public void call(MfiDomain mfiDomain) throws Exception {
				// TODO Auto-generated method stub
				//System.out.println("Reason of failure:" +mfiDomain.getReasonOfFailureForCounter());
				
				if(!mfiDomain.getHeaderTrailerIndicator()){
					
					if(mfiDomain.getValid()){
						
					passedCount.add(1);
					
						List<Account> accounts = mfiDomain.getAccount();
						for(Account account : accounts) {

							if (StringUtils.isNotBlank(account.getLoanCategory()) 
									&& StringUtils.isNotBlank(account.getLoanPurpose()) 
									&& StringUtils.isNotBlank(account.getAccStatus())) {

								// Loan Category
								if(account.getLoanCategory().equalsIgnoreCase(MfiConstants.T01_ACCOUNT_LOAN_CATEGORY)){
									loanCategoryJLGGroup.add(1);
								}else if(account.getLoanCategory().equalsIgnoreCase(MfiConstants.T02_ACCOUNT_LOAN_CATEGORY)){
									loanCategoryJLGIndividual.add(1);
								}else if (account.getLoanCategory().equalsIgnoreCase(MfiConstants.T03_ACCOUNT_LOAN_CATEGORY)){
									loanCategoryIndividual.add(1);
								}else{
									loanCategoryOther.add(1);
								}
								//Loan Purpose
								if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A01_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeIndividualLoanEducationLoan.add(1);
								}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A02_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeGroupLoanEmergencyFestivalLoan.add(1);
								}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A03_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeIndividualLoanEmergencyFestivalLoan.add(1);
								}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A04_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeFamilyLoan.add(1);
								}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A05_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeGeneralLoan.add(1);
								}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A06_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeGoldLoan.add(1);
								}else if(account.getLoanPurpose().equalsIgnoreCase(MfiConstants.A35_ACCOUNT_LOAN_PURPOSE)){
									loanPurposeOthers.add(1);
								}
								//Account Status
								if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S01_ACCOUNT_STATUS)){
									accStatusLoanSubmitted.add(1);
								}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S02_ACCOUNT_STATUS)){
									accStatusLoanApprovedNotyetdisbursed.add(1);
								}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S03_ACCOUNT_STATUS)){
									accStatusLoanDeclined.add(1);
								}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S04_ACCOUNT_STATUS)){
									accStatusCurrent.add(1);
								}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S05_ACCOUNT_STATUS)){
									accStatusDelinquent.add(1);
								}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S06_ACCOUNT_STATUS)){
									accStatusWrittenOff.add(1);
								}else if(account.getAccStatus().equalsIgnoreCase(MfiConstants.S07_ACCOUNT_STATUS)){
									accStatusAccountClosed.add(1);
								}
							}
						}
						//}
				}else{
					failedCount.add(1);
					String[] arrayOfReasonOfFailure = null;
					if(null != mfiDomain.getReasonOfFailureForCounter()){
						arrayOfReasonOfFailure = StringUtils.splitPreserveAllTokens(mfiDomain.getReasonOfFailureForCounter(), MfiConstants.splitDelimiterComma);
					for (int i = 0; i < arrayOfReasonOfFailure.length; i++) {
						
						if(null != arrayOfReasonOfFailure[i]){
					

							if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT002)) {
								countOfEACT002.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT003)) {
								countOfEACT003.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT004)) {
								countOfEACT004.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT005)) {
								countOfEACT005.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT006)) {
								countOfEACT006.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT007)) {
								countOfEACT007.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT008)) {
								countOfEACT008.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT011)) {
								countOfEACT011.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT012)) {
								countOfEACT012.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT015)) {
								countOfEACT015.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT019)) {
								countOfEACT019.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT020)) {
								countOfEACT020.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT023)) {
								countOfEACT023.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT024)) {
								countOfEACT024.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EACT025)) {
								countOfEACT025.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.EADR000)) {
								countOfEADR000.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS002)) {
								countOfECNS002.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS003)) {
								countOfECNS003.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS004)) {
								countOfECNS004.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS006)) {
								countOfECNS006.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS010)) {
								countOfECNS010.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS011)) {
								countOfECNS011.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS012)) {
								countOfECNS012.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS013)) {
								countOfECNS013.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.ECNS015_024)) {
								countOfECNS015_024.add(1);
							}else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E001)) {
								countOfE001.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E002)) {
								countOfE002.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E003)) {
								countOfE003.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E004)) {
								countOfE004.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E005)) {
								countOfE005.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E006)) {
								countOfE006.add(1);
							} else if (arrayOfReasonOfFailure[i].contains(ErrorCodeList.E007)) {
								countOfE007.add(1);
							}
					    }
					
					 }
				   }
				}
					totalCount.add(1);
			}
				
			}
			
		});

		
		JavaRDD<SummaryStatistics> summaryStatisticsRdd = javaSparkcontext.parallelize(SummaryStatistics.generateErrorList(totalCount.value(), passedCount.value(), failedCount.value(),
				countOfE001.value(), countOfE002.value(),countOfE003.value(),countOfE004.value(),countOfE005.value(),countOfE006.value(),countOfE007.value(),
				countOfEACT002.value(), countOfEACT003.value(), countOfEACT004.value(), countOfEACT005.value(),
				countOfEACT006.value(), countOfEACT007.value(), countOfEACT008.value(), countOfEACT011.value(),
				countOfEACT012.value(), countOfEACT015.value(), countOfEACT019.value(), countOfEACT020.value(),
				countOfEACT023.value(), countOfEACT024.value(), countOfEACT025.value(), countOfEADR000.value(),
				countOfECNS002.value(), countOfECNS003.value(), countOfECNS004.value(), countOfECNS006.value(), countOfECNS010.value(),
				countOfECNS011.value(), countOfECNS012.value(), countOfECNS013.value(), countOfECNS015_024.value())).filter(new Function<SummaryStatistics, Boolean>() {

					public Boolean call(SummaryStatistics arg0) throws Exception {
						// TODO Auto-generated method stub
						return 0 != arg0.getCount();
					}
				});

		DataFrame summaryStatisticsDf = DataFramesFactory.createDataFrameSql(summaryStatisticsRdd,
				SummaryStatistics.class, sqlContext);

		JavaRDD<Profiling> profilingRdd = javaSparkcontext
				.parallelize(Profiling.generateProfilingList(loanCategoryJLGGroup.value(),
						loanCategoryJLGIndividual.value(), loanCategoryIndividual.value(),
						loanCategoryOther.value(), loanPurposeIndividualLoanEducationLoan.value(),
						loanPurposeGroupLoanEmergencyFestivalLoan.value(),
						loanPurposeIndividualLoanEmergencyFestivalLoan.value(), loanPurposeFamilyLoan.value(),
						loanPurposeGeneralLoan.value(), loanPurposeGoldLoan.value(), loanPurposeOthers.value(),
						accStatusLoanSubmitted.value(), accStatusLoanApprovedNotyetdisbursed.value(),
						accStatusLoanDeclined.value(), accStatusCurrent.value(), accStatusDelinquent.value()));
		/*JavaRDD<Profiling> profilingRdd = javaSparkcontext
				.parallelize(Profiling.generateProfilingList(loanCategoryJLGGroup.value(),
						loanCategoryJLGIndividual.value(), loanCategoryIndividual.value(),
						loanCategoryOther.value(), loanPurposeIndividualLoanEducationLoan.value(),
						loanPurposeGroupLoanEmergencyFestivalLoan.value(),
						loanPurposeIndividualLoanEmergencyFestivalLoan.value(), loanPurposeFamilyLoan.value(),
						loanPurposeGeneralLoan.value(), loanPurposeGoldLoan.value(), loanPurposeOthers.value(),
						accStatusLoanSubmitted.value(), accStatusLoanApprovedNotyetdisbursed.value(),
						accStatusLoanDeclined.value(), accStatusCurrent.value(), accStatusDelinquent.value()));*/
		System.out.println("Summary statistics............");
		summaryStatisticsDf.show();
		System.out.println("Profiling............");
		DataFrame profilingDf = DataFramesFactory.createDataFrameSql(profilingRdd, Profiling.class, sqlContext);
		profilingDf.show();
		System.out.println("Count of failed Record: " +failedCount.value());
		System.out.println("Count of passed Record: " +passedCount.value());
		System.out.println("Count of Total Record: " +totalCount.value());
		System.out.println("Count of Error E003: " +countOfE003);
		/*System.out.println("Count of Error E009: " +countOfE009);
		System.out.println("Count of Error E013: " +countOfE013);
		System.out.println("Count of Error E015: " +countOfE015);
		System.out.println("Count of Error E016: " +countOfE016);*/
		
		
		
		//JavaRDD<SummaryStatistics> summaryStatisticsRdd = mfiDomainRdd.
		
		/*mfiDomainRdd.foreach(new VoidFunction<MfiDomain>(){

			public void call(MfiDomain mfiDomain) throws Exception {}
			
		});*/
		
		System.out.println("Count of loanCategoryIndividual Record: " +loanCategoryIndividual.value());
		
	}

}
