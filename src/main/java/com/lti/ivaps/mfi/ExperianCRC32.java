package com.lti.ivaps.mfi;


import java.util.List;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import org.apache.commons.lang3.StringUtils;

import com.lti.ivaps.mfi.beans.Address;
import com.lti.ivaps.mfi.beans.Consumer;


public class ExperianCRC32 {


	private static final ExperianCRC32 ev = new ExperianCRC32();

	private ExperianCRC32() {

	}

	public static ExperianCRC32 getInstance() {

		return ev;
	}

	public long getCRC32(String value) {
		for(String s:ExperianConstants.REMOVE_SPECIAL_CHARACTERS)
		{
			StringUtils.replace(value, s, "");
		}
		value = value.toUpperCase();
		byte bytes[] = value.getBytes();

		Checksum checksum = new CRC32();

		checksum.update(bytes, 0, bytes.length);

		//System.out.println(checksum.getValue());

		return checksum.getValue();
	}

	public long getCRCTCRC32(String memberId,String branchId,String kendra)
	{
		return getCRC32(memberId+branchId+kendra);
	}

	public long getGlobalCRC32(String memberId,String branchId,String kendra,String Name1,String Name2,String Name3,String dob,String gender,String keypersonname,String keypersontype,String relationname,String nomineename,String voterId,String uId,String pan,String rationCard,String otherId1,String otherId2,String otherId3,String telephone1,String telephone1type,String telephone2,String telephone2type,String perAdd,String perStatecode,String perPincode,String curAdd,String curStatecode,String curPincode)
	{
		//System.out.println(getCRC32(memberId+branchId+kendra+Name1+Name2+Name3+dob+gender+keypersonname+keypersontype+relationname+nomineename+voterId+uId+pan+rationCard+otherId1+otherId2+otherId3+telephone1+telephone1type+telephone2+telephone2type+perAdd+perStatecode+perPincode+curAdd+curStatecode+curPincode));

		return getCRC32(memberId+branchId+kendra+Name1+Name2+Name3+dob+gender+keypersonname+keypersontype+relationname+nomineename+voterId+uId+pan+rationCard+otherId1+otherId2+otherId3+telephone1+telephone1type+telephone2+telephone2type+perAdd+perStatecode+perPincode+curAdd+curStatecode+curPincode);
	} 

	public long getGlobalCRCLTI(List<Address> address, Consumer consumer){
		StringBuilder builder = new StringBuilder();

		for(Address a : address){
			builder.append(a.getPermanentAddress());
			builder.append(a.getPermanentAdrstateCode());
			builder.append(a.getPermanentAdrpinCode());
			builder.append(a.getCurrentAddress());
			builder.append(a.getCurrentAdrstateCode());
			builder.append(a.getCurrentAdrpinCode());
		}

		String consumerCRCDetails = getConsumerDetailsForCRC(consumer);
		builder.append(consumerCRCDetails);
		return getCRC32(builder.toString());


	}

	private String getConsumerDetailsForCRC(Consumer consumer) {
		StringBuilder builder = new StringBuilder();
		//for(Consumer cns : consumer){
			builder.append(consumer.getMemberIdentifier());
			builder.append(consumer.getBranchIdentifier());
			builder.append(consumer.getCentreIdentifier());
			builder.append(consumer.getMemberFirstName());
			builder.append(consumer.getMemberMiddleName());
			builder.append(consumer.getMemberLastName());
			builder.append(consumer.getMemberDOB());
			builder.append(consumer.getMemberGender());
			builder.append(consumer.getKeyPersonName());
			builder.append(consumer.getKeyPersonRelnship());
			builder.append(consumer.getNomineeName());
			builder.append(consumer.getVoterId());
			builder.append(consumer.getUid());
			builder.append(consumer.getPan());
			builder.append(consumer.getRationCard());
			builder.append(consumer.getOtrIdTypeOneValue());
			builder.append(consumer.getOtherIdTypeTwoValue());
			builder.append(consumer.getTelephNumberTypeOneValue());
			builder.append(consumer.getTelephNumberTypeOneInd());
			builder.append(consumer.getTelephNumberTypeTwoValue());
			builder.append(consumer.getTelephNumberTypeTwoInd());
		//}
		return builder.toString();

	}

	public long getSubGlobalCRC32(String Name1,String Name2,String Name3,String dob,String gender,String keypersonname,String keypersontype,String relationname,String nomineename,String voterId,String uId,String pan,String rationCard,String otherId1,String otherId2,String otherId3,String telephone1,String telephone1type,String telephone2,String telephone2type,String perAdd,String perStatecode,String perPincode,String curAdd,String curStatecode,String curPincode)
	{
		return getCRC32(Name1+Name2+Name3+dob+gender+keypersonname+keypersontype+relationname+nomineename+voterId+uId+pan+rationCard+otherId1+otherId2+otherId3+telephone1+telephone1type+telephone2+telephone2type+perAdd+perStatecode+perPincode+curAdd+curStatecode+curPincode);
	}

	public long getCRCRCRC32(String memberId,String branchId,String kendra,String accountNumber)
	{
		return getCRC32(memberId+branchId+kendra+accountNumber);
	}

	public long getNameCRC32(String Name1,String Name2,String Name3)
	{
		/*for(String s : ExperianConstants.REMOVE_SPECIAL_CHARACTERS)
		{*/
		Name1 = StringUtils.replace(Name1, " ", "");
		Name1 = StringUtils.replace(Name1, " ", "");
		Name1 = StringUtils.replace(Name1, " ", "");

		Name2 = StringUtils.replace(Name2, " ", "");
		Name2 = StringUtils.replace(Name2, " ", "");
		Name2 = StringUtils.replace(Name2, " ", "");

		Name3 = StringUtils.replace(Name3, " ", "");
		Name3 = StringUtils.replace(Name3, " ", "");
		Name3 = StringUtils.replace(Name3, " ", "");

		//}
		//System.out.println(Name1+Name2+Name3+" : "+getCRC32((Name1+Name2+Name3).trim()));
		return getCRC32(Name1+Name2+Name3);
	}

	public long getAddressCRC32(String Add1,String state1,String zipCode1,String Add2,String state2,String zipCode2)
	{
		return getCRC32(Add1+state1+zipCode1+Add2+state2+zipCode2);
	}

	public long getPhoneCRC32(String telephoneNumber1,String telephoneType1,String telephoneNumber2,String telephoneType2)
	{
		return getCRC32(telephoneNumber1+telephoneType1+telephoneNumber2+telephoneType2);
	}

	public long getIDCRC32(String PAN,String voterID,String rationCard,String uid)
	{
		return getCRC32(PAN+voterID+rationCard+uid);
	}

	public long getRelationshipCRC32(String keypersonName, String keyPersonType,String relationShipName,String relationship1type,String nomineeName,String nomineeType) {

		return getCRC32(keypersonName+keyPersonType+relationShipName+nomineeName);
	}

	public long getAccount(String uniqueAccount, String accountNumber,String branchId,String kendra,String dateofaccountInformation,String loanCategory,String accountStatus,String sanctionDate,String openDate,String replaymentFrequency,String currentBalance,String insuranceIndicator,String typeOfInsurance) {

		return getCRC32(uniqueAccount+accountNumber+branchId+kendra+dateofaccountInformation+loanCategory+accountStatus+sanctionDate+openDate+replaymentFrequency+currentBalance+insuranceIndicator+typeOfInsurance);
	}




}
