/*package com.lti.ivaps.mfi;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.HiveContext;

public class TestMfi {

	static int passedRecords = 0;
	static int failedRecords = 0;

	public static void main(String[] args) {
		CNSDomain cnsDomain = new CNSDomain();

		cnsDomain.setMember_id("100");
		cnsDomain.setMfi_id("NBFLTFIN50");
		cnsDomain.setName("Aditya Calangutkar");
		cnsDomain.setAge("27");

		Address ad = new Address();
		ad.setAdd_key("123");
		ad.setAddress("Goa");
		List<Address> address = new ArrayList<Address>();
		address.add(ad);
		// cnsDomain.setAdd(address);

		Account ac = new Account();
		ac.setAct_key("2222");
		ac.setAct_number("0000524371");

		List<Account> account = new ArrayList<Account>();
		//account.add(ac);
		// cnsDomain.setAct(account);

		SparkConf conf = null;
		// final JavaSparkContext context = null;
		// final SQLContext sqlContext = null;
		HiveContext hc = null;

		conf = new SparkConf().setAppName("SparkApp");
		conf.setMaster("local");
		conf.set("spark.sql.parquet.compression.codec", "snappy");
		final JavaSparkContext context = new JavaSparkContext(conf);
		final SQLContext sqlContext = new SQLContext(context);

		JavaRDD<String> textFile = context.textFile("C:\\Users\\10612976\\Desktop\\FileS\\Sample1.CDF");

		JavaRDD<MfiDomain> map = textFile.map(new Function<String, MfiDomain>() {

			public MfiDomain call(String record) throws Exception {

				MfiDomain mfiDomain = PopulateMfiDomain.populateMfiDomainFromRDDTest(record, passedRecords, failedRecords);

				boolean validateRecord = Validations.validateRecord(mfiDomain, passedRecords, failedRecords);
				if(validateRecord){
					passedRecords++;
				}else{
					failedRecords++;
				}

				return mfiDomain;
			}

		});
		
		JavaRDD<MyClass> map = textFile.map(new Function<String, MyClass>() {

			public MyClass call(String record) throws Exception {
				// TODO Auto-generated method stub
				MyClass myClass = new MyClass();
				myClass.setRecord(record);
				return myClass;
			}
		});

		//DataFrame createDataFrame = DataFramesFactory.createDataFrameNew(map,MyClass.class,sqlContext);

		JavaRDD<Row> javaRDD = createDataFrame.toJavaRDD();
		JavaRDD<MfiDomain> map2 = javaRDD.map(new Function<Row, MfiDomain>() {

			public MfiDomain call(Row record) throws Exception {

				MfiDomain mfiDomain = PopulateMfiDomain.populateMfiDomainFromRDD(record, passedRecords, failedRecords);

				boolean validateRecord = Validations.validateRecord(mfiDomain, passedRecords, failedRecords);
				if(validateRecord){
					passedRecords++;
				}else{
					failedRecords++;
				}

				return mfiDomain;
			}
		});
		//DataFrame createDataFrame = sqlContext.createDataFrame(map, MfiDomain.class);
		JavaRDD<Row> javaRDD = createDataFrame.toJavaRDD();
		javaRDD.foreach(new VoidFunction<Row>() {

			public void call(Row arg0) throws Exception {
				System.out.println(" Data : " + arg0.get(0) + arg0.get(1) + arg0.get(2));

			}
		});
		//createDataFrame.show(5000);

		System.out.println("PASSED RECORDS ==> " + passedRecords);
		System.out.println("FAILED RECORDS ==> " + failedRecords);
		
		DataFrame createDataFrame1 = DataFramesFactory.createDataFrame(map2,MfiDomain.class,sqlContext);
		createDataFrame1.show(100);

		createDataFrame.registerTempTable("mytable");
		DataFrame sql = sqlContext.sql("SELECT count(*) as passedrecords from mytable where valid='yes'");
		sql.show();

		// map.collect();

		
		 * DataFrame json2 = sqlContext.read().json(parallelize); json2.show();
		 

		
		 * DataFrame json = sqlContext.read().json("D:\\user.json");
		 * json.registerTempTable("mytable"); DataFrame sql = sqlContext.sql(
		 * "SELECT add from mytable"); sql.show();
		 * sql.write().parquet("D:\\parquetfile");
		 

		
		 * DataFrame parquet = sqlContext.read().parquet("D:\\parquetfile");
		 * parquet.show();
		 

		
		 * List<StructField> fields = null; StructType schema =
		 * DataTypes.createStructType(fields);
		 

		// System.out.println(cnsDomain.getAge());
	}
}
*/