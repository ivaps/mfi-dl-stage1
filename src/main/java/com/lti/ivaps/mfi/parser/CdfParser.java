package com.lti.ivaps.mfi.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.lti.ivaps.mfi.constans.MfiConstants;
import com.lti.ivaps.mfi.error.ErrorCodeList;
import com.lti.ivaps.mfi.exceptions.CdfParsingException;
import com.lti.mfi.data.load.MfiDataLoad;

//import List;

public class CdfParser {
	
	static  Logger log = Logger.getLogger(CdfParser.class);
	
	static HashMap<String, Integer> segmentFieldLenMap = new HashMap<String, Integer>();
	static String errorCode = null;
	static
	{
		segmentFieldLenMap.put(MfiConstants.CNSCRD, 55);
		segmentFieldLenMap.put(MfiConstants.ACTCRD, 37);
		segmentFieldLenMap.put(MfiConstants.ADRCRD, 8);
	}
	
	public static HashMap<String, ArrayList<String[]>> parseMe(String record) throws CdfParsingException{
		
		log.info("Starting CDF parsing");
		
		HashMap<String, ArrayList<String[]>> parsedSegments = new HashMap<String, ArrayList<String[]>>();
		
		
		while (StringUtils.length(record) > 0){
			String segment = StringUtils.substring(record, 0, 6);
			//System.out.println(segment);
			
			Integer noOfFieldsInThisSegment = segmentFieldLenMap.get(segment);
			
			if (null == noOfFieldsInThisSegment){
				log.error("Failed on Error code " +ErrorCodeList.E001);
				throw new CdfParsingException(ErrorCodeList.E001);
			}
			
			int nextSegmentStartPos = StringUtils.ordinalIndexOf(record, "~", noOfFieldsInThisSegment);
			String segmentData = StringUtils.substring(record, 0, nextSegmentStartPos);
			//System.out.println(segmentData);
			
			String[] fieldsArray = StringUtils.splitPreserveAllTokens(segmentData, "~");
			
			if(fieldsArray.length != noOfFieldsInThisSegment){
				log.error("Failed on Error code " +ErrorCodeList.E001);
				throw new CdfParsingException(ErrorCodeList.E001);
			}
			
			ArrayList<String[]> segmentDataList = parsedSegments.containsKey(segment) ? parsedSegments.get(segment) : null;
			
			if (null == segmentDataList)
				segmentDataList = new ArrayList<String[]>();
			
			segmentDataList.add(fieldsArray);
				
			
			parsedSegments.put(segment, segmentDataList);
			
			record = StringUtils.substring(record, nextSegmentStartPos+1);
			
//			System.out.println(record);
		}
		
		//System.out.println("CNSCRD" +parsedSegments.get("CNSCRD").get(0)[1]);
		//System.out.println("ACTCRD" +parsedSegments.get("ACTCRD").size());
		
		return parsedSegments;
	}
	

	
	public static void main(String[] args) throws CdfParsingException{
		String rec = "CNSCRD~1101001140~1101~11010001~1101000107~GOWDRA PRAHALADA  ~~~~02031980~~~M~M01~~~~~~~~~~~~~~~~~~I~25                            ~~~~~~~~~~~0~~~~Z03~~~R01~NSP~~N~~ADRCRD~BANGALORE BANGALORE~~~~~~~";
		
//		String rec = "CNSCRD~1101001185~1101~11010001~1101000106~Umme Salma ~~~~26051988~~~M~M06~~~~~~~~~~~~~~~~~~I~B546531                       ~~~~~P03~9611800835~~~~~0~~~~Z03~~~R01~OBC~~N~~ACTCRD~1101791000007321~1101791000007321~1101~11010001~RHRM~18112013~T03~1101000106~1~12~S04~23122011~23122011~23122011~~31082013~30000~30000~30000~24~F03~1498~5766~2996~51~~~X09~~~Y~L01~~WED~11:00~~";
		CdfParser cdfParser = new CdfParser();
		cdfParser.parseMe(rec);
		
		//System.out.println("Error  Code: " +ErrorRecord.E001);
	}
}
