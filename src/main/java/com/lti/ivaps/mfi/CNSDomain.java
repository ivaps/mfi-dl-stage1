package com.lti.ivaps.mfi;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.HiveContext;
import org.datanucleus.metadata.ImplementsMetaData;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import scala.collection.immutable.Seq;

public class CNSDomain implements Serializable {/*
	String member_id;
	String mfi_id;
	String name;
	String age;
	List<Address> add;
	List<Account> act;

	Address add;
	Account act;

	public String getMember_id() {
		return member_id;
	}
	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}
	public String getMfi_id() {
		return mfi_id;
	}
	public void setMfi_id(String mfi_id) {
		this.mfi_id = mfi_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
		public List<Address> getAdd() {
		return add;
	}
	public void setAdd(List<Address> add) {
		this.add = add;
	}
	public List<Account> getAct() {
		return act;
	}
	public void setAct(List<Account> act) {
		this.act = act;
	}





	@Override
	public String toString() {
		return "CNSDomain [member_id=" + member_id + ", mfi_id=" + mfi_id + ", name=" + name + ", age=" + age + ", add="
				+ add + ", act=" + act + "]";
	}


	public Address getAdd() {
		return add;
	}
	public void setAdd(Address add) {
		this.add = add;
	}
	public Account getAct() {
		return act;
	}
	public void setAct(Account act) {
		this.act = act;
	}


	public static void main(String[] args) {
		CNSDomain cnsDomain = new CNSDomain();

		cnsDomain.setMember_id("100");
		cnsDomain.setMfi_id("NBFLTFIN50");
		cnsDomain.setName("Aditya Calangutkar");
		cnsDomain.setAge("27");

		Address ad = new Address();
		ad.setAdd_key("123");
		ad.setAddress("Goa");
		List<Address> address = new ArrayList<Address>();
		address.add(ad);
		//	cnsDomain.setAdd(address);

		Account ac = new Account();
		ac.setAct_key("2222");
		ac.setAct_number("0000524371");

		List<Account> account = new ArrayList<Account>();
		account.add(ac);
		//cnsDomain.setAct(account);

		SparkConf conf = null;
		//	final JavaSparkContext context = null;
		//final SQLContext sqlContext = null;
		HiveContext hc = null;

		conf = new SparkConf().setAppName("SparkApp");
		conf.setMaster("local");
		conf.set("spark.sql.parquet.compression.codec", "snappy");
		final JavaSparkContext context = new JavaSparkContext(conf);
		final SQLContext sqlContext = new SQLContext(context);

		JavaRDD<String> textFile = context.textFile("D:\\iVAPS\\MFI\\test.CDF");
				textFile.foreach(new VoidFunction<String>() {

			public void call(String arg0) throws Exception {
				System.out.println(arg0);
				String[] split = arg0.split("ADRCRD");
				//String[] splitPreserveAllTokens = StringUtils.splitPreserveAllTokens(arg0, "ADRCRD");
				System.out.println("CNSCRD data : " + split[0]);
				CNSDomain cnDomain = new CNSDomain();
				cnDomain.setMember_id(split[0].toString().split("~")[1]);
				cnDomain.setMfi_id(split[0].toString().split("~")[3]);
				cnDomain.setName(split[0].toString().split("~")[5]);
				cnDomain.setAge(split[0].toString().split("~")[10]);

				Address ad = new Address();
				ad.setAdd_key("123");
				ad.setAddress("Goa");
				List<Address> address = new ArrayList<Address>();
				address.add(ad);
				cnDomain.setAdd(address);

				Account ac = new Account();
				ac.setAct_key("2222");
				ac.setAct_number("0000524371");

				List<Account> account = new ArrayList<Account>();
				account.add(ac);
				cnDomain.setAct(account);

				System.out.println(cnDomain.toString());
			}
		});

		final JavaRDD<String> parallelize = null;

		JavaRDD<MfiDomain> map = textFile.map(new Function<String, MfiDomain>()  {

			public MfiDomain call(String arg0) throws Exception { 

				String[] split = arg0.split("ADRCRD");
				//String[] splitPreserveAllTokens = StringUtils.splitPreserveAllTokens(arg0, "ADRCRD");
				System.out.println("CNSCRD data : " + split[0]);

				MfiDomain mfiDomain = new MfiDomain();
				mfiDomain.setMfiId("NBFLTFIN50");
				mfiDomain.setMemberId("10002");
				mfiDomain.setCNSCRD("CNSCRD~011032667~BERHAMPUR~01100600~011~SRI LAXMIRANI DAS~~~~01011981~30~32~F~M01~LINGARAJ DAS~K02~~~~~~~~~LINGARAJ DAS~K02~~~~~~~~~~~~~~~~~~~~~~Z02~537600~41000~~O.B.C~~~~");
				mfiDomain.setADDRCRD("ADRCRD~FIRSTGATE .  GANJAM~AP~760002~~AP~~~");
				mfiDomain.setACCTCRD("ACTCRD~BERHAMPUR9942441~BERHAMPUR9942441~BERHAMPUR~01100600~SAISMITA PATRA(FS - 3)~31082012~T02~011~1~A07~S07~24092011~25092011~29092011~08102013~08102013~20000~20000~20000~25~F03~0~0~0~0~~~~~~Y~L01~60000~~~~");
				mfiDomain.setIdentity("ARSPC5749D~K42773");
				mfiDomain.setPhone("9970763410~8149936408");
				mfiDomain.setEmail("a.calangutkar@gmail.com~aditya.calangutkar@yahoo.com");
				mfiDomain.setValid("yes");
				mfiDomain.setReasonForFailure("ACCT not valid");
				mfiDomain.setGlobalCRC(12345l);

				return mfiDomain;
			}
		});


		DataFrame createDataFrame = sqlContext.createDataFrame(map, MfiDomain.class);
		createDataFrame.show();

		//map.collect();

		DataFrame json2 = sqlContext.read().json(parallelize);
		json2.show();

		DataFrame json = sqlContext.read().json("D:\\user.json");
		json.registerTempTable("mytable");
		DataFrame sql = sqlContext.sql("SELECT add from mytable");
		sql.show();
		sql.write().parquet("D:\\parquetfile");


		DataFrame parquet = sqlContext.read().parquet("D:\\parquetfile");
		parquet.show();

		 List<StructField> fields = null;
		StructType schema = DataTypes.createStructType(fields);

		//System.out.println(cnsDomain.getAge());
	}

	static String createJSON(CNSDomain cnDomain) {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			System.out.println(cnDomain.toString());
			//Convert object to JSON string and save into file directly
			mapper.writeValue(new File("D:\\user.json"), cnDomain);

			//Convert object to JSON string
			jsonInString = mapper.writeValueAsString(cnDomain);
			System.out.println("Normal Print : " + jsonInString);

			//Convert object to JSON string and pretty print
			jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(cnDomain);
			System.out.println("Pretty Print : " + jsonInString);
			return jsonInString;

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonInString;

	*/}
//}	
