/*package com.lti.ivaps.mfi;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.lti.mfi.data.load.AccountBeanBuilder;
import com.lti.mfi.data.load.AddressBeanBuilder;
import com.lti.mfi.data.load.ConsumerBeanBuilder;

public class Validations {
	public static boolean validateRecord(MfiDomain mfiDomain, int passedRecords, int failedRecords) {

		String memberId = mfiDomain.getMemberId();
		String mfiId = mfiDomain.getMfiId();
		String cnscrd = mfiDomain.getCNSCRD();
		String addrcrd = mfiDomain.getADDRCRD();
		String acctcrd = mfiDomain.getACCTCRD();
		String identity = mfiDomain.getIdentity();
		//String email = mfiDomain.getEmail();
		String phone = mfiDomain.getPhone();
		String rptDate = mfiDomain.getRptDate();
		
		//System.out.println("Validate Record...............!!!!!!!!!!!!!");
		
		//System.out.println(mfiDomain.toString());

		if (checkMemberId(memberId) && checkMemberId(mfiId) && checkCNSCRD(cnscrd) && checkCNSCRD(addrcrd)
				&& checkCNSCRD(acctcrd) && checkRptDate(rptDate)) {
			//if(checkIdentity(identity) || checkPhone(phone)){
			mfiDomain.setValid("yes");
			//System.out.println("MFI Domain: " +mfiDomain);
			List<Address> address = AddressBeanBuilder.buidAddressBean(addrcrd);
			List<Account> account = AccountBeanBuilder.buidAccountBean(acctcrd);
			List<Consumer> consumer = ConsumerBeanBuilder.buildConsumer(cnscrd);
		//	mfiDomain.setGlobalCRC(ExperianCRC32.getInstance().getGlobalCRC32(consumer.getMemberIdentifier(), consumer.getBranchIdentifier(), consumer.getCentreIdentifier(), consumer.getMemberFirstName(), consumer.getMemberMiddleName(), consumer.getMemberLastName(), consumer.getMemberDOB(), consumer.getMemberGender(), consumer.getKeyPersonName(), "", consumer.getKeyPersonRelnship(), consumer.getNomineeName(), consumer.getVoterId(), consumer.getUid(), consumer.getPan(), consumer.getRationCard(), consumer.getOtrIdTypeOneValue(), consumer.getOtherIdTypeTwoValue(), "", consumer.getTelephNumberTypeOneValue(), consumer.getTelephNumberTypeOneInd(), consumer.getTelephNumberTypeTwoValue(), consumer.getTelephNumberTypeTwoInd(), address.getPermanentAddress(), address.getPermanentAdrstateCode(), address.getPermanentAdrpinCode(), address.getCurrentAddress(), address.getCurrentAdrstateCode(), address.getCurrentAdrpinCode()));
			mfiDomain.setGlobalCRC(ExperianCRC32.getInstance().getGlobalCRCLTI(address,consumer));
			passedRecords++;
			//System.out.println("Record Validated successfully!!");
			return true;
		//	}
		} else {
			mfiDomain.setValid("no");
			failedRecords++;
			//System.out.println("Record validation Failed!!");
			return false;
		}

	}

	private static boolean checkIdentity(String identity) {
		if(!StringUtils.isBlank(identity) && identity != null){
			return true;
		}
		return false;
	}

	private static boolean checkEmail(String email) {
		if(!StringUtils.isBlank(email) && email != null){
			return true;
		}
		return false;
	}

	private static boolean checkPhone(String phone) {
		if(!StringUtils.isBlank(phone) && phone != null){
			return true;
		}
		return false;
	}

	private static boolean checkCNSCRD(String cnscrd) {
		if(!StringUtils.isBlank(cnscrd) && cnscrd != null){
			return true;
		}
		return false;
	}

	private static boolean checkMemberId(String memberId) {
		if(!StringUtils.isBlank(memberId) && memberId != null){
			return true;
		}
		return false;
	}
	
	private static boolean checkRptDate(String rptDate) {
		if(!StringUtils.isBlank(rptDate) && rptDate != null){
			return true;
		}
		return false;
	}
}
*/