package com.lti.ivaps.mfi.dataframes;

import java.io.Serializable;

public class Profiling2 implements Serializable {
	
	private int totalLoanCategoryJLGGroup;
	private int totalLoanCategoryJLGIndividual;
	private int totalLoanCategoryIndividual;
	private int totalLoanCategoryOther;

	private int totalLoanPurposeIndividualLoanEducationLoan;
	private int totalLoanPurposeGroupLoanEmergencyFestivalLoan;
	private int totalLoanPurposeIndividualLoanEmergencyFestivalLoan;
	private int totalLoanPurposeFamilyLoan;
	private int totalLoanPurposeGeneralLoan;
	private int totalLoanPurposeGoldLoan;
	private int totalLoanPurposeOthers;

	private int totalAccStatusLoanSubmitted;
	private int totalAccStatusLoanApprovedNotyetdisbursed;
	private int totalAccStatusLoanDeclined;
	private int totalAccStatusCurrent;
	private int totalAccStatusDelinquent;

	public int getTotalLoanCategoryJLGGroup() {
		return totalLoanCategoryJLGGroup;
	}

	public void setTotalLoanCategoryJLGGroup(int totalLoanCategoryJLGGroup) {
		this.totalLoanCategoryJLGGroup = totalLoanCategoryJLGGroup;
	}

	public int getTotalLoanCategoryJLGIndividual() {
		return totalLoanCategoryJLGIndividual;
	}

	public void setTotalLoanCategoryJLGIndividual(int totalLoanCategoryJLGIndividual) {
		this.totalLoanCategoryJLGIndividual = totalLoanCategoryJLGIndividual;
	}

	public int getTotalLoanCategoryIndividual() {
		return totalLoanCategoryIndividual;
	}

	public void setTotalLoanCategoryIndividual(int totalLoanCategoryIndividual) {
		this.totalLoanCategoryIndividual = totalLoanCategoryIndividual;
	}

	public int getTotalLoanCategoryOther() {
		return totalLoanCategoryOther;
	}

	public void setTotalLoanCategoryOther(int totalLoanCategoryOther) {
		this.totalLoanCategoryOther = totalLoanCategoryOther;
	}

	public int getTotalLoanPurposeIndividualLoanEducationLoan() {
		return totalLoanPurposeIndividualLoanEducationLoan;
	}

	public void setTotalLoanPurposeIndividualLoanEducationLoan(int totalLoanPurposeIndividualLoanEducationLoan) {
		this.totalLoanPurposeIndividualLoanEducationLoan = totalLoanPurposeIndividualLoanEducationLoan;
	}

	public int getTotalLoanPurposeGroupLoanEmergencyFestivalLoan() {
		return totalLoanPurposeGroupLoanEmergencyFestivalLoan;
	}

	public void setTotalLoanPurposeGroupLoanEmergencyFestivalLoan(int totalLoanPurposeGroupLoanEmergencyFestivalLoan) {
		this.totalLoanPurposeGroupLoanEmergencyFestivalLoan = totalLoanPurposeGroupLoanEmergencyFestivalLoan;
	}

	public int getTotalLoanPurposeIndividualLoanEmergencyFestivalLoan() {
		return totalLoanPurposeIndividualLoanEmergencyFestivalLoan;
	}

	public void setTotalLoanPurposeIndividualLoanEmergencyFestivalLoan(
			int totalLoanPurposeIndividualLoanEmergencyFestivalLoan) {
		this.totalLoanPurposeIndividualLoanEmergencyFestivalLoan = totalLoanPurposeIndividualLoanEmergencyFestivalLoan;
	}

	public int getTotalLoanPurposeFamilyLoan() {
		return totalLoanPurposeFamilyLoan;
	}

	public void setTotalLoanPurposeFamilyLoan(int totalLoanPurposeFamilyLoan) {
		this.totalLoanPurposeFamilyLoan = totalLoanPurposeFamilyLoan;
	}

	public int getTotalLoanPurposeGeneralLoan() {
		return totalLoanPurposeGeneralLoan;
	}

	public void setTotalLoanPurposeGeneralLoan(int totalLoanPurposeGeneralLoan) {
		this.totalLoanPurposeGeneralLoan = totalLoanPurposeGeneralLoan;
	}

	public int getTotalLoanPurposeGoldLoan() {
		return totalLoanPurposeGoldLoan;
	}

	public void setTotalLoanPurposeGoldLoan(int totalLoanPurposeGoldLoan) {
		this.totalLoanPurposeGoldLoan = totalLoanPurposeGoldLoan;
	}

	public int getTotalLoanPurposeOthers() {
		return totalLoanPurposeOthers;
	}

	public void setTotalLoanPurposeOthers(int totalLoanPurposeOthers) {
		this.totalLoanPurposeOthers = totalLoanPurposeOthers;
	}

	public int getTotalAccStatusLoanSubmitted() {
		return totalAccStatusLoanSubmitted;
	}

	public void setTotalAccStatusLoanSubmitted(int totalAccStatusLoanSubmitted) {
		this.totalAccStatusLoanSubmitted = totalAccStatusLoanSubmitted;
	}

	public int getTotalAccStatusLoanApprovedNotyetdisbursed() {
		return totalAccStatusLoanApprovedNotyetdisbursed;
	}

	public void setTotalAccStatusLoanApprovedNotyetdisbursed(int totalAccStatusLoanApprovedNotyetdisbursed) {
		this.totalAccStatusLoanApprovedNotyetdisbursed = totalAccStatusLoanApprovedNotyetdisbursed;
	}

	public int getTotalAccStatusLoanDeclined() {
		return totalAccStatusLoanDeclined;
	}

	public void setTotalAccStatusLoanDeclined(int totalAccStatusLoanDeclined) {
		this.totalAccStatusLoanDeclined = totalAccStatusLoanDeclined;
	}

	public int getTotalAccStatusCurrent() {
		return totalAccStatusCurrent;
	}

	public void setTotalAccStatusCurrent(int totalAccStatusCurrent) {
		this.totalAccStatusCurrent = totalAccStatusCurrent;
	}

	public int getTotalAccStatusDelinquent() {
		return totalAccStatusDelinquent;
	}

	public void setTotalAccStatusDelinquent(int totalAccStatusDelinquent) {
		this.totalAccStatusDelinquent = totalAccStatusDelinquent;
	}

	public Profiling2(int totalLoanCategoryJLGGroup, int totalLoanCategoryJLGIndividual, int totalLoanCategoryIndividual,
			int totalLoanCategoryOther, int totalLoanPurposeIndividualLoanEducationLoan,
			int totalLoanPurposeGroupLoanEmergencyFestivalLoan, int totalLoanPurposeIndividualLoanEmergencyFestivalLoan,
			int totalLoanPurposeFamilyLoan, int totalLoanPurposeGeneralLoan, int totalLoanPurposeGoldLoan,
			int totalLoanPurposeOthers, int totalAccStatusLoanSubmitted, int totalAccStatusLoanApprovedNotyetdisbursed,
			int totalAccStatusLoanDeclined, int totalAccStatusCurrent, int totalAccStatusDelinquent) {
		super();
		this.totalLoanCategoryJLGGroup = totalLoanCategoryJLGGroup;
		this.totalLoanCategoryJLGIndividual = totalLoanCategoryJLGIndividual;
		this.totalLoanCategoryIndividual = totalLoanCategoryIndividual;
		this.totalLoanCategoryOther = totalLoanCategoryOther;
		this.totalLoanPurposeIndividualLoanEducationLoan = totalLoanPurposeIndividualLoanEducationLoan;
		this.totalLoanPurposeGroupLoanEmergencyFestivalLoan = totalLoanPurposeGroupLoanEmergencyFestivalLoan;
		this.totalLoanPurposeIndividualLoanEmergencyFestivalLoan = totalLoanPurposeIndividualLoanEmergencyFestivalLoan;
		this.totalLoanPurposeFamilyLoan = totalLoanPurposeFamilyLoan;
		this.totalLoanPurposeGeneralLoan = totalLoanPurposeGeneralLoan;
		this.totalLoanPurposeGoldLoan = totalLoanPurposeGoldLoan;
		this.totalLoanPurposeOthers = totalLoanPurposeOthers;
		this.totalAccStatusLoanSubmitted = totalAccStatusLoanSubmitted;
		this.totalAccStatusLoanApprovedNotyetdisbursed = totalAccStatusLoanApprovedNotyetdisbursed;
		this.totalAccStatusLoanDeclined = totalAccStatusLoanDeclined;
		this.totalAccStatusCurrent = totalAccStatusCurrent;
		this.totalAccStatusDelinquent = totalAccStatusDelinquent;
	}

}
