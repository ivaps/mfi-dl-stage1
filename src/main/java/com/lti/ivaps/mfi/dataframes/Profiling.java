package com.lti.ivaps.mfi.dataframes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.calcite.linq4j.tree.ArrayLengthRecordField;

public class Profiling implements Serializable {

	private String Field;
	private String ProfilingDescription;
	private int Count;


	public Profiling(String field, String profilingDescription, int count) {
		super();
		Field = field;
		ProfilingDescription = profilingDescription;
		Count = count;
	}

	public static List<Profiling> generateProfilingList(int totalLoanCategoryJLGGroup, int totalLoanCategoryJLGIndividual, int totalLoanCategoryIndividual,
			int totalLoanCategoryOther, int totalLoanPurposeIndividualLoanEducationLoan,
			int totalLoanPurposeGroupLoanEmergencyFestivalLoan, int totalLoanPurposeIndividualLoanEmergencyFestivalLoan,
			int totalLoanPurposeFamilyLoan, int totalLoanPurposeGeneralLoan, int totalLoanPurposeGoldLoan,
			int totalLoanPurposeOthers, int totalAccStatusLoanSubmitted, int totalAccStatusLoanApprovedNotyetdisbursed,
			int totalAccStatusLoanDeclined, int totalAccStatusCurrent, int totalAccStatusDelinquent) {
		
		List<Profiling> profList = new ArrayList();

		profList.add(new Profiling("Loan Category", "Total # JLG group", totalLoanCategoryJLGGroup));
		profList.add(new Profiling("Loan Category", "Total # JLG individual", totalLoanCategoryJLGIndividual));
		profList.add(new Profiling("Loan Category", "Total # Individual", totalLoanCategoryIndividual));
		profList.add(new Profiling("Loan Category", "Total # other loans", totalLoanCategoryOther));
		profList.add(new Profiling("Loan Purpose", "Total # education loan", totalLoanPurposeIndividualLoanEducationLoan));
		profList.add(new Profiling("Loan Purpose", "Total # group emergency festival loan", totalLoanPurposeGroupLoanEmergencyFestivalLoan));
		profList.add(new Profiling("Loan Purpose", "Total # individual emergency festival loan", totalLoanPurposeIndividualLoanEmergencyFestivalLoan));
		profList.add(new Profiling("Loan Purpose", "Total # family loan", totalLoanPurposeFamilyLoan));
		profList.add(new Profiling("Loan Purpose", "Total # general loan", totalLoanPurposeGeneralLoan));
		profList.add(new Profiling("Loan Purpose", "Total # gold loan", totalLoanPurposeGoldLoan));
		profList.add(new Profiling("Loan Purpose", "Total # other loans", totalLoanPurposeOthers));
		profList.add(new Profiling("Account Status", "Total # loans submitted", totalAccStatusLoanSubmitted));
		profList.add(new Profiling("Account Status", "Total # loans approved and not yet disbursed", totalAccStatusLoanApprovedNotyetdisbursed));
		profList.add(new Profiling("Account Status", "Total # declined loans", totalAccStatusLoanDeclined));
		profList.add(new Profiling("Account Status", "Total # current loans", totalAccStatusCurrent));
		profList.add(new Profiling("Account Status", "Total # delinquent loans", totalAccStatusDelinquent));
/*		profList.add(new Profiling(1, "Total # JLG group", totalLoanCategoryJLGGroup));
		profList.add(new Profiling(1, "Total # JLG group", totalLoanCategoryJLGGroup));
		profList.add(new Profiling(1, "Total # JLG group", totalLoanCategoryJLGGroup));
		profList.add(new Profiling(1, "Total # JLG group", totalLoanCategoryJLGGroup));*/
		
		return profList;
	}

	public String getField() {
		return Field;
	}

	public void setField(String field) {
		Field = field;
	}

	public String getProfilingDescription() {
		return ProfilingDescription;
	}

	public void setProfilingDescription(String profilingDescription) {
		ProfilingDescription = profilingDescription;
	}

	public int getCount() {
		return Count;
	}

	public void setCount(int count) {
		Count = count;
	}

	@Override
	public String toString() {
		return "Profiling [Field=" + Field + ", ProfilingDescription=" + ProfilingDescription + ", Count=" + Count
				+ "]";
	}

}
