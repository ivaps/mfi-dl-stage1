package com.lti.ivaps.mfi;


public interface ExperianConstants {

	static final String STR_SPACE = " ";
	static final String DOT = "\\.";
	static final String COLON = "\\:";
	static final String SINGLEQUOTION = "\\'";
	static final String DOUBLEQUOTION = "\"";
	static final String QUESTIONMARK = "\\?";
	static final String COMMA = "\\,";
	static final String TILDE = "\\~";
	static final String BANG = "\\!";
	static final String AT = "\\@";
	static final String HASH = "\\#";
	static final String DOLLAR = "\\$";
	static final String PERCENT = "\\%";
	static final String CARET = "\\^";
	static final String AND = "\\&";
	static final String STAR = "\\*";
	static final String OPENPARENTHESIS = "\\(";
	static final String CLOSEPARENTHESIS = "\\)";
	static final String HYPEN = "\\-";
	static final String UNDERSCORE = "\\_";
	static final String PLUS = "\\+";
	static final String EQUALS = "\\=";
	static final String OPENBRACE = "\\{";
	static final String CLOSEBRACE = "\\}";
	static final String OPENBRACKET = "\\[";
	static final String COLSEBRACKET = "\\]";
	static final String PIPE = "\\|";
	static final String BACKSLASH ="\\\\";
	
	static final String REMOVE_SPECIAL_CHARACTERS[] = {"~","`","!","@","#","$","%","^","&","*","(",")","-","_","+","=","[","]","{","}","\\","|","\'","\"",";",":","/","?",".",",",">","<"};
	static final String REMOVE_CHARACTERS_FOR_NAME[] = {"~","`","!","@","#","$","%","^","&","*","(",")","-","_","+","=","[","]","{","}","\\","|","\'","\"",";",":","?",".",",",">","<"};
	static final String REMOVE_CHARACTERS_FOR_ACCOUNT[] = {"~","`","!","@","#","$","%","^","&","*","(",")","-","_","+","=","[","]","{","}","\\","|","\'","\"",";",":","?",".",",",">","<"};
	static final String REMOVE_CHARACTERS_FOR_PHONE[] = {"~","`","!","@","#","$","%","^","&","*","(",")","-","_","+","=","[","]","{","}","\\","|","\'","\"",";",":","?",".",",",">","<"};
	static final String REMOVE_CHARACTERS_FOR_AMOUNT[] = {"~","`","!","@","#","$","%","^","&","*","(",")","-","_","+","=","[","]","{","}","\\","|","\'","\"",";",":","?",",",">","<"};
	
	
	
	static final String NINE_REGEX = "9{1,}";
	static final String EIGHT_REGEX = "8{1,}";
	static final String SEVEN_REGEX = "7{1,}";
	static final String SIX_REGEX = "6{1,}";
	static final String FIVE_REGEX = "5{1,}";
	static final String FOUR_REGEX = "4{1,}";
	static final String THREE_REGEX = "3{1,}";
	static final String TWO_REGEX = "2{1,}";
	static final String ONE_REGEX = "1{1,}";
	static final String ZERO_REGEX = "0{1,}";
	static final String AlphaNumString = "QWERTYUIOPASDFGHJKLZXCVBNM1234567890";
	

}
