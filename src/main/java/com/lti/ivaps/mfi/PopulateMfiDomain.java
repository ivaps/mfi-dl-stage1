package com.lti.ivaps.mfi;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.sql.Row;

import com.lti.ivaps.mfi.beans.Address;
import com.lti.ivaps.mfi.beans.Consumer;
import com.lti.ivaps.mfi.beans.MfiDomain;
import com.lti.ivaps.mfi.constans.MfiConstants;
import com.lti.ivaps.mfi.exceptions.CdfValidationException;

public class PopulateMfiDomain {
	
	static HashMap<String, String> segmentMap = new HashMap<String, String>();
	
	public static MfiDomain populateMfiDomainFromRDD(HashMap<String, ArrayList<String[]>> record, MfiDomain mfiDomain) throws CdfValidationException {
		
		mfiDomain.setMemberId(record.get(MfiConstants.CNSCRD).get(0)[1]);
		mfiDomain.setConsumerSegment(record.get(MfiConstants.CNSCRD).get(0));
		mfiDomain.setAddressSegment(record.get(MfiConstants.ADRCRD));
		mfiDomain.setAccountSegment(record.get(MfiConstants.ACTCRD));
		mfiDomain.setGlobalCrc(mfiDomain.calculateGlobalCRC());
		
		return mfiDomain;
		
		
	}
	
	public static MfiDomain populateMfiDomainFromRDD1(HashMap<String, ArrayList<String[]>> parsedRecord,
			int passedRecords, int failedRecords, String mfiId, String rptDt, MfiDomain mfiDomain) {
		// TODO Auto-generated method stub
		return null;
	}
		
}
