package com.lti.ivaps.mfi;

public class MyClass {
	String record;

	public String getRecord() {
		return record;
	}

	public void setRecord(String record) {
		this.record = record;
	}

	@Override
	public String toString() {
		return "MyClass [record=" + record + "]";
	}
	
	
}
