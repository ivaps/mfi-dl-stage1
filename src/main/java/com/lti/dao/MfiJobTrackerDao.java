package com.lti.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.lti.connection.mysql.MysqlConnection;
import com.lti.ivaps.mfi.constans.Db_Queries;
import com.lti.ivaps.mfi.validation.CdfValidation;

public class MfiJobTrackerDao {
	
	static  Logger log = Logger.getLogger(MfiJobTrackerDao.class);

	public static boolean updateMfiJobTrackerAndProcessedFilesStatus(String mfiJobId, String maxiqJobId, String appName, String nodeName,/*String mfiJobStatus,*/ int totalRecs, int failedRecs, int passRecs) throws Exception{

		String queryToUpdateJobTracker = Db_Queries.updateMfiJobTrackerStatus;
		Connection con = MysqlConnection.getConnection();
		
		log.info("Connected to SQL database");

		PreparedStatement statementToUpdateJobTracker = con.prepareStatement(queryToUpdateJobTracker);
		//String[] id =StringUtils.splitPreserveAllTokens( getFileId(mfiJobId), "|");
		//String fileId = id[0];
		//String maxiqJobId = id[1];
		//statementToUpdateJobTracker.setString(1, mfiJobStatus);
		statementToUpdateJobTracker.setString(1, mfiJobId);
		statementToUpdateJobTracker.setString(2, maxiqJobId);
		int executeQuery = statementToUpdateJobTracker.executeUpdate();
		int executeQueryresult = 0;
		String fileId = getFileId(mfiJobId, maxiqJobId, con);
		
		if(StringUtils.isNotBlank(fileId)){
			String queryToupdateMfiProcessedFilesStatus = Db_Queries.updateMfiProcessedFilesStatus;
			PreparedStatement statementToUpdateProcessedFile = con.prepareStatement(queryToupdateMfiProcessedFilesStatus);
			statementToUpdateProcessedFile.setLong(1, totalRecs);
			statementToUpdateProcessedFile.setLong(2, failedRecs);
			statementToUpdateProcessedFile.setLong(3, passRecs);
			//app_testingmfi07_mfi_passedrecords_1784137
			statementToUpdateProcessedFile.setString(4, "app_"+appName+"_"+nodeName+"_passedrecords_"+maxiqJobId);
			statementToUpdateProcessedFile.setString(5, "app_"+appName+"_"+nodeName+"_failedRecords_"+maxiqJobId);
			statementToUpdateProcessedFile.setString(6, "app_"+appName+"_"+nodeName+"_profiling_"+maxiqJobId);
			statementToUpdateProcessedFile.setString(7, "app_"+appName+"_"+nodeName+"_summarystatistics_"+maxiqJobId);
			//statementToUpdateProcessedFile.setString(8, mfiJobStatus);
			statementToUpdateProcessedFile.setString(8, fileId);
			statementToUpdateProcessedFile.setString(9, "1");

			executeQueryresult = statementToUpdateProcessedFile.executeUpdate();
		}
		if(executeQuery > 0 && executeQueryresult > 0)
			return true;
		else
			return false;
	}

	
	private static String getFileId(String mfiJobId, String maxiqJobId, Connection con) throws Exception{
		String queryToGetFileId = Db_Queries.getFileIDForProcessedFile;	
		/*Connection*/ con = MysqlConnection.getConnection();
		PreparedStatement prepareStatement = con.prepareStatement(queryToGetFileId);
		prepareStatement.setString(1, mfiJobId);
		prepareStatement.setString(2, maxiqJobId);

		ResultSet executeQuery = prepareStatement.executeQuery();		
		if(executeQuery.next())
			return executeQuery.getString("FILE_ID")/*+"|"+executeQuery.getString("MAXIQ_JOB_ID")*/;
		return "";
	}
	
	public static void main(String[] args) {
		try {
			/*Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://172.20.99.22:3306/" + "ivaps_mfi" + "?zeroDateTimeBehavior=convertToNull", "root", "");*/
			MfiJobTrackerDao.updateMfiJobTrackerAndProcessedFilesStatus("59", "1784150", "Stage1", "mfi", 30, 10, 20);
			
			//System.out.println("Update Successful");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
